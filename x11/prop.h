#ifndef _winx68k_config
#define _winx68k_config

typedef struct
{
	DWORD SampleRate;
	DWORD BufferSize;
	int WinPosX;
	int WinPosY;
	int OPM_VOL;
	int PCM_VOL;
#ifndef NO_MERCURY
	int MCR_VOL;
#endif	// !NO_MERCURY
	int JOY_BTN[2][8];
	int MouseSpeed;
	int WindowFDDStat;
	int FullScrFDDStat;
	int DSAlert;
#ifndef NO_MIDI
	int MIDI_SW;
	int MIDI_Type;
	int MIDI_Reset;
#endif	// !NO_MIDI
	int JoyKey;
	int JoyKeyReverse;
	int JoyKeyJoy2;
	int SRAMWarning;
	char HDImage[16][MAX_PATH];
	int XVIMode;
	int JoySwap;
#ifndef NO_WINDRV
	int LongFileName;
	int WinDrvFD;
#endif	// !NO_WINDRV
	int WinStrech;
	int DSMixing;
#ifndef NO_WINDRV
	int CDROM_ASPI;
	int CDROM_ASPI_Drive;
	int CDROM_IOCTRL_Drive;
	int CDROM_SCSIID;
	int CDROM_Enable;
#endif	// !NO_WINDRV
	int SSTP_Enable;
	int SSTP_Port;
	int Sound_LPF;
#ifdef USE_ROMEO
	int SoundROMEO;
#endif	// USE_ROMEO
#ifndef NO_MIDI
	int ToneMap;
	char ToneMapFile[MAX_PATH];
	int MIDIDelay;
	int MIDIAutoDelay;
#endif	// !NO_MIDI
} Win68Conf;

extern Win68Conf Config;

void LoadConfig(void);
void SaveConfig(void);
void PropPage_Init(void);

int set_modulepath(char *path, size_t len);

#endif //_winx68k_config

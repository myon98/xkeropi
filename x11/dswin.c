/*	$Id: dswin.c,v 1.3 2010/11/07 14:22:50 nonaka Exp $	*/

/* 
 * Copyright (c) 2003 NONAKA Kimihiro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include	"windows.h"
#include	"common.h"
#include	"dswin.h"

#ifndef NOSOUND
#include	"prop.h"
#include	"adpcm.h"
#include	"mercury.h"
#include	"../fmgen/fmg_wrap.h"
#include	<SDL.h>
#include	<SDL_audio.h>
#include	<SDL_mutex.h>

static	short	playing = FALSE;

static	BYTE	*pcmbuffer = NULL;
static	DWORD	pcmbuffers = 22050;
static	DWORD	pcmbuffermax = 22050 * 3;
static	DWORD	pcmindex = 0;
static	DWORD	ratebase = 22050;
static	long	DSound_PreCounter = 0;

static int audio_fd = -1;
static SDL_mutex *mutex = NULL;

// ぷろとたいぷ
static DWORD calc_blocksize(DWORD size);
static void sdlaudio_callback(void *userdata, unsigned char *stream, int len);

// ---------------------------------------------------------------------------
//  初期化
// ---------------------------------------------------------------------------

int
DSound_Init(unsigned long rate, unsigned long buflen)
{
	// 初期化済みか？
	if (playing) {
		return FALSE;
	}

	// 無音指定か？
	if (rate == 0) {
		audio_fd = -1;
		return TRUE;
	}

	// pcmbuffermax = Dsoundバッファ3倍のサンプル数
	const DWORD samples = calc_blocksize((DWORD)rate * buflen / 200 / 4);
	pcmbuffers = samples * 4;
	pcmbuffermax = pcmbuffers * 3;
	pcmbuffer = (BYTE *)malloc(pcmbuffermax);
	bzero(pcmbuffer, pcmbuffermax);
	pcmindex = 0;
	ratebase = rate;

	// 初期化
	int rv = SDL_Init(SDL_INIT_AUDIO);
	if (rv < 0) {
		audio_fd = -1;
		return FALSE;
	}

	// サウンドバッファを確保するよ～
	SDL_AudioSpec fmt;
	bzero(&fmt, sizeof(fmt));
	fmt.freq = rate;
	fmt.format = AUDIO_S16SYS;
	fmt.channels = 2;
	fmt.samples = samples;
	fmt.callback = sdlaudio_callback;
	fmt.userdata = NULL;
	audio_fd = SDL_OpenAudio(&fmt, NULL);
	if (audio_fd < 0) {
		SDL_Quit();
		return FALSE;
	}

	mutex = SDL_CreateMutex();

	playing = TRUE;
	return TRUE;
}

void
DSound_Play(void)
{
	if (audio_fd >= 0)
		SDL_PauseAudio(0);
}

void
DSound_Stop(void)
{
	if (audio_fd >= 0)
		SDL_PauseAudio(1);
}


// ---------------------------------------------------------------------------
//  後片付け
// ---------------------------------------------------------------------------

int
DSound_Cleanup(void)
{
	playing = FALSE;
	if (audio_fd >= 0) {
		SDL_CloseAudio();
		SDL_Quit();
		audio_fd = -1;

		SDL_DestroyMutex(mutex);
		mutex = NULL;
	}
	return TRUE;
}


// ---------------------------------------------------------------------------
//  適当に呼ばれる
// ---------------------------------------------------------------------------

static void FASTCALL
Update(int length)
{
	const int pcmfree = pcmbuffermax - pcmindex;
	if (length > pcmfree) {
		length = pcmfree;
	}
	if (length > 0) {
		short *pcmbufp = (short *)(pcmbuffer + pcmindex);
		ADPCM_Update(pcmbufp, length);
		OPM_Update(pcmbufp, length);
#ifndef	NO_MERCURY
		Mcry_Update(pcmbufp, length);
#endif
		pcmindex += length * sizeof(WORD) * 2;
	}
}

void FASTCALL
DSound_Send0(long clock)
{
	int length = 0;

	if (audio_fd >= 0) {
		DSound_PreCounter += (ratebase * clock);
		if (SDL_LockMutex(mutex) == 0) {
			while (DSound_PreCounter >= 10000000L) {
				length++;
				DSound_PreCounter -= 10000000L;
			}
			Update(length);
			SDL_UnlockMutex(mutex);
		}
	}
}


void FASTCALL
DSound_Send(void)
{
	// 本来は sdlaudio_callback内で足りないサンプルを補償したいが、
	// スレッドが異なるので、メインスレッドで埋めておく
	if (audio_fd >= 0) {
		if (SDL_LockMutex(mutex) == 0) {
			if (pcmindex < pcmbuffers) {
				Update((pcmbuffers - pcmindex) / 4);
			}
			SDL_UnlockMutex(mutex);
		}
	}
}

static void
sdlaudio_callback(void *userdata, unsigned char *stream, int len)
{
	if (SDL_LockMutex(mutex) == 0) {
		if (len > pcmindex) {
			len = pcmindex;
		}
		if (len > 0) {
			SDL_MixAudio(stream, pcmbuffer, len, SDL_MIX_MAXVOLUME);

			const int remain = pcmindex - len;
			if (remain > 0) {
				memmove(pcmbuffer, pcmbuffer + len, remain);
			}
			pcmindex = remain;
		}
		SDL_UnlockMutex(mutex);
	}
}

// ---------------------------------------------------------------------------
//  その他
// ---------------------------------------------------------------------------

static DWORD
calc_blocksize(DWORD size)
{
	DWORD s = size;

	if (size & (size - 1))
		for (s = 32; s < size; s <<= 1)
			continue;
	return s;
}

#else	/* NOSOUND */
int
DSound_Init(unsigned long rate, unsigned long buflen)
{

	return FALSE;
}

void
DSound_Play(void)
{
}

void
DSound_Stop(void)
{
}

int
DSound_Cleanup(void)
{

	return TRUE;
}

void FASTCALL
DSound_Send0(long clock)
{
}

void FASTCALL
DSound_Send(void)
{
}
#endif	/* !NOSOUND */

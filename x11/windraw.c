/*	$Id: windraw.c,v 1.3 2008/11/08 01:42:42 nonaka Exp $	*/

/* 
 * Copyright (c) 2003 NONAKA Kimihiro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "common.h"
#include "winx68k.h"
#include "winui.h"

#include "mouse.h"
#include "prop.h"
#include "status.h"
#include "../x68k/crtc.h"
#include "../x68k/palette.h"
#include "../x68k/tvram.h"

#include "../icons/keropi.xpm"

WORD *ScrBuf = 0;

int Draw_Opaque;
int FullScreenFlag = 0;
extern BYTE Draw_RedrawAllFlag;
BYTE Draw_DrawFlag = 1;
BYTE Draw_ClrMenu = 0;

BYTE Draw_BitMask[800];
BYTE Draw_TextBitMask[800];

int winx = 0, winy = 0;
DWORD winh = 0, winw = 0;
gint root_width, root_height;
WORD FrameCount = 0;
int SplashFlag = 0;

WORD WinDraw_Pal16B, WinDraw_Pal16R, WinDraw_Pal16G;

DWORD WindowX = 0;
DWORD WindowY = 0;

GdkImage *surface;
GdkRectangle surface_rect = { 16, 16, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT };
GdkImage *scaled_screen;
GdkPixmap *pixmap;
GdkPixmap *splash_pixmap;
static int screen_mode;
static int real_screen_depth = 0;
static uint32_t pal15to32[0x8000];

GdkImage *gdk_scale_image(GdkImage *dest, GdkImage *src, GdkRectangle *dest_rect, GdkRectangle *src_rect);


void WinDraw_InitWindowSize(WORD width, WORD height)
{
	static BOOL inited = FALSE;

	if (!inited) {
		GdkWindow *t, *root = window->window;

		while ((t = gdk_window_get_parent(root)) != 0)
			root = t;
		gdk_window_get_size(root, &root_width, &root_height);
		inited = TRUE;
	}

	gdk_window_get_position(window->window, &winx, &winy);

	winw = width;
	winh = height;

	if (root_width < winw)
		winx = (root_width - winw) / 2;
	else if (winx < 0)
		winx = 0;
	else if ((winx + winw) > root_width)
		winx = root_width - winw;
	if (root_height < winh)
		winy = (root_height - winh) / 2;
	else if (winy < 0)
		winy = 0;
	else if ((winy + winh) > root_height)
		winy = root_height - winh;
}

void WinDraw_ChangeSize(void)
{
	DWORD oldx = WindowX, oldy = WindowY;
	int dif;

	Mouse_ChangePos();

	switch (Config.WinStrech) {
	case 0:
		WindowX = TextDotX;
		WindowY = TextDotY;
		break;

	case 1:
		WindowX = 768;
		WindowY = 512;
		break;

	case 2:
		if (TextDotX <= 384)
			WindowX = TextDotX * 2;
		else
			WindowX = TextDotX;
		if (TextDotY <= 256)
			WindowY = TextDotY * 2;
		else
			WindowY = TextDotY;
		break;

	case 3:
		if (TextDotX <= 384)
			WindowX = TextDotX * 2;
		else
			WindowX = TextDotX;
		if (TextDotY <= 256)
			WindowY = TextDotY * 2;
		else
			WindowY = TextDotY;
		dif = WindowX - WindowY;
		if ((dif > -32) && (dif < 32)) {
			// 正方形に近い画面なら、としておこう
			WindowX = (int)(WindowX * 1.25);
		}
		break;
	}

	if ((WindowX > 768) || (WindowX <= 0)) {
		if (oldx)
			WindowX = oldx;
		else
			WindowX = oldx = 768;
	}
	if ((WindowY > 512) || (WindowY <= 0)) {
		if (oldy)
			WindowY = oldy;
		else
			WindowY = oldy = 512;
	}

	surface_rect.width = TextDotX;
	surface_rect.height = TextDotY;

	if ((oldx == WindowX) && (oldy == WindowY))
		return;

	if ((TextDotX == WindowX) && (TextDotY == WindowY))
		screen_mode = 0;
	else {
		screen_mode = 1;
		if (scaled_screen)
			gdk_image_destroy(scaled_screen);
		scaled_screen = gdk_image_new(GDK_IMAGE_FASTEST,
		    surface->visual, WindowX, WindowY);
	}
	if (surface) {
		bzero(ScrBuf, FULLSCREEN_WIDTH * FULLSCREEN_HEIGHT * 2);
#if 1
		gdk_draw_rectangle(pixmap, window->style->black_gc, TRUE,
		    0, 0, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT);
#else
		gdk_draw_rectangle(pixmap, window->style->black_gc, TRUE,
		    oldx, 0, FULLSCREEN_WIDTH - oldx, FULLSCREEN_HEIGHT);
		gdk_draw_rectangle(pixmap, window->style->black_gc, TRUE,
		    0, oldy, FULLSCREEN_WIDTH - oldx, FULLSCREEN_HEIGHT - oldy);
#endif
	}

	WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
	gtk_widget_set_usize(drawarea, winw, winh);
	gtk_widget_set_uposition(window, winx, winy);
	StatBar_Show(Config.WindowFDDStat);
	Mouse_ChangePos();
}

//static int dispflag = 0;
void WinDraw_StartupScreen(void)
{
}

void WinDraw_CleanupScreen(void)
{
}

void WinDraw_ChangeMode(int flag)
{

	/* full screen mode(TRUE) <-> window mode(FALSE) */
	(void)flag;
}

void WinDraw_ShowSplash(void)
{

	gdk_draw_pixmap(pixmap,
	    drawarea->style->fg_gc[GTK_WIDGET_STATE(drawarea)],
	    splash_pixmap, 0, 0,
	    768 - keropi_xpm_width, 512 - keropi_xpm_height,
	    keropi_xpm_width, keropi_xpm_height);
}

void WinDraw_HideSplash(void)
{

	gdk_draw_rectangle(pixmap, window->style->black_gc, TRUE, 0, 0,
	    FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT);
	Draw_DrawFlag = 1;
}

int WinDraw_Init(void)
{
	GdkVisual *visual;
	GdkColormap *colormap;
	GdkBitmap *mask;

	WindowX = 768;
	WindowY = 512;

	if ((root_width < WindowX) || (root_height < WindowY)) {
		fprintf(stderr, "No support resolution.\n");
		return FALSE;
	}

	visual = gtk_widget_get_visual(drawarea);

	switch (visual->type) {
	case GDK_VISUAL_TRUE_COLOR:
		break;
	case GDK_VISUAL_DIRECT_COLOR:
		if (visual->depth >= 15)
			break;
		/* FALLTHROUGH */
	default:
		fprintf(stderr, "No support visual class.\n");
		return FALSE;
	}

	/* 15 or 16 bpp 以外はサポート外 */
	if (visual->depth == 15 || visual->depth == 16) {
		WinDraw_Pal16R = visual->red_mask;
		WinDraw_Pal16G = visual->green_mask;
		WinDraw_Pal16B = visual->blue_mask;
	}
	else if (visual->depth == 24) {
		real_screen_depth = visual->depth;
		WinDraw_Pal16R = 0x7c00;
		WinDraw_Pal16G = 0x03e0;
		WinDraw_Pal16B = 0x001f;
		for (unsigned int i = 0; i < 0x8000; i++) {
			const uint8_t r5 = (i >> 10) & 0x1f;
			const uint8_t g5 = (i >>  5) & 0x1f;
			const uint8_t b5 = (i >>  0) & 0x1f;
			const uint8_t r8 = (r5 << 3) + (r5 >> 2);
			const uint8_t g8 = (g5 << 3) + (g5 >> 2);
			const uint8_t b8 = (b5 << 3) + (b5 >> 2);
			pal15to32[i] = (r8 << 16) + (g8 << 8) + b8;
		}
	}
	else {
		fprintf(stderr, "No support depth.\n");
		return FALSE;
	}

	surface = gdk_image_new(GDK_IMAGE_FASTEST, visual, FULLSCREEN_WIDTH,
	    FULLSCREEN_HEIGHT);
	if (surface == NULL) {
		g_message("can't create surface.");
		return 1;
	}
	if (real_screen_depth == 0) {
		ScrBuf = (WORD *)(surface->mem);
	}
	else {
		ScrBuf = (WORD *)malloc(FULLSCREEN_WIDTH * FULLSCREEN_HEIGHT * 2);
	}

	pixmap = gdk_pixmap_new(drawarea->window,
	    FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT, visual->depth);
	if (pixmap == NULL) {
		g_message("can't create pixmap.");
		return FALSE;
	}
	gdk_draw_rectangle(pixmap, window->style->black_gc, TRUE, 0, 0,
	    FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT);

	/* けろぴーすぷらっしゅ用意 */
	colormap = gtk_widget_get_colormap(window);
	splash_pixmap = gdk_pixmap_colormap_create_from_xpm_d(NULL, colormap,
	    &mask, NULL, keropi_xpm);
	if (splash_pixmap == NULL)
		g_error("Couldn't create replacement pixmap.");

	return TRUE;
}

void
WinDraw_Cleanup(void)
{

	if (splash_pixmap) {
		gdk_pixmap_unref(splash_pixmap);
		splash_pixmap = 0;
	}
	if (pixmap) {
		gdk_pixmap_unref(pixmap);
		pixmap = 0;
	}
	if (scaled_screen) {
		gdk_image_destroy(scaled_screen);
		scaled_screen = 0;
	}
	if (surface) {
		gdk_image_destroy(surface);
		surface = 0;
		if (real_screen_depth) {
			free(ScrBuf);
		}
		ScrBuf = 0;
	}
	gdk_window_get_position(window->window, &winx, &winy);
}

void
WinDraw_Redraw(void)
{

	TVRAM_SetAllDirty();
}

void FASTCALL
WinDraw_Draw(void)
{
	GtkWidget *w = (GtkWidget *)drawarea;
	GdkDrawable *d = (GdkDrawable *)drawarea->window;

	FrameCount++;
	if (!Draw_DrawFlag && is_installed_idle_process())
		return;
	Draw_DrawFlag = 0;

	if (SplashFlag)
		WinDraw_ShowSplash();

	if (screen_mode == 0) {
		gdk_draw_pixmap(d, w->style->fg_gc[GTK_WIDGET_STATE(w)],
		    pixmap, 0, 0, 0, 0, TextDotX, TextDotY);
	} else {
		if (real_screen_depth == 24) {
			uint32_t* ptr = (uint32_t *)(surface->mem);
			int adr = surface_rect.x + (surface_rect.y * FULLSCREEN_HEIGHT);
			for (unsigned int y = 0; y < surface_rect.height; y++) {
				for (unsigned int x = 0; x < surface_rect.width; x++) {
					const unsigned int c = ScrBuf[x + adr];
					ptr[x + adr] = pal15to32[c & 0x7fff];
				}
				adr += FULLSCREEN_HEIGHT;
			}
		}
		gdk_scale_image(scaled_screen, surface, NULL, &surface_rect);
		gdk_draw_image(d, w->style->fg_gc[GTK_WIDGET_STATE(w)],
		    scaled_screen, 0, 0, 0, 0, WindowX, WindowY);
	}
}

void WinDraw_DrawLine(uint32_t v)
{
	unsigned int adr;

	if (v >= _countof(TextDirtyLine)) return;
	if (!TextDirtyLine[v]) return;
	TextDirtyLine[v] = 0;

	adr = 16 + ((v + 16) * FULLSCREEN_WIDTH);
	CRTC_DrawLine(&ScrBuf[adr], v);

	Draw_DrawFlag = 1;

	switch (screen_mode) {
	case 0:
		if (real_screen_depth == 24) {
			uint32_t* ptr = (uint32_t *)(surface->mem) + adr;
			for (unsigned int i = 0; i < WindowX; i++) {
				const unsigned int c = ScrBuf[adr + i];
				ptr[i] = pal15to32[c & 0x7fff];
			}
		}
		gdk_draw_image(pixmap,
		    drawarea->style->fg_gc[GTK_WIDGET_STATE(drawarea)],
		    surface, 16, 16 + v, 0, v, WindowX, 1);
		break;
	}
}


/* ----- */

/**
 * 最大公約数を求める
 */
unsigned int
gcd(unsigned int v0, unsigned int v1)
{
#if 0
/*
 * ユークリッドの互除法版
 */
	unsigned int t;

	if (v0 == 0 || v1 == 0)
		return 0;
	if (v0 == 1 || v1 == 1)
		return 1;

	if (v0 < v1)
		t = v0, v0 = v1, v1 = t;

	for (; t = v0 % v1; v0 = v1, v1 = t)
		continue;
	return v1;
#else
/*
 * Brent の改良型アルゴリズム
 */
	unsigned int c;
	unsigned int d;
	unsigned int t;

	if (v0 == 0 || v1 == 0)
		return 0;
	if (v0 == 1 || v1 == 1)
		return 1;
	if (v0 == v1)
		return v0;

	for (c = 0; !(v0 & 1) && !(v1 & 1); v0 >>= 1, v1 >>= 1, ++c)
		continue;

	while (!(v0 & 1))
		v0 >>= 1;
	while (!(v1 & 1))
		v1 >>= 1;
	if (v0 < v1)
		t = v0, v0 = v1, v1 = t;

	for (; (d = v0 - v1) != 0; v0 = v1, v1 = d) {
		while ((d & 1) == 0)
			d >>= 1;
		if (v1 < d)
			t = v1, v1 = d, d = t;
	}
	return v1 << c;
#endif
}

static void expand16_fast(GdkImage *dest, GdkImage *src, GdkRectangle *dr, GdkRectangle *sr, int ratio[4]);

GdkImage *
gdk_scale_image(GdkImage *dest, GdkImage *src, GdkRectangle *dest_rect, GdkRectangle *src_rect)
{
	GdkRectangle dr, sr;
	GdkImage *_new = dest;
	int ratio[4];
	int x_gcd, y_gcd;

	g_return_val_if_fail(src, 0);
	g_return_val_if_fail((src->visual->depth == 15 || src->visual->depth == 16), 0);

	if (dest_rect == 0) {
		GdkImage *p = dest ? dest : src;
		dr.x = dr.y = 0;
		dr.width = p->width;
		dr.height = p->height;
	} else 
		dr = *dest_rect;

	if (src_rect == 0) {
		sr.x = sr.y = 0;
		sr.width = src->width;
		sr.height = src->height;
	} else {
		sr = *src_rect;
		g_return_val_if_fail((sr.x >= 0 && sr.y >= 0), 0);
	}

	if (_new == 0) {
		_new = gdk_image_new(GDK_IMAGE_FASTEST, src->visual, dr.width + (dr.x > 0 ? dr.x : 0), dr.height + (dr.y > 0 ? dr.y : 0));
		g_return_val_if_fail(_new, 0);
	}

	x_gcd = gcd(sr.width, dr.width);
	y_gcd = gcd(sr.height, dr.height);

	if (x_gcd >= 2 && y_gcd >= 2)
		/* continue */;
	else {
		if (dest == 0)
			gdk_image_destroy(_new);
		g_return_val_if_fail((x_gcd >= 2 && y_gcd >= 2), 0);
	}

	ratio[0] /* dx_ratio */ = dr.width / x_gcd;
	ratio[1] /* sx_ratio */ = sr.width / x_gcd;
	ratio[2] /* dy_ratio */ = dr.height / y_gcd;
	ratio[3] /* sy_ratio */ = sr.height / y_gcd;
#if 0
	if (ratio[0] >= ratio[1] && ratio[2] >= ratio[3])
		/* continue */;
	else {
		printf("sr(width, height) = (%d, %d), dr(width, height) = (%d, %d)\n", sr.width, sr.height, dr.width, dr.height);
		printf("gcd(x, y) = (%d, %d)\n", x_gcd, y_gcd);
		printf("ratio(0, 1, 2, 3) = (%d, %d, %d, %d)\n", ratio[0], ratio[1], ratio[2], ratio[3]);
		if (dest == 0)
			gdk_image_destroy(_new);
		g_return_val_if_fail((ratio[0] >= ratio[1] && ratio[2] >= ratio[3]), NULL);
	}
#endif
	expand16_fast(_new, src, &dr, &sr, ratio);

	return _new;
}

static void
expand16_fast(GdkImage *dest, GdkImage *src, GdkRectangle *dr, GdkRectangle *sr, int ratio[4])
{
	guint16 *dp, *sp;
	int dest_right, dest_bottom;
	int sx, sy;
	int dx, dy;
	int x_ratio = 0, y_ratio = 0;

	dest_right = dr->x + dr->width;
	dest_bottom = dr->y + dr->height;
	if (dest_right < 0 && dest_bottom < 0)
		return;

	for (sy = sr->y, dy = dr->y; dy < dest_bottom; ++dy) {
		dp = (guint16 *)(dest->mem + dest->bpl * dy);
		sp = (guint16 *)(src->mem + src->bpl * sy);
		for (sx = sr->x, dx = dr->x; dx < dest_right; ++dx) {
			if (dx >= 0 && dy >= 0)
				dp[dx] = sp[sx];

			x_ratio += ratio[1] /* sx_ratio */;
			if (ratio[0] /* dx_ratio */ <= x_ratio) {
				++sx;
				x_ratio -= ratio[0] /* dx_ratio */;
			}
		}

		y_ratio += ratio[3] /* sy_ratio */;
		if (ratio[2] /* dy_ratio */ <= y_ratio) {
			++sy;
			y_ratio -= ratio[2] /* dy_ratio */;
		}
	}
}

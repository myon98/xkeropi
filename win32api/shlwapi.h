/**
 * @file	shlwapi.h
 * @brief	パス関係クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

LPTSTR PathAddBackslash(LPTSTR pszPath);
BOOL PathAppend(LPTSTR pszPath, LPCTSTR pszMore);
LPTSTR PathCombine(LPTSTR pszDest, LPCTSTR pszDir, LPCTSTR pszFile);
LPTSTR PathFindExtension(LPCTSTR pszPath);
LPTSTR PathFindFileName(LPCTSTR pszPath);

#ifdef __cplusplus
}
#endif	// __cplusplus

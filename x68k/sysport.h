#ifndef _winx68k_sysport
#define _winx68k_sysport

extern	BYTE	SysPort[7];

void SysPort_Init(void);
EXTERNC BYTE FASTCALL SysPort_Read(DWORD adr);
EXTERNC void FASTCALL SysPort_Write(DWORD adr, BYTE data);

#endif

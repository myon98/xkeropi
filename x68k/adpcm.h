#ifndef _winx68k_adpcm_h
#define _winx68k_adpcm_h

void FASTCALL ADPCM_PreUpdate(DWORD clock);
EXTERNC void FASTCALL ADPCM_Update(signed short *buffer, DWORD length);

EXTERNC void FASTCALL ADPCM_Write(DWORD adr, BYTE data);
EXTERNC BYTE FASTCALL ADPCM_Read(DWORD adr);

void ADPCM_SetVolume(BYTE vol);
void ADPCM_SetPan(int n);
EXTERNC void ADPCM_SetClock(int n);

void ADPCM_Init(DWORD samplerate);
int ADPCM_IsReady(void);

#endif

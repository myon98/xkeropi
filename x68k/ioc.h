#ifndef _winx68k_ioc
#define _winx68k_ioc

extern	BYTE	IOC_IntStat;
extern	BYTE	IOC_IntVect;

void IOC_Init(void);
EXTERNC BYTE FASTCALL IOC_Read(DWORD adr);
EXTERNC void FASTCALL IOC_Write(DWORD adr, BYTE data);

#endif

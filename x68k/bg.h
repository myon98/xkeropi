#ifndef _winx68k_bg
#define _winx68k_bg

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	BG[0x8000];
extern	BYTE	Sprite_Regs[0x800];
extern	BYTE	BG_Regs[0x12];
extern	WORD	BG_BG0TOP;
extern	WORD	BG_BG1TOP;
extern	BYTE	BG_CHRSIZE;
extern	DWORD	BG0ScrollX, BG0ScrollY;
extern	DWORD	BG1ScrollX, BG1ScrollY;

extern	int32_t	BG_HAdjust;
extern	int32_t	BG_VAdjust;

extern	BYTE	BGCHR8[8*8*256];
extern	BYTE	BGCHR16[16*16*256];

extern	WORD	BG_LineBuf[1600];
extern	WORD	BG_PriBuf[1600];

void BG_Init(void);

BYTE FASTCALL BG_Read(DWORD adr);
void FASTCALL BG_Write(DWORD adr, BYTE data);

uint32_t BG_CalcLine(uint32_t v);
void BG_DrawLine(uint32_t v, uint_fast8_t opaq, uint_fast8_t gd);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_bg

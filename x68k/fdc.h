#ifndef _winx68k_fdc
#define _winx68k_fdc

void FDC_Init(void);
EXTERNC BYTE FASTCALL FDC_Read(DWORD adr);
EXTERNC void FASTCALL FDC_Write(DWORD adr, BYTE data);
short FDC_Flush(void);
void FDC_EPhaseEnd(void);
EXTERNC void FDC_SetForceReady(int n);
int FDC_IsDataReady(void);

#endif //_winx68k_fdc


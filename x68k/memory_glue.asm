        BITS 32

        SECTION .text

;
; ブリッジ マクロ
;
%macro	BusCallBridge	1
		; Write
		global	@%1_Write@8
		extern	%1_Write
		ALIGN	4
@%1_Write@8:	push	edx
		push	ecx
		call	%1_Write
		add	esp, byte 8
		ret
		; Read
		global	@%1_Read@4
		extern	%1_Read
		ALIGN	4
@%1_Read@4:	push	ecx
		call	%1_Read
		add	esp, byte 4
		ret
%endmacro

	BusCallBridge	ADPCM
	BusCallBridge	BG
	BusCallBridge	CRTC
	BusCallBridge	DMA
	BusCallBridge	FDC
	BusCallBridge	GVRAM
	BusCallBridge	IOC
%ifndef NO_MERCURY
	BusCallBridge	Mcry
%endif	; !NO_MERCURY
	BusCallBridge	MFP
%ifndef NO_MIDI
	BusCallBridge	MIDI
%endif	; !NO_MIDI
	BusCallBridge	OPM
	BusCallBridge	Pal
	BusCallBridge	PIA
	BusCallBridge	RTC
	BusCallBridge	SASI
	BusCallBridge	SCC
	BusCallBridge	SCSI
	BusCallBridge	SRAM
	BusCallBridge	SysPort
	BusCallBridge	TVRAM
	BusCallBridge	VCtrl
%ifndef NO_WINDRV
	BusCallBridge	WinDrv
%endif	; !NO_WINDRV

;
; メモリ
;
%macro	StdToFast1	1
	global	%1
	extern	@%1@4
	ALIGN 4
%1:	mov	ecx, [esp + 4]
	jmp	@%1@4
%endmacro

%macro	StdToFast2	1
	global	%1
	extern	@%1@8
	ALIGN 4
%1:	mov	ecx, [esp + 4]
	mov	edx, [esp + 8]
	jmp	@%1@8
%endmacro

	StdToFast1	cpu_setOPbase24

	StdToFast1	dma_readmem24
	StdToFast1	dma_readmem24_word
	StdToFast1	dma_readmem24_dword
	StdToFast2	dma_writemem24
	StdToFast2	dma_writemem24_word
	StdToFast2	dma_writemem24_dword


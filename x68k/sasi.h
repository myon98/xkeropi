#ifndef _winx68k_sasi
#define _winx68k_sasi

#ifdef WIN68DEBUG
extern uint8_t hddtrace;
#endif	// WIN68DEBUG

void SASI_Init(void);
EXTERNC BYTE FASTCALL SASI_Read(DWORD adr);
EXTERNC void FASTCALL SASI_Write(DWORD adr, BYTE data);
int SASI_IsReady(void);

#endif //_winx68k_sasi

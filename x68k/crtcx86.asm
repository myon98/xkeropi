
%include 'x68k.inc'

%ifidn __OUTPUT_FORMAT__, win32
%define	CRTC_DrawGrpLine	_CRTC_DrawGrpLine
%define	CRTC_DrawGrpLineNonSP	_CRTC_DrawGrpLineNonSP
%define	CRTC_DrawTextLine	_CRTC_DrawTextLine
%define	CRTC_DrawTextLineTR	_CRTC_DrawTextLineTR
%define	CRTC_DrawTextLineTR2	_CRTC_DrawTextLineTR2
%define	CRTC_DrawBGLine		_CRTC_DrawBGLine
%define	CRTC_DrawBGLineTR	_CRTC_DrawBGLineTR
%define	CRTC_DrawPriLine	_CRTC_DrawPriLine
%define	CRTC_DrawTRLine		_CRTC_DrawTRLine
%endif

	bits	32
	section	.text

	global	CRTC_DrawGrpLine
	align	16
CRTC_DrawGrpLine:
	push	ebx
	mov	ebx, [esp + 4 + 4]
	mov	edx, Grp_LineBuf
	mov	ecx, [TextDotX]
	cmp	byte [esp + 4 + 8], 0
	je	short .drawgrplinelp

	shr	ecx, 1
.drawgrplineolp:
		mov	eax, [edx]
		mov	[ebx], eax
		add	edx, byte 4
		add	ebx, byte 4
		loop	.drawgrplineolp
		pop	ebx
		ret

.drawgrplinelp:
		mov	ax, [edx]
		or	ax, ax
		jz	short .drawgrplineskip
		mov	[ebx], ax
.drawgrplineskip:
		add	edx, byte 2
		add	ebx, byte 2
		loop	.drawgrplinelp
		pop	ebx
		ret

	global	CRTC_DrawGrpLineNonSP
	align	16
CRTC_DrawGrpLineNonSP:
	push	ebx
	mov	ebx, [esp + 4 + 4]
	cmp	byte [esp + 4 + 8], 0
	mov	edx, Grp_LineBufSP2
	mov	ecx, [TextDotX]
	je	short .drawgrpnslinelp
.drawgrpnslinelpo:
		mov	ax, [edx]
		mov	[ebx], ax
		add	edx, byte 2
		add	ebx, byte 2
		loop	.drawgrpnslinelpo
		pop	ebx
		ret

.drawgrpnslinelp:
		mov	ax, [edx]
		or	ax, ax
		jz	short .drawgrpnslineskip
		mov	[ebx], ax
.drawgrpnslineskip:
		add	edx, byte 2
		add	ebx, byte 2
		loop	.drawgrpnslinelp
		pop	ebx
		ret

	global	CRTC_DrawTextLine
	align	16
CRTC_DrawTextLine:
	push	ebx
	mov	ebx, [esp + 4 + 4]
	xor	edx, edx
	mov	ecx, [TextDotX]
	cmp	byte [esp + 4 + 8], 0
	je	short .noopaq

.drawtextlineolp:
		mov	ax, [BG_LineBuf + 32 + edx*2]
		mov	[ebx], ax
		inc	edx
		add	ebx, 2
		loop	.drawtextlineolp
		pop	ebx
		ret

.noopaq:
	cmp	byte [esp + 4 + 12], 0
	je	short .drawtextlinelp

.drawtextlinetdlp:
		test	byte [Text_TrFlag + 16 + edx], 1
		jz	short .drawtextlinetdskip
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawtextlinetdskip
		mov	[ebx], ax
.drawtextlinetdskip:
		inc	edx
		add	ebx, 2
		loop	.drawtextlinetdlp
		pop	ebx
		ret

.drawtextlinelp:
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawtextlineskip
		mov	[ebx], ax
.drawtextlineskip:
		inc	edx
		add	ebx, byte 2
		loop	.drawtextlinelp
		pop	ebx
		ret

	global	CRTC_DrawTextLineTR
	align	16
CRTC_DrawTextLineTR:
	push	ebx
	push	edi
	mov	ebx, [esp + 8 + 4]
	xor	edx, edx
	xor	eax, eax
	cmp	byte [esp + 8 + 8], 0
	mov	ecx, [TextDotX]
	je	short .noopaq

.drawtexttrlineolp:
		mov	di, [Grp_LineBufSP + edx*2]
		or	di, di
		jnz	short .drawtexttrlineotr
		test	byte [Text_TrFlag + 16 + edx], 1
		jz	short .drawtexttrlineopal0
		mov	ax, [BG_LineBuf + 32 + edx*2]
		jmp	short .drawtexttrlineonorm
.drawtexttrlineopal0:
		xor	ax, ax
		jmp	short .drawtexttrlineonorm
.drawtexttrlineotr:		; ねこーねこー
		mov	ax, word [BG_LineBuf + 32 + edx*2]
		and	di, [Pal_HalfMask]
		test	ax, [Ibit]
		jz	short .drawtexttrlineotrI
		add	di, [Pal_Ix2]
.drawtexttrlineotrI:
		and	ax, [Pal_HalfMask]
		add	ax, di		; 17bit計算中
		rcr	ax, 1		; 17bit計算中
.drawtexttrlineonorm:
		mov	[ebx], ax
		inc	edx
		add	ebx, byte 2
		dec	cx
		jnz	short .drawtexttrlineolp
		pop	edi
		pop	ebx
		ret

.noopaq:
.drawtexttrlinelp:
		test	byte [Text_TrFlag + 16 + edx], 1
		jz	short .drawtexttrlineskip

		mov	di, [Grp_LineBufSP + edx*2]
		or	di, di
		jnz	short .drawtexttrlinetr

		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawtexttrlineskip
		jmp	short .drawtexttrlinenorm

.drawtexttrlinetr:			; ねこーねこー
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawtexttrlineskip
		and	di, [Pal_HalfMask]
		test	ax, [Ibit]
		jz	short .drawtexttrlinetrI
		add	di, [Pal_Ix2]
.drawtexttrlinetrI:
		and	ax, [Pal_HalfMask]
		add	ax, di		; 17bit計算中
		rcr	ax, 1		; 17bit計算中
.drawtexttrlinenorm:
		mov	[ebx], ax
.drawtexttrlineskip:
		inc	edx
		add	ebx, byte 2
		dec	cx
		jnz	short .drawtexttrlinelp
		pop	edi
		pop	ebx
		ret

	global	CRTC_DrawTextLineTR2
	align	16
CRTC_DrawTextLineTR2:
	push	ebx
	push	edi
	mov	ebx, [esp + 8 + 4]
	xor	edx, edx
	xor	edi, edi
	xor	eax, eax
	mov	ecx, [TextDotX]
	cmp	byte [esp + 8 + 8], 0
	je	short .noopaq

.drawtexttrline2olp:
		mov	di, [Grp_LineBufSP + edx*2]
		or	di, di
		jnz	short .drawtexttrline2otr

		test	byte [Text_TrFlag + 16 + edx], 1
		jz	short .drawtexttrline2opal0

		mov	ax, [BG_LineBuf + 32 + edx*2]
		jmp	short .drawtexttrline2onorm

.drawtexttrline2opal0:
		xor	ax, ax
		jmp	short .drawtexttrline2onorm

.drawtexttrline2otr:		; ねこーねこー
		mov	ax, [BG_LineBuf + 32 + edx*2]
		and	di, [Pal_HalfMask]
		test	ax, [Ibit]
		jz	short .drawtexttrline2otrI
		add	di, [Pal_Ix2]
.drawtexttrline2otrI:
		and	ax, [Pal_HalfMask]
		add	ax, di		; 17bit計算中
		rcr	ax, 1		; 17bit計算中
.drawtexttrline2onorm:
		mov	[ebx], ax
		inc	edx
		add	ebx, byte 2
		dec	cx
		jnz	short .drawtexttrline2olp
		pop	edi
		pop	ebx
		ret

.noopaq:
.drawtexttrline2lp:
		test	byte [Text_TrFlag + 16 + edx], 1
		jz	short .drawtexttrline2skip

		mov	di, [Grp_LineBufSP + edx*2]
		or	di, di
		jnz	short .drawtexttrline2tr

		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawtexttrline2skip
		jmp	short .drawtexttrline2norm

.drawtexttrline2tr:			; ねこーねこー
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawtexttrline2skip
		and	di, [Pal_HalfMask]
		test	ax, [Ibit]
		jz	short .drawtexttrline2trI
		add	di, [Pal_Ix2]
.drawtexttrline2trI:
		and	ax, [Pal_HalfMask]
		add	ax, di		; 17bit計算中
		rcr	ax, 1		; 17bit計算中
.drawtexttrline2norm:
		mov	[ebx], ax
.drawtexttrline2skip:
		inc	edx
		add	ebx, byte 2
		dec	cx
		jnz	short .drawtexttrline2lp
		pop	edi
		pop	ebx
		ret

	global	CRTC_DrawBGLine
	align	16
CRTC_DrawBGLine:
	push	ebx
	mov	ebx, [esp + 4 + 4]
	xor	edx, edx
	mov	ecx, [TextDotX]
	cmp	byte [esp + 4 + 8], 0
	je	short .noopaq

.drawbglineolp:
		mov	ax, [BG_LineBuf + 32 + edx*2]
		mov	[ebx], ax
		inc	edx
		add	ebx, byte 2
		loop	.drawbglineolp
		pop	ebx
		ret

.noopaq:
	cmp	byte [esp + 4 + 12], 0
	je	short .notd

.drawbglinetdlp:
		test	byte [Text_TrFlag + 16 + edx], 2
		jz	short .drawbglinetdskip
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawbglinetdskip
		mov	[ebx], ax
.drawbglinetdskip:
		inc	edx
		add	ebx, byte 2
		loop	.drawbglinetdlp
		pop	ebx
		ret

.notd:
.drawbglinelp:
		mov	ax, word [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawbglineskip
		mov	[ebx], ax
.drawbglineskip:
		inc	edx
		add	ebx, byte 2
		loop	.drawbglinelp
		pop	ebx
		ret

	global	CRTC_DrawBGLineTR
	align	16
CRTC_DrawBGLineTR:
	push	ebx
	push	edi
	mov	ebx, [esp + 8 + 4]
	xor	edx, edx
	xor	edi, edi
	xor	eax, eax
	mov	ecx, [TextDotX]
	cmp	byte [esp + 8 + 8], 0
	je	short .drawbgtrlinelp

.drawbgtrlineolp:
		mov	di, [Grp_LineBufSP + edx*2]
		or	di, di
		jnz	short .drawbgtrlineotr
		mov	ax, [BG_LineBuf + 32 + edx*2]
		jmp	short .drawbgtrlineonorm
.drawbgtrlineotr:
		mov	ax, [BG_LineBuf + 32 + edx*2]
		and	di, [Pal_HalfMask]
		test	ax, [Ibit]
		jz	short .drawbgtrlineotrI
		add	di, [Pal_Ix2]
.drawbgtrlineotrI:
		and	ax, [Pal_HalfMask]
		add	ax, di		; 17bit計算中
		rcr	ax, 1		; 17bit計算中
.drawbgtrlineonorm:
		mov	[ebx], ax
		inc	edx
		add	ebx, byte 2
		dec	cx
		jnz	short .drawbgtrlineolp
		pop	edi
		pop	ebx
		ret

.drawbgtrlinelp:
		test	byte [Text_TrFlag + 16 + edx], 2
		jz	short .drawbgtrlineskip
		mov	di, [Grp_LineBufSP + edx*2]
		or	di, di
		jnz	short .drawbgtrlinetr
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawbgtrlineskip
		jmp	short .drawbgtrlinenorm
.drawbgtrlinetr:			; ねこーねこー
		mov	ax, [BG_LineBuf + 32 + edx*2]
		or	ax, ax
		jz	short .drawbgtrlineskip
		and	di, [Pal_HalfMask]
		test	ax, [Ibit]
		jz	short .drawbgtrlinetrI
		add	di, [Pal_Ix2]
.drawbgtrlinetrI:
		and	ax, [Pal_HalfMask]
		add	ax, di		; 17bit計算中
		rcr	ax, 1		; 17bit計算中
.drawbgtrlinenorm:
		mov	[ebx], ax
.drawbgtrlineskip:
		inc	edx
		add	ebx, 2
		dec	cx
		jnz	short .drawbgtrlinelp
		pop	edi
		pop	ebx
		ret

	global	CRTC_DrawPriLine
	align	16
CRTC_DrawPriLine:
	push	ebx
	mov	ebx, [esp + 4 + 4]
	mov	edx, Grp_LineBufSP
	mov	ecx, [TextDotX]
.drawprilinelp:
	mov	ax, [edx]
	or	ax, ax
	jz	short .drawprilineskip
	mov	[ebx], ax
.drawprilineskip:
	add	edx, 2
	add	ebx, 2
	loop	.drawprilinelp
	pop	ebx
	ret

	global	CRTC_DrawTRLine
	align	16
CRTC_DrawTRLine:
	push	ebx
	push	edi
	mov	ebx, [esp + 4 + 8]
	xor	edx, edx
	xor	edi, edi
	xor	eax, eax
	mov	ecx, [TextDotX]
.drawtrlinelp:
	mov	di, [Grp_LineBufSP + edx]
	or	di, di
	jz	short .drawtrlineskip

	mov	ax, [ebx]
	and	di, [Pal_HalfMask]
	test	ax, [Ibit]
	jz	short .drawtrlineoI
	add	di, [Pal_Ix2]
.drawtrlineoI:
	and	ax, [Pal_HalfMask]
	add	ax, di		; 17bit計算中
	rcr	ax, 1		; 17bit計算中
	mov	[ebx], ax
.drawtrlineskip:
	add	edx, byte 2
	add	ebx, byte 2
	dec	cx
	jnz	short .drawtrlinelp
	pop	edi
	pop	ebx
	ret

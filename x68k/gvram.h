#ifndef _winx68k_gvram
#define _winx68k_gvram

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	GVRAM[0x80000];
extern	WORD	Grp_LineBuf[1024];
extern	WORD	Grp_LineBufSP[1024];
extern	WORD	Grp_LineBufSP2[1024];
extern	WORD	Pal16Adr[256];

void GVRAM_Init(void);

void GVRAM_FastClear(void);

BYTE FASTCALL GVRAM_Read(DWORD adr);
void FASTCALL GVRAM_Write(DWORD adr, BYTE data);

void Grp_DrawLine16(uint32_t v);
void Grp_DrawLine8(uint32_t v, uint32_t page, uint_fast8_t opaq);
void Grp_DrawLine4(uint32_t v, uint32_t page, uint_fast8_t opaq);
void Grp_DrawLine4h(uint32_t v);
void Grp_DrawLine16SP(uint32_t v);
void Grp_DrawLine8SP(uint32_t v, uint32_t page/*, uint_fast8_t opaq*/);
void Grp_DrawLine4SP(uint32_t v, uint32_t page/*, uint_fast8_t opaq*/);
void Grp_DrawLine4hSP(uint32_t v);
void Grp_DrawLine8TR(uint32_t v, uint32_t page, uint_fast8_t opaq);
void Grp_DrawLine4TR(uint32_t v, uint32_t page, uint_fast8_t opaq);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_gvram

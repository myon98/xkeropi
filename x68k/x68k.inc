
%ifidn __OUTPUT_FORMAT__, win32

%define	BG		_BG
%define	Sprite_Regs	_Sprite_Regs
%define	BG_Regs		_BG_Regs
%define	BG_BG0TOP	_BG_BG0TOP
%define	BG_BG1TOP	_BG_BG1TOP
%define	BG_CHRSIZE	_BG_CHRSIZE
%define	BG0ScrollX	_BG0ScrollX
%define	BG0ScrollY	_BG0ScrollY
%define	BG1ScrollX	_BG1ScrollX
%define	BG1ScrollY	_BG1ScrollY
%define	BG_HAdjust	_BG_HAdjust
%define	BGCHR8		_BGCHR8
%define	BGCHR16		_BGCHR16
%define	BG_LineBuf	_BG_LineBuf
%define	BG_PriBuf	_BG_PriBuf
%define BG_CalcLine	_BG_CalcLine

%define	CRTC_Regs	_CRTC_Regs
%define	TextDotX	_TextDotX
%define	TextScrollX	_TextScrollX
%define	TextScrollY	_TextScrollY
%define	CRTC_FastClrMask _CRTC_FastClrMask
%define	GrphScrollX	_GrphScrollX
%define	GrphScrollY	_GrphScrollY

%define	GVRAM		_GVRAM
%define Grp_LineBuf	_Grp_LineBuf
%define Grp_LineBufSP	_Grp_LineBufSP
%define Grp_LineBufSP2	_Grp_LineBufSP2
%define Pal16Adr	_Pal16Adr

%define	Pal_Regs	_Pal_Regs
%define	TextPal		_TextPal
%define	GrphPal		_GrphPal
%define	Pal16		_Pal16
%define	Ibit		_Ibit
%define Pal_HalfMask	_Pal_HalfMask
%define	Pal_Ix2		_Pal_Ix2

%define	TextDrawWork	_TextDrawWork
%define	Text_TrFlag	_Text_TrFlag
%endif

extern	BG		; BYTE [0x8000]
extern	Sprite_Regs	; BYTE [0x800]
extern	BG_Regs		; BYTE [0x12]
extern	BG_BG0TOP	; WORD
extern	BG_BG1TOP	; WORD
extern	BG_CHRSIZE	; BYTE
extern	BG0ScrollX	; DWORD
extern	BG0ScrollY	; DWORD
extern	BG1ScrollX	; DWORD
extern	BG1ScrollY	; DWORD
extern	BG_HAdjust	; long
extern	BGCHR8		; BYTE [8*8*256]
extern	BGCHR16		; BYTE [16*16*256]
extern	BG_LineBuf	; WORD [1600]
extern	BG_PriBuf	; WORD [1600]
extern	BG_CalcLine	; DWORD

extern	CRTC_Regs
extern	TextDotX	; DWORD
extern	TextScrollX
extern	TextScrollY
extern	CRTC_FastClrMask
extern	GrphScrollX
extern	GrphScrollY

extern	GVRAM
extern	Grp_LineBuf
extern	Grp_LineBufSP
extern	Grp_LineBufSP2
extern	Pal16Adr

extern	Pal_Regs
extern	TextPal		; WORD [256]
extern	GrphPal
extern	Pal16
extern	Ibit
extern	Pal_HalfMask
extern	Pal_Ix2

extern	TextDrawWork
extern	Text_TrFlag	; BYTE [1024]

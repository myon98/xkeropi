﻿// ---------------------------------------------------------------------------------------
//  CRTC.C - CRT Controller / Video Controller
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	"crtc.h"
#include	"bg.h"
#include	"gvram.h"
#include	"palette.h"
#include	"tvram.h"
#include	"windraw.h"
#include	"winx68k.h"


static const WORD FastClearMask[16] = {
	0xffff, 0xfff0, 0xff0f, 0xff00, 0xf0ff, 0xf0f0, 0xf00f, 0xf000,
	0x0fff, 0x0ff0, 0x0f0f, 0x0f00, 0x00ff, 0x00f0, 0x000f, 0x0000
};
	BYTE	CRTC_Regs[24*2];
	BYTE	CRTC_Mode = 0;
	DWORD	TextDotX = 768, TextDotY = 512;
	WORD	CRTC_VSTART, CRTC_VEND;
	WORD	CRTC_HSTART;
static	WORD	CRTC_HEND;
	DWORD	TextScrollX = 0, TextScrollY = 0;
	DWORD	GrphScrollX[4] = {0, 0, 0, 0};		// 配列にしちゃった…
	DWORD	GrphScrollY[4] = {0, 0, 0, 0};

	BYTE	CRTC_FastClr = 0;
static	DWORD	CRTC_FastClrLine = 0;
	WORD	CRTC_FastClrMask = 0;
	WORD	CRTC_IntLine = 0;
	BYTE	CRTC_VStep = 2;

	BYTE	VCReg0[2] = {0, 0};
	BYTE	VCReg1[2] = {0, 0};
	BYTE	VCReg2[2] = {0, 0};

static	BYTE	CRTC_RCFlag[2] = {0, 0};
	int	HSYNC_CLK = 324;

#ifdef WIN68DEBUG
	uint8_t		Debug_Text = 1, Debug_Grp = 1, Debug_Sp = 1;
#else	// WIN68DEBUG
const	uint8_t		Debug_Text = 1, Debug_Grp = 1, Debug_Sp = 1;
#endif	// WIN68DEBUG

// -----------------------------------------------------------------------
//   らすたーこぴー
// -----------------------------------------------------------------------
static void CRTC_RasterCopy(void)
{
	unsigned int i;
	uint32_t line = (uint32_t)CRTC_Regs[0x2d] << 2;
	const uint8_t* src = &TVRAM[(uint32_t)CRTC_Regs[0x2c] << 9];
	uint8_t* dst = &TVRAM[(uint32_t)CRTC_Regs[0x2d] << 9];
	if (src == dst)
	{
		return;
	}

	if (CRTC_Regs[0x2b] & (1 << 0))
	{
		memcpy(&dst[0x20000 * 0], &src[0x20000 * 0], 512);
	}
	if (CRTC_Regs[0x2b] & (1 << 1))
	{
		memcpy(&dst[0x20000 * 1], &src[0x20000 * 1], 512);
	}
	if (CRTC_Regs[0x2b] & (1 << 2))
	{
		memcpy(&dst[0x20000 * 2], &src[0x20000 * 2], 512);
	}
	if (CRTC_Regs[0x2b] & (1 << 3))
	{
		memcpy(&dst[0x20000 * 3], &src[0x20000 * 3], 512);
	}

	line = line - TextScrollY;
	for (i = 0; i < 4; i++)
	{
		TextDirtyLine[(line + i) & 0x3ff] = 1;
	}

	TVRAM_RCUpdate();
}


// -----------------------------------------------------------------------
//   びでおこんとろーるれじすた
// -----------------------------------------------------------------------
// Reg0の色モードは、ぱっと見CRTCと同じだけど役割違うので注意。
// CRTCはGVRAMへのアクセス方法（メモリマップ上での見え方）が変わるのに対し、
// VCtrlは、GVRAM→画面の展開方法を制御する。
// つまり、アクセス方法（CRTC）は16bitモードで、表示は256色モードってな使い
// 方も許されるのれす。
// コットン起動時やYs（電波版）OPなどで使われてまふ。

BYTE FASTCALL VCtrl_Read(DWORD adr)
{
	BYTE ret = 0xff;
	switch(adr&0x701)
	{
	case 0x400:
	case 0x401:
		ret = VCReg0[adr&1];
		break;
	case 0x500:
	case 0x501:
		ret = VCReg1[adr&1];
		break;
	case 0x600:
	case 0x601:
		ret = VCReg2[adr&1];
		break;
	}
	return ret;
}


void FASTCALL VCtrl_Write(DWORD adr, BYTE data)
{
	switch(adr&0x701)
	{
	case 0x401:
		if (VCReg0[adr&1] != data)
		{
			VCReg0[adr&1] = data;
			TVRAM_SetAllDirty();
		}
		break;
	case 0x500:
	case 0x501:
		if (VCReg1[adr&1] != data)
		{
			VCReg1[adr&1] = data;
			TVRAM_SetAllDirty();
		}
		break;
	case 0x600:
	case 0x601:
		if (VCReg2[adr&1] != data)
		{
			VCReg2[adr&1] = data;
			TVRAM_SetAllDirty();
		}
		break;
	}

/*{
FILE* fp = fopen("_vreg.txt", "a");
fprintf(fp, "VREG 1=$%02X%02X 2=$%02X%02X 3=$%02X%02X\n", VCReg0[0], VCReg0[1], VCReg1[0], VCReg1[1], VCReg2[0], VCReg2[1]);
fclose(fp);
}*/
}


// -----------------------------------------------------------------------
//   CRTCれじすた
// -----------------------------------------------------------------------
// レジスタアクセスのコードが汚い ^^;

void CRTC_Init(void)
{
	ZeroMemory(CRTC_Regs, 48);
	TextScrollX = 0, TextScrollY = 0;
	ZeroMemory(GrphScrollX, sizeof(GrphScrollX));
	ZeroMemory(GrphScrollY, sizeof(GrphScrollY));
}


BYTE FASTCALL CRTC_Read(DWORD adr)
{
	BYTE ret;
	if (adr<0xe803ff) {
		int reg = adr&0x3f;
		if ( (reg>=0x28)&&(reg<=0x2b) ) return CRTC_Regs[reg];
		else return 0;
	} else if ( adr==0xe80481 ) {
// FastClearの注意点：
// FastClrビットに1を書き込むと、その時点ではReadBackしても1は見えない。
// 1書き込み後の最初の垂直帰線期間で1が立ち、消去を開始する。
// 1垂直同期期間で消去がおわり、0に戻る……らしひ（PITAPAT）
		if (CRTC_FastClr)
			ret = CRTC_Mode | 0x02;
		else
			ret = CRTC_Mode & 0xfd;

		return ret;
	}
	else
		return 0x00;
}


void FASTCALL CRTC_Write(DWORD adr, BYTE data)
{
	BYTE reg = (BYTE)(adr&0x3f);
	if (adr<0xe80400)
	{
		if ( reg>=0x30 ) return;
		if (CRTC_Regs[reg]==data) return;
		CRTC_Regs[reg] = data;
		TVRAM_SetAllDirty();
		switch(reg)
		{
		case 0x04:
		case 0x05:
			CRTC_HSTART = (((WORD)CRTC_Regs[0x4]<<8)+CRTC_Regs[0x5]);
			TextDotX = (CRTC_HEND-CRTC_HSTART)*8;
			BG_HAdjust = ((int32_t)BG_Regs[0x0d]-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			WinDraw_ChangeSize();
			break;
		case 0x06:
		case 0x07:
			CRTC_HEND = (((WORD)CRTC_Regs[0x6]<<8)+CRTC_Regs[0x7]);
			TextDotX = (CRTC_HEND-CRTC_HSTART)*8;
			WinDraw_ChangeSize();
			break;
		case 0x08:
		case 0x09:
			VLINE_TOTAL = (((WORD)CRTC_Regs[8]<<8)+CRTC_Regs[9]);
			HSYNC_CLK = ((CRTC_Regs[0x29]&0x10)?VSYNC_HIGH:VSYNC_NORM)/VLINE_TOTAL;
			break;
		case 0x0c:
		case 0x0d:
			CRTC_VSTART = (((WORD)CRTC_Regs[0xc]<<8)+CRTC_Regs[0xd]);
			BG_VAdjust = ((int32_t)BG_Regs[0x0f]-CRTC_VSTART)/((BG_Regs[0x11]&4)?1:2);	// BGとその他がずれてる時の差分
			TextDotY = CRTC_VEND-CRTC_VSTART;
			if ((CRTC_Regs[0x29]&0x14)==0x10)
			{
				TextDotY/=2;
				CRTC_VStep = 1;
			}
			else if ((CRTC_Regs[0x29]&0x14)==0x04)
			{
				TextDotY*=2;
				CRTC_VStep = 4;
			}
			else
				CRTC_VStep = 2;
			WinDraw_ChangeSize();
			break;
		case 0x0e:
		case 0x0f:
			CRTC_VEND = (((WORD)CRTC_Regs[0xe]<<8)+CRTC_Regs[0xf]);
			TextDotY = CRTC_VEND-CRTC_VSTART;
			if ((CRTC_Regs[0x29]&0x14)==0x10)
			{
				TextDotY/=2;
				CRTC_VStep = 1;
			}
			else if ((CRTC_Regs[0x29]&0x14)==0x04)
			{
				TextDotY*=2;
				CRTC_VStep = 4;
			}
			else
				CRTC_VStep = 2;
			WinDraw_ChangeSize();
			break;
		case 0x28:
			TVRAM_SetAllDirty();
			break;
		case 0x29:
			HSYNC_CLK = ((CRTC_Regs[0x29]&0x10)?VSYNC_HIGH:VSYNC_NORM)/VLINE_TOTAL;
			TextDotY = CRTC_VEND-CRTC_VSTART;
			if ((CRTC_Regs[0x29]&0x14)==0x10)
			{
				TextDotY/=2;
				CRTC_VStep = 1;
			}
			else if ((CRTC_Regs[0x29]&0x14)==0x04)
			{
				TextDotY*=2;
				CRTC_VStep = 4;
			}
			else
				CRTC_VStep = 2;
			WinDraw_ChangeSize();
			break;
		case 0x12:
		case 0x13:
			CRTC_IntLine = (((WORD)CRTC_Regs[0x12]<<8)+CRTC_Regs[0x13])&1023;
			break;
		case 0x14:
		case 0x15:
			TextScrollX = (((DWORD)CRTC_Regs[0x14]<<8)+CRTC_Regs[0x15])&1023;
			break;
		case 0x16:
		case 0x17:
			TextScrollY = (((DWORD)CRTC_Regs[0x16]<<8)+CRTC_Regs[0x17])&1023;
			break;
		case 0x18:
		case 0x19:
			GrphScrollX[0] = (((DWORD)CRTC_Regs[0x18]<<8)+CRTC_Regs[0x19])&1023;
			break;
		case 0x1a:
		case 0x1b:
			GrphScrollY[0] = (((DWORD)CRTC_Regs[0x1a]<<8)+CRTC_Regs[0x1b])&1023;
			break;
		case 0x1c:
		case 0x1d:
			GrphScrollX[1] = (((DWORD)CRTC_Regs[0x1c]<<8)+CRTC_Regs[0x1d])&511;
			break;
		case 0x1e:
		case 0x1f:
			GrphScrollY[1] = (((DWORD)CRTC_Regs[0x1e]<<8)+CRTC_Regs[0x1f])&511;
			break;
		case 0x20:
		case 0x21:
			GrphScrollX[2] = (((DWORD)CRTC_Regs[0x20]<<8)+CRTC_Regs[0x21])&511;
			break;
		case 0x22:
		case 0x23:
			GrphScrollY[2] = (((DWORD)CRTC_Regs[0x22]<<8)+CRTC_Regs[0x23])&511;
			break;
		case 0x24:
		case 0x25:
			GrphScrollX[3] = (((DWORD)CRTC_Regs[0x24]<<8)+CRTC_Regs[0x25])&511;
			break;
		case 0x26:
		case 0x27:
			GrphScrollY[3] = (((DWORD)CRTC_Regs[0x26]<<8)+CRTC_Regs[0x27])&511;
			break;
		case 0x2a:
		case 0x2b:
			break;
		case 0x2c:				// CRTC動作ポートのラスタコピーをONにしておいて（しておいたまま）、
		case 0x2d:				// Src/Dstだけ次々変えていくのも許されるらしい（ドラキュラとか）
			CRTC_RCFlag[reg-0x2c] = 1;	// Dst変更後に実行される？
			if ((CRTC_Mode&8)&&/*(CRTC_RCFlag[0])&&*/(CRTC_RCFlag[1]))
			{
				CRTC_RasterCopy();
				CRTC_RCFlag[0] = 0;
				CRTC_RCFlag[1] = 0;
			}
			break;
		}
	}
	else if (adr==0xe80481)
	{					// CRTC動作ポート
		CRTC_Mode = (data|(CRTC_Mode&2));
		if (CRTC_Mode&8)
		{				// Raster Copy
			CRTC_RasterCopy();
			CRTC_RCFlag[0] = 0;
			CRTC_RCFlag[1] = 0;
		}
		if (CRTC_Mode&2)		// 高速クリア
		{
			CRTC_FastClrLine = vline;
						// この時点のマスクが有効らしい（クォース）
			CRTC_FastClrMask = FastClearMask[CRTC_Regs[0x2b]&15];
/*{
FILE *fp;
fp=fopen("_crtc.txt", "a");
fprintf(fp, "FastClr\n");
fclose(fp);
}*/
		}
	}
}

#ifdef USE_ASM
EXTERNC void CRTC_DrawGrpLine(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawGrpLineNonSP(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawTextLine(uint16_t* dst, uint_fast8_t opaq, uint_fast8_t td);
EXTERNC void CRTC_DrawTextLineTR(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawTextLineTR2(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawBGLine(uint16_t* dst, uint_fast8_t opaq, uint_fast8_t td);
EXTERNC void CRTC_DrawBGLineTR(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawPriLine(uint16_t* dst);
EXTERNC void CRTC_DrawTRLine(uint16_t* dst);
#else	// USE_ASM
INLINE void CRTC_DrawGrpLine(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		memcpy(pLine, Grp_LineBuf, TextDotX * 2);
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			const uint16_t w = Grp_LineBuf[i];
			if (w != 0)
			{
				pLine[i] = w;
			}
		}
	}
}

INLINE void CRTC_DrawGrpLineNonSP(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		memcpy(pLine, Grp_LineBufSP2, TextDotX * 2);
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			const uint16_t w = Grp_LineBufSP2[i];
			if (w != 0)
			{
				pLine[i] = w;
			}
		}
	}
}

INLINE void CRTC_DrawTextLine(uint16_t* pLine, uint_fast8_t opaq, uint_fast8_t td)
{
	unsigned int i;
	if (opaq)
	{
		memcpy(pLine, &BG_LineBuf[16], TextDotX * 2);
	}
	else
	{
		if (td)
		{
			for (i = 0; i < TextDotX; i++)
			{
				if (Text_TrFlag[i + 16] & 1)
				{
					const uint16_t w = BG_LineBuf[i + 16];
					if (w != 0)
					{
						pLine[i] = w;
					}
				}
			}
		}
		else
		{
			for (i = 0; i < TextDotX; i++)
			{
				const uint16_t w = BG_LineBuf[i + 16];
				if (w != 0)
				{
					pLine[i] = w;
				}
			}
		}
	}
}

INLINE void CRTC_DrawTextLineTR(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		for (i = 0; i < TextDotX; i++)
		{
			uint32_t v = 0;
			uint16_t w = Grp_LineBufSP[i];
			if (w != 0)
			{
				w &= Pal_HalfMask;
				v = BG_LineBuf[i + 16];
				if (v & Ibit)
				{
					w += Pal_Ix2;
				}
				v &= Pal_HalfMask;
				v += w;
				v >>= 1;
			}
			else if (Text_TrFlag[i + 16] & 1)
			{
				v = BG_LineBuf[i + 16];
			}
			pLine[i] = (uint16_t)v;
		}
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			if (Text_TrFlag[i + 16] & 1)
			{
				uint32_t v = BG_LineBuf[i + 16];
				if (v != 0)
				{
					uint16_t w = Grp_LineBufSP[i];
					if (w != 0)
					{
						w &= Pal_HalfMask;
						if (v & Ibit)
						{
							w += Pal_Ix2;
						}
						v &= Pal_HalfMask;
						v += w;
						v >>= 1;
					}
					pLine[i] = (uint16_t)v;
				}
			}
		}
	}
}

INLINE void CRTC_DrawTextLineTR2(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		for (i = 0; i < TextDotX; i++)
		{
			uint32_t v = 0;
			uint16_t w = Grp_LineBufSP[i];
			if (w != 0)
			{
				w &= Pal_HalfMask;
				v = BG_LineBuf[i + 16];
				if (v & Ibit)
				{
					w += Pal_Ix2;
				}
				v &= Pal_HalfMask;
				v += w;
				v >>= 1;
			}
			else if (Text_TrFlag[i + 16] & 1)
			{
				v = BG_LineBuf[i + 16];
			}
			pLine[i] = (uint16_t)v;
		}
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			if (Text_TrFlag[i + 16] & 1)
			{
				uint32_t v = BG_LineBuf[i + 16];
				if (v != 0)
				{
					uint16_t w = Grp_LineBufSP[i];
					if (w != 0)
					{
						w &= Pal_HalfMask;
						if (v & Ibit)
						{
							w += Pal_Ix2;
						}
						v &= Pal_HalfMask;
						v += w;
						v >>= 1;
					}
					pLine[i] = (uint16_t)v;
				}
			}
		}
	}
}

INLINE void CRTC_DrawBGLine(uint16_t* pLine, uint_fast8_t opaq, uint_fast8_t td)
{
	unsigned int i;
	if (opaq)
	{
		memcpy(pLine, &BG_LineBuf[16], TextDotX * 2);
	}
	else
	{
		if (td)
		{
			for (i = 0; i < TextDotX; i++)
			{
				if (Text_TrFlag[i + 16] & 2)
				{
					const uint16_t w = BG_LineBuf[i + 16];
					if (w != 0)
					{
						pLine[i] = w;
					}
				}
			}
		}
		else
		{
			for (i = 0; i < TextDotX; i++)
			{
				const uint16_t w = BG_LineBuf[i + 16];
				if (w != 0)
				{
					pLine[i] = w;
				}
			}
		}
	}
}

INLINE void CRTC_DrawBGLineTR(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		for (i = 0; i < TextDotX; i++)
		{
			uint32_t v = BG_LineBuf[i + 16];
			uint16_t w = Grp_LineBufSP[i];
			if (w != 0)
			{
				w &= Pal_HalfMask;
				if (v & Ibit)
					w += Pal_Ix2;
				v &= Pal_HalfMask;
				v += w;
				v >>= 1;
			}
			pLine[i] = (uint16_t)v;
		}
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			if (Text_TrFlag[i + 16] & 2)
			{
				uint32_t v = BG_LineBuf[i + 16];
				if (v != 0)
				{
					uint16_t w = Grp_LineBufSP[i];
					if (w != 0)
					{
						w &= Pal_HalfMask;
						if (v & Ibit)
						{
							w += Pal_Ix2;
						}
						v &= Pal_HalfMask;
						v += w;
						v >>= 1;
					}
					pLine[i] = (uint16_t)v;
				}
			}
		}
	}
}

INLINE void CRTC_DrawPriLine(uint16_t* pLine)
{
	unsigned int i;
	for (i = 0; i < TextDotX; i++)
	{
		const uint16_t w = Grp_LineBufSP[i];
		 if (w != 0)
		{
			pLine[i] = w;
		}
	}
}

INLINE void CRTC_DrawTRLine(uint16_t* pLine)
{
	unsigned int i;
	for (i = 0; i < TextDotX; i++)
	{
		uint16_t w = Grp_LineBufSP[i];
		if (w != 0)
		{
			uint32_t v = pLine[i];
			w &= Pal_HalfMask;
			if (v & Ibit)
			{
				w += Pal_Ix2;
			}
			v &= Pal_HalfMask;
			v += w;
			v >>= 1;
			pLine[i] = (uint16_t)v;
		}
	}
}
#endif	// USE_ASM

void CRTC_DrawLine(uint16_t* dst, uint32_t v)
{
	uint_fast8_t opaq, ton=0, gon=0, bgon=0, tron=0, pron=0, tdrawed=0/*, gdrawed=0*/;

	if (Debug_Grp)
	{
	switch(VCReg0[1]&3)
	{
	case 0:					// 16 colors
		if (VCReg0[1]&4)		// 1024dot
		{
			if (VCReg2[1]&0x10)
			{
				if ( (VCReg2[0]&0x14)==0x14 )
				{
					Grp_DrawLine4hSP(v);
					pron = tron = 1;
				}
				else
				{
					Grp_DrawLine4h(v);
					gon=1;
				}
			}
		}
		else				// 512dot
		{
			if ( (VCReg2[0]&0x10)&&(VCReg2[1]&1) )
			{
				Grp_DrawLine4SP(v, (VCReg1[1]   )&3/*, 1*/);			// 半透明の下準備
				pron = tron = 1;
			}
			opaq = 1;
			if (VCReg2[1]&8)
			{
				Grp_DrawLine4(v, (VCReg1[1]>>6)&3, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&4)
			{
				Grp_DrawLine4(v, (VCReg1[1]>>4)&3, opaq);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&2)
			{
				if ( ((VCReg2[0]&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine4TR(v, (VCReg1[1]>>2)&3, opaq);
				else
					Grp_DrawLine4(v, (VCReg1[1]>>2)&3, opaq);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&1)
			{
//				if ( (VCReg2[0]&0x1e)==0x1e )
//				{
//					Grp_DrawLine4SP(v, (VCReg1[1]   )&3, opaq);
//					tron = pron = 1;
//				}
//				else
				if ( (VCReg2[0]&0x14)!=0x14 )
				{
					Grp_DrawLine4(v, (VCReg1[1]   )&3, opaq);
					gon=1;
				}
			}
		}
		break;
	case 1:	
	case 2:	
		opaq = 1;		// 256 colors
		if ( (VCReg1[1]&3) <= ((VCReg1[1]>>4)&3) )	// 同じ値の時は、GRP0が優先（ドラスピ）
		{
			if ( (VCReg2[0]&0x10)&&(VCReg2[1]&1) )
			{
				Grp_DrawLine8SP(v, 0);			// 半透明の下準備
				tron = pron = 1;
			}
			if (VCReg2[1]&4)
			{
				if ( ((VCReg2[0]&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine8TR(v, 1, 1);
				else
					Grp_DrawLine8(v, 1, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&1)
			{
				if ( (VCReg2[0]&0x14)!=0x14 )
				{
					Grp_DrawLine8(v, 0, opaq);
					gon=1;
				}
			}
		}
		else
		{
			if ( (VCReg2[0]&0x10)&&(VCReg2[1]&1) )
			{
				Grp_DrawLine8SP(v, 1);			// 半透明の下準備
				tron = pron = 1;
			}
			if (VCReg2[1]&4)
			{
				if ( ((VCReg2[0]&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine8TR(v, 0, 1);
				else
					Grp_DrawLine8(v, 0, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&1)
			{
				if ( (VCReg2[0]&0x14)!=0x14 )
				{
					Grp_DrawLine8(v, 1, opaq);
					gon=1;
				}
			}
		}
		break;
	case 3:					// 65536 colors
		if (VCReg2[1]&15)
		{
			if ( (VCReg2[0]&0x14)==0x14 )
			{
				Grp_DrawLine16SP(v);
				tron = pron = 1;
			}
			else
			{
				Grp_DrawLine16(v);
				gon=1;
			}
		}
		break;
	}
	}


//	if ( ( ((VCReg1[0]&0x30)>>4) < (VCReg1[0]&0x03) ) && (gon) )
//		gdrawed = 1;				// GrpよりBGの方が上

	if ( ((VCReg1[0]&0x30)>>2) < (VCReg1[0]&0x0c) )
	{						// BGの方が上
		if ((VCReg2[1]&0x20)&&(Debug_Text))
		{
			Text_DrawLine(v, 1);
			ton = 1;
		}
		else
			ZeroMemory(Text_TrFlag, TextDotX+16);

		if ((VCReg2[1]&0x40)&&(BG_Regs[8]&2)&&(!(BG_Regs[0x11]&2))&&(Debug_Sp))
		{
			BG_DrawLine(v, (uint_fast8_t)(ton ^ 1), 0);
			bgon = 1;
		}
	}
	else
	{						// Textの方が上
		if ((VCReg2[1]&0x40)&&(BG_Regs[8]&2)&&(!(BG_Regs[0x11]&2))&&(Debug_Sp))
		{
			ZeroMemory(Text_TrFlag, TextDotX+16);
			BG_DrawLine(v, 1, 1);
			bgon = 1;
		}
		else
		{
			if ((VCReg2[1]&0x20)&&(Debug_Text))
			{
				unsigned int i;
				for (i = 0; i < TextDotX; ++i)
					BG_LineBuf[16 + i] = TextPal[0];
			} else {		// 20010120 （琥珀色）
				ZeroMemory(&BG_LineBuf[16], TextDotX * 2);
			}
			ZeroMemory(Text_TrFlag, TextDotX+16);
			bgon = 1;
		}

		if ((VCReg2[1]&0x20)&&(Debug_Text))
		{
			Text_DrawLine(v, (uint_fast8_t)(bgon ^ 1));
			ton = 1;
		}
	}


	opaq = 1;


#if 0
					// Pri = 3（違反）に設定されている画面を表示
		if ( ((VCReg1[0]&0x30)==0x30)&&(bgon) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x03)&&(tron) )
			{
				if ( (VCReg1[0]&3)<((VCReg1[0]>>2)&3) )
				{
					CRTC_DrawBGLineTR(dst, opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				CRTC_DrawBGLine(dst, opaq, /*tdrawed*/0);
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( ((VCReg1[0]&0x0c)==0x0c)&&(ton) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x0c)&&(tron) )
				CRTC_DrawTextLineTR(dst, opaq);
			else
				CRTC_DrawTextLine(dst, opaq, /*tdrawed*/((VCReg1[0]&0x30)==0x30));
			opaq = 0;
			tdrawed = 1;
		}
#endif
					// Pri = 2 or 3（最下位）に設定されている画面を表示
					// プライオリティが同じ場合は、GRP<SP<TEXT？（ドラスピ、桃伝、YsIII等）

					// GrpよりTextが上にある場合にTextとの半透明を行うと、SPのプライオリティも
					// Textに引きずられる？（つまり、Grpより下にあってもSPが表示される？）

					// KnightArmsとかを見ると、半透明のベースプレーンは一番上になるみたい…。

		if ( (VCReg1[0]&0x02) )
		{
			if (gon)
			{
				CRTC_DrawGrpLine(dst, opaq);
				opaq = 0;
			}
			if (tron)
			{
				CRTC_DrawGrpLineNonSP(dst, opaq);
				opaq = 0;
			}
		}
		if ( (VCReg1[0]&0x20)&&(bgon) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x02)&&(tron) )
			{
				if ( (VCReg1[0]&3)<((VCReg1[0]>>2)&3) )
				{
					CRTC_DrawBGLineTR(dst, opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				CRTC_DrawBGLine(dst, opaq, /*0*/tdrawed);
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( (VCReg1[0]&0x08)&&(ton) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x02)&&(tron) )
				CRTC_DrawTextLineTR(dst, opaq);
			else
				CRTC_DrawTextLine(dst, opaq, tdrawed/*((VCReg1[0]&0x30)>=0x20)*/);
			opaq = 0;
			tdrawed = 1;
		}

					// Pri = 1（2番目）に設定されている画面を表示
		if ( ((VCReg1[0]&0x03)==0x01)&&(gon) )
		{
			CRTC_DrawGrpLine(dst, opaq);
			opaq = 0;
		}
		if ( ((VCReg1[0]&0x30)==0x10)&&(bgon) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&(!(VCReg1[0]&0x03))&&(tron) )
			{
				if ( (VCReg1[0]&3)<((VCReg1[0]>>2)&3) )
				{
					CRTC_DrawBGLineTR(dst, opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				CRTC_DrawBGLine(dst, opaq, (uint_fast8_t)((VCReg1[0]&0xc)==0x8));
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( ((VCReg1[0]&0x0c)==0x04) && ((VCReg2[0]&0x5d)==0x1d) && (VCReg1[0]&0x03) && (((VCReg1[0]>>4)&3)>(VCReg1[0]&3)) && (bgon) && (tron) )
		{
			CRTC_DrawBGLineTR(dst, opaq);
			tdrawed = 1;
			opaq = 0;
			if (tron)
			{
				CRTC_DrawGrpLineNonSP(dst, opaq);
			}
		}
		else if ( ((VCReg1[0]&0x03)==0x01)&&(tron)&&(gon)&&(VCReg2[0]&0x10) )
		{
			CRTC_DrawGrpLineNonSP(dst, opaq);
			opaq = 0;
		}
		if ( ((VCReg1[0]&0x0c)==0x04)&&(ton) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&(!(VCReg1[0]&0x03))&&(tron) )
				CRTC_DrawTextLineTR(dst, opaq);
			else
				CRTC_DrawTextLine(dst, opaq, (uint_fast8_t)((VCReg1[0]&0x30)>=0x10));
			opaq = 0;
			tdrawed = 1;
		}

					// Pri = 0（最優先）に設定されている画面を表示
		if ( (!(VCReg1[0]&0x03))&&(gon) )
		{
			CRTC_DrawGrpLine(dst, opaq);
			opaq = 0;
		}
		if ( (!(VCReg1[0]&0x30))&&(bgon) )
		{
			CRTC_DrawBGLine(dst, opaq, /*tdrawed*/(uint_fast8_t)((VCReg1[0]&0xc)>=0x4));
			tdrawed = 1;
			opaq = 0;
		}
		if ( (!(VCReg1[0]&0x0c)) && ((VCReg2[0]&0x5d)==0x1d) && (((VCReg1[0]>>4)&3)>(VCReg1[0]&3)) && (bgon) && (tron) )
		{
			CRTC_DrawBGLineTR(dst, opaq);
			tdrawed = 1;
			opaq = 0;
			if (tron)
			{
				CRTC_DrawGrpLineNonSP(dst, opaq);
			}
		}
		else if ( (!(VCReg1[0]&0x03))&&(tron)&&(VCReg2[0]&0x10) )
		{
			CRTC_DrawGrpLineNonSP(dst, opaq);
			opaq = 0;
		}
		if ( (!(VCReg1[0]&0x0c))&&(ton) )
		{
			CRTC_DrawTextLine(dst, opaq, 1);
			tdrawed = 1;
			opaq = 0;
		}

					// 特殊プライオリティ時のグラフィック
		if ( ((VCReg2[0]&0x5c)==0x14)&&(pron) )	// 特殊Pri時は、対象プレーンビットは意味が無いらしい（ついんびー）
		{
			CRTC_DrawPriLine(dst);
		}
		else if ( ((VCReg2[0]&0x5d)==0x1c)&&(tron) )	// 半透明時に全てが透明なドットをハーフカラーで埋める
		{						// （AQUALES）
			unsigned int i;
			for (i = 0; i < TextDotX; ++i) {
				const uint16_t w = Grp_LineBufSP[i];
				if ((w != 0) && (dst[i] == 0))
					dst[i] = (w & Pal_HalfMask) >> 1;
			}
		}

	if (opaq)
	{
		ZeroMemory(dst, TextDotX * 2);
	}
}

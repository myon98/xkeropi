#ifndef _winx68k_scsi
#define _winx68k_scsi

EXTERNC	BYTE	SCSIIPL[0x2000];

void SCSI_Init(void);
void SCSI_Cleanup(void);

EXTERNC BYTE FASTCALL SCSI_Read(DWORD adr);
EXTERNC void FASTCALL SCSI_Write(DWORD adr, BYTE data);

#endif


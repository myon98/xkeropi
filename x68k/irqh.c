﻿// ---------------------------------------------------------------------------------------
//  IRQH.C - IRQ Handler (架空のデバイスにょ)
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "irqh.h"
#include "m68000.h"

static	BYTE		IRQH_IRQ[8];
static	DMAHANDLER	IRQH_CallBack[8];

// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void IRQH_Init(void)
{
	ZeroMemory(IRQH_IRQ, 8);
}


// -----------------------------------------------------------------------
//   デフォルトのベクタを返す（これが起こったら変だお）
// -----------------------------------------------------------------------
static DWORD FASTCALL IRQH_DefaultVector(BYTE irq)
{
	IRQH_IRQCallBack(irq);
	return -1;
}


// -----------------------------------------------------------------------
//   他の割り込みのチェック
//   各デバイスのベクタを返すルーチンから呼ばれます
// -----------------------------------------------------------------------
void IRQH_IRQCallBack(BYTE irq)
{
#ifdef USE_68KEM
	int i;
	IRQH_IRQ[irq] = 0;
	regs.IRQ_level = 0;
	for (i=7; i>0; i--)
	{
		if (IRQH_IRQ[i])
		{
			regs.irq_callback = IRQH_CallBack[i];
			regs.IRQ_level = i;
			if ( m68000_ICount ) {					// 多重割り込み時（CARAT）
				m68000_ICountBk += m68000_ICount;		// 強制的に割り込みチェックをさせる
				m68000_ICount = 0;				// 苦肉の策 ^^;
			}
			break;
		}
	}
#else	// USE_68KEM
	int i;
	IRQH_IRQ[irq&7] = 0;
	#ifdef CYCLONE
	m68k.irq =0;
	#else
C68k_Set_IRQ(&C68K, 0);
	#endif
	for (i=7; i>0; i--)
	{
	    if (IRQH_IRQ[i])
	    {
#ifdef CYCLONE

	m68k.irq = i;

	#else
		C68k_Set_IRQ(&C68K, i);
#endif
		return;
	    }
	}
#endif	// USE_68KEM
}


// -----------------------------------------------------------------------
//   割り込み発生
// -----------------------------------------------------------------------
void IRQH_Int(BYTE irq, DMAHANDLER handler)
{
#ifdef USE_68KEM
	int i;
	IRQH_IRQ[irq] = 1;
	if (handler==NULL)
		IRQH_CallBack[irq] = &IRQH_DefaultVector;
	else
		IRQH_CallBack[irq] = handler;
	for (i=7; i>0; i--)
	{
		if (IRQH_IRQ[i])
		{
			regs.irq_callback = IRQH_CallBack[i];
			regs.IRQ_level = i;
			if ( m68000_ICount ) {					// 多重割り込み時（CARAT）
				m68000_ICountBk += m68000_ICount;		// 強制的に割り込みチェックをさせる
				m68000_ICount = 0;				// 苦肉の策 ^^;
			}
			return;
		}
	}
#else	// USE_68KEM
	int i;
	IRQH_IRQ[irq&7] = 1;
	if (handler==NULL)
	    IRQH_CallBack[irq&7] = &IRQH_DefaultVector;
	else
	    IRQH_CallBack[irq&7] = handler;
	for (i=7; i>0; i--)
	{
	    if (IRQH_IRQ[i])
	    {
#ifdef CYCLONE

	m68k.irq = i;

	#else
		C68k_Set_IRQ(&C68K, i);
#endif
		return;
	    }
	}
}

signed int FASTCALL my_irqh_callback(signed int level)
{
    int i;

	if (level == 255 /*BUS Error*/)
	{
		return 2;
	}

    DMAHANDLER func = IRQH_CallBack[level&7];
    int vect = (*func)(level&7);
    //printf("irq vect = %x line = %d\n", vect, level);

    for (i=7; i>0; i--)
    {
	if (IRQH_IRQ[i])
	{
	#ifdef CYCLONE

	m68k.irq = i;
	    break;
	#else
	    C68k_Set_IRQ(&C68K, i);
	    break;
	#endif
	}
    }

    return (signed int )vect;
#endif	// USE_68KEM
}

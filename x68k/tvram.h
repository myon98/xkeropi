#ifndef _winx68k_tvram
#define _winx68k_tvram

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	TVRAM[0x80000];
extern	BYTE	TextDrawWork[1024 * 1024];
extern	BYTE	TextDirtyLine[1024];
//extern	WORD	Text_LineBuf[1024];
extern	BYTE	Text_TrFlag[1024];

void TVRAM_SetAllDirty(void);

void TVRAM_Init(void);
void TVRAM_Cleanup(void);

BYTE FASTCALL TVRAM_Read(DWORD adr);
void FASTCALL TVRAM_Write(DWORD adr, BYTE data);
void FASTCALL TVRAM_RCUpdate(void);
void Text_DrawLine(uint32_t v, uint_fast8_t opaq);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_tvram

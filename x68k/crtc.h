#ifndef _winx68k_crtc
#define _winx68k_crtc

#define	VSYNC_HIGH	180310L
#define	VSYNC_NORM	162707L

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	CRTC_Regs[48];
extern	BYTE	CRTC_Mode;
extern	WORD	CRTC_VSTART, CRTC_VEND;
extern	WORD	CRTC_HSTART;
extern	DWORD	TextDotX, TextDotY;
extern	DWORD	TextScrollX, TextScrollY;
extern	BYTE	VCReg0[2];
extern	BYTE	VCReg1[2];
extern	BYTE	VCReg2[2];
extern	WORD	CRTC_IntLine;
extern	BYTE	CRTC_FastClr;
extern	WORD	CRTC_FastClrMask;
extern	BYTE	CRTC_VStep;
extern	int		HSYNC_CLK;

extern	DWORD	GrphScrollX[];
extern	DWORD	GrphScrollY[];

#ifdef WIN68DEBUG
	uint8_t	Debug_Text, Debug_Grp, Debug_Sp;
#endif	// WIN68DEBUG

void CRTC_Init(void);

BYTE FASTCALL CRTC_Read(DWORD adr);
void FASTCALL CRTC_Write(DWORD adr, BYTE data);
void CRTC_DrawLine(uint16_t* dst, uint32_t v);

BYTE FASTCALL VCtrl_Read(DWORD adr);
void FASTCALL VCtrl_Write(DWORD adr, BYTE data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_crtc

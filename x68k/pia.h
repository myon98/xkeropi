#ifndef _winx68k_pia
#define _winx68k_pia

void PIA_Init(void);
EXTERNC BYTE FASTCALL PIA_Read(DWORD adr);
EXTERNC void FASTCALL PIA_Write(DWORD adr, BYTE data);

#endif

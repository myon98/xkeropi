#ifndef _winx68k_irqh
#define _winx68k_irqh

typedef DWORD (FASTCALL * DMAHANDLER)(BYTE irq);

void IRQH_Init(void);
void IRQH_IRQCallBack(BYTE irq);
void IRQH_Int(BYTE irq, DMAHANDLER handler);

#endif

﻿// ---------------------------------------------------------------------------------------
//  BG.C - BGとスプライト
//  ToDo：透明色の処理チェック（特に対Text間）
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "bg.h"
#include "crtc.h"
#include "tvram.h"
#include "palette.h"
#include "windraw.h"

	BYTE	BG[0x8000];
	BYTE	Sprite_Regs[0x800];
	BYTE	BG_Regs[0x12];
static	WORD	BG_CHREND = 0;
	WORD	BG_BG0TOP = 0;
static	WORD	BG_BG0END = 0;
	WORD	BG_BG1TOP = 0;
static	WORD	BG_BG1END = 0;
	BYTE	BG_CHRSIZE = 16;
static	DWORD	BG_AdrMask = 511;
	DWORD	BG0ScrollX = 0, BG0ScrollY = 0;
	DWORD	BG1ScrollX = 0, BG1ScrollY = 0;

	int32_t	BG_HAdjust = 0;
	int32_t	BG_VAdjust = 0;

	BYTE	BGCHR8[8*8*256];
	BYTE	BGCHR16[16*16*256];

	WORD	BG_LineBuf[1600];
	WORD	BG_PriBuf[1600];

struct SPRITECTRLTBL {
	uint16_t posx;
	uint16_t posy;
	uint8_t num;
	uint8_t ctrl;
	uint8_t ply;
	uint8_t dummy;
} __attribute__ ((packed));
typedef struct SPRITECTRLTBL SPRITECTRLTBL_T;

// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void BG_Init(void)
{
	DWORD i;
	ZeroMemory(Sprite_Regs, 0x800);
	ZeroMemory(BG, 0x8000);
	ZeroMemory(BGCHR8, 8*8*256);
	ZeroMemory(BGCHR16, 16*16*256);
	ZeroMemory(BG_LineBuf, 1600*2);
	for (i=0; i<0x12; i++)
		BG_Write(0xeb0800+i, 0);
	BG_CHREND = 0x8000;
}


// -----------------------------------------------------------------------
//   I/O Read
// -----------------------------------------------------------------------
BYTE FASTCALL BG_Read(DWORD adr)
{
	if ((adr>=0xeb0000)&&(adr<0xeb0400))
	{
		adr -= 0xeb0000;
		adr ^= 1;
		return Sprite_Regs[adr];
	}
	else if ((adr>=0xeb0800)&&(adr<0xeb0812))
		return BG_Regs[adr-0xeb0800];
	else if ((adr>=0xeb8000)&&(adr<0xec0000))
		return BG[adr-0xeb8000];
	else
		return 0xff;
}


// -----------------------------------------------------------------------
//   I/O Write
// -----------------------------------------------------------------------
void FASTCALL BG_Write(DWORD adr, BYTE data)
{
	DWORD bg16chr;
	if ((adr>=0xeb0000)&&(adr<0xeb0400))
	{
		int v = 0;
		if (!(BG_Regs[0x11] & 16))
		{
			const uint8_t s1 = (((BG_Regs[0x11]  &4)?2:1)-((BG_Regs[0x11]  &16)?1:0));
			const uint8_t s2 = (((CRTC_Regs[0x29]&4)?2:1)-((CRTC_Regs[0x29]&16)?1:0));
			v = ((BG_Regs[0x0f] >> s1) - (CRTC_Regs[0x0d] >> s2));
		}
/*{
FILE* fp = fopen("_sprite.txt", "a");
if (fp) fprintf(fp, "Adr:$%08X  Data:$%02X @ $%08X\n", adr, data, M68000_GETPC);
fclose(fp);
}*/
		adr -= 0xeb0000;
		adr ^= 1;
		if (Sprite_Regs[adr] != data)
		{
			const uint32_t y = BG_VAdjust - 16 - v;
			volatile SPRITECTRLTBL_T* sp = (SPRITECTRLTBL_T *)(Sprite_Regs + (adr & 0x3f8));
			uint32_t y0, y1;
			unsigned int i;

			y0 = y + sp->posy;
			for (i = 0; i < 16; i++)
			{
				TextDirtyLine[(y0 + i) & 1023] = 1;
			}

			Sprite_Regs[adr] = data;

			y1 = y + sp->posy;
			if (y0 != y1)
			{
				for (i = 0; i < 16; i++)
				{
					TextDirtyLine[(y1 + i) & 1023] = 1;
				}
			}
		}
	}
	else if ((adr>=0xeb0800)&&(adr<0xeb0812))
	{
		adr -= 0xeb0800;
		if (BG_Regs[adr]==data) return;	// データに変化が無ければ帰る
		BG_Regs[adr] = data;
		switch(adr)
		{
		case 0x00:
		case 0x01:
			BG0ScrollX = (((DWORD)BG_Regs[0x00]<<8)+BG_Regs[0x01])&BG_AdrMask;
			TVRAM_SetAllDirty();
			break;
		case 0x02:
		case 0x03:
			BG0ScrollY = (((DWORD)BG_Regs[0x02]<<8)+BG_Regs[0x03])&BG_AdrMask;
			TVRAM_SetAllDirty();
			break;
		case 0x04:
		case 0x05:
			BG1ScrollX = (((DWORD)BG_Regs[0x04]<<8)+BG_Regs[0x05])&BG_AdrMask;
			TVRAM_SetAllDirty();
			break;
		case 0x06:
		case 0x07:
			BG1ScrollY = (((DWORD)BG_Regs[0x06]<<8)+BG_Regs[0x07])&BG_AdrMask;
			TVRAM_SetAllDirty();
			break;

		case 0x08:		// BG On/Off Changed
			TVRAM_SetAllDirty();
			break;

		case 0x0d:
			BG_HAdjust = ((int32_t)BG_Regs[0x0d]-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			TVRAM_SetAllDirty();
			break;
		case 0x0f:
			BG_VAdjust = ((int32_t)BG_Regs[0x0f]-CRTC_VSTART)/((BG_Regs[0x11]&4)?1:2);	// BGとその他がずれてる時の差分
			TVRAM_SetAllDirty();
			break;

		case 0x11:		// BG ScreenRes Changed
			if (data&3)
			{
				if ((BG_BG0TOP==0x4000)||(BG_BG1TOP==0x4000))
					BG_CHREND = 0x4000;
				else if ((BG_BG0TOP==0x6000)||(BG_BG1TOP==0x6000))
					BG_CHREND = 0x6000;
				else
					BG_CHREND = 0x8000;
			}
			else
				BG_CHREND = 0x2000;
			BG_CHRSIZE = ((data&3)?16:8);
			BG_AdrMask = ((data&3)?1023:511);
			BG_HAdjust = ((int32_t)BG_Regs[0x0d]-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			BG_VAdjust = ((int32_t)BG_Regs[0x0f]-CRTC_VSTART)/((BG_Regs[0x11]&4)?1:2);	// BGとその他がずれてる時の差分
			break;
		case 0x09:		// BG Plane Cfg Changed
			TVRAM_SetAllDirty();
			if (data&0x08)
			{
				if (data&0x30)
				{
					BG_BG1TOP = 0x6000;
					BG_BG1END = 0x8000;
				}
				else
				{
					BG_BG1TOP = 0x4000;
					BG_BG1END = 0x6000;
				}
			}
			else
				BG_BG1TOP = BG_BG1END = 0;
			if (data&0x01)
			{
				if (data&0x06)
				{
					BG_BG0TOP = 0x6000;
					BG_BG0END = 0x8000;
				}
				else
				{
					BG_BG0TOP = 0x4000;
					BG_BG0END = 0x6000;
				}
			}
			else
				BG_BG0TOP = BG_BG0END = 0;
			if (BG_Regs[0x11]&3)
			{
				if ((BG_BG0TOP==0x4000)||(BG_BG1TOP==0x4000))
					BG_CHREND = 0x4000;
				else if ((BG_BG0TOP==0x6000)||(BG_BG1TOP==0x6000))
					BG_CHREND = 0x6000;
				else
					BG_CHREND = 0x8000;
			}
			break;
		case 0x0b:
			break;
		}
		Draw_DrawFlag = 1;

	}
	else if ((adr>=0xeb8000)&&(adr<0xec0000))
	{
		adr -= 0xeb8000;
		if (BG[adr]==data) return;			// データに変化が無ければ帰る
		BG[adr] = data;
		if (adr<0x2000)
		{
			BGCHR8[adr*2]   = data>>4;
			BGCHR8[adr*2+1] = data&15;
		}
		bg16chr = ((adr&3)*2)+((adr&0x3c)*4)+((adr&0x40)>>3)+((adr&0x7f80)*2);
		BGCHR16[bg16chr]   = data>>4;
		BGCHR16[bg16chr+1] = data&15;

		if (adr<BG_CHREND)				// パターンエリア
		{
			TVRAM_SetAllDirty();
		}
		if ((adr>=BG_BG1TOP)&&(adr<BG_BG1END))	// BG1 MAPエリア
		{
			TVRAM_SetAllDirty();
		}
		if ((adr>=BG_BG0TOP)&&(adr<BG_BG0END))	// BG0 MAPエリア
		{
			TVRAM_SetAllDirty();
		}
	}
}


// -----------------------------------------------------------------------
//   1ライン分の描画
// -----------------------------------------------------------------------
uint32_t BG_CalcLine(uint32_t v)
{
	const uint8_t s1 = (((BG_Regs[0x11]  &4)?2:1)-((BG_Regs[0x11]  &16)?1:0));
	const uint8_t s2 = (((CRTC_Regs[0x29]&4)?2:1)-((CRTC_Regs[0x29]&16)?1:0));
	v <<= s1;
	v >>= s2;
	if ( !(BG_Regs[0x11]&16) ) v -= ((BG_Regs[0x0f]>>s1)-(CRTC_Regs[0x0d]>>s2));
	return v - BG_VAdjust;
}

#ifndef USE_ASM
static void
Sprite_DrawLineMcr(uint32_t v, uint_fast8_t pri)
{
	const SPRITECTRLTBL_T *sct = (SPRITECTRLTBL_T *)Sprite_Regs;
	const SPRITECTRLTBL_T *sctp = sct + 128;
	DWORD y;
	DWORD t;

	while (sct < sctp)
	{
		--sctp;
		if ((sctp->ply & 3) != pri)
		{
			continue;
		}

		t = (sctp->posx + BG_HAdjust) & 0x3ff;
		if (t >= TextDotX + 16)
		{
			continue;
		}

		y = v + 16 - (sctp->posy & 0x3ff);
		if (y < 16)
		{
			const uint16_t *pal = &TextPal[(sctp->ctrl & 15) << 4];
			const uint8_t *p;
			int d = 1;
			unsigned int i;

			if (sctp->ctrl & 0x80)
			{
				y = 15 - y;
			}
			p = &BGCHR16[(sctp->num * 256) + (y * 16)];

			if (sctp->ctrl & 0x40)
			{
				p += 15;
				d = -1;
			}

			for (i = 0; i < 16; i++)
			{
				const uint8_t dat = *p;
				p += d;
				if (dat)
				{
					const unsigned int x = t + i;
					const uint_fast16_t n = (uint_fast16_t)((intptr_t)sctp - (intptr_t)sct);
					if (BG_PriBuf[x] >= n)
					{
						BG_LineBuf[x] = pal[dat];
						Text_TrFlag[x] |= 2;
						BG_PriBuf[x] = (uint16_t)n;
					}
				}
			}
		}
	}
}

#define BG_DRAWLINE_LOOPY(cnt) \
{ \
	unsigned int j;							\
	const uint8_t n = bl & 15;					\
	if (n == 0) {							\
		for (j = 0; j < cnt; j++, edi++) {			\
			const uint8_t dat = *esi;			\
			esi += d;					\
			if (dat == 0)					\
				continue;				\
			if (!(Text_TrFlag[edi + 1] & 2)) {		\
				BG_LineBuf[edi + 1] = TextPal[dat];	\
				Text_TrFlag[edi + 1] |= 2;		\
			}						\
		}							\
	} else {							\
		const uint16_t* pal = &TextPal[n << 4];			\
		for (j = 0; j < cnt; j++, edi++) {			\
			const uint8_t dat = *esi;			\
			esi += d;					\
			if ((dat) || !(Text_TrFlag[edi + 1] & 2)) {	\
				BG_LineBuf[edi + 1] = pal[dat];		\
				Text_TrFlag[edi + 1] |= 2;		\
			}						\
		}							\
	}								\
}

#define BG_DRAWLINE_LOOPY_NG(cnt) \
{ \
	unsigned int j;						\
	const uint16_t* pal = &TextPal[(bl & 15) << 4];		\
	for (j = 0; j < cnt; j++, edi++) {			\
		const uint8_t dat = *esi;			\
		esi += d;					\
		if (dat) {					\
			BG_LineBuf[edi + 1] = pal[dat];		\
			Text_TrFlag[edi + 1] |= 2;		\
		}						\
	}							\
}

INLINE void
bg_drawline_loopx8(uint_fast16_t BGTOP, uint32_t h, uint32_t v, uint_fast8_t ng)
{
	int i;
	const uint32_t ebp = (v & 7) << 3;
	const uint32_t edx = BGTOP + ((v & 0x1f8) << 4);
	uint32_t edi = (h & 7) ^ 15;
	uint32_t ecx = (h & 0x1f8) >> 2;

	for (i = TextDotX >> 3; i >= 0; i--) {
		const uint8_t bl = BG[ecx + edx];
		uint_fast16_t si = (BG[ecx + edx + 1] << 6) + ebp;
		const uint8_t *esi;
		int d = 1;
		if (bl & 0x80) {
			si ^= 0x38;
		}
		esi = &BGCHR8[si];
		if (bl & 0x40) {
			esi += 7;
			d = -1;
		}
		if (ng) {
			BG_DRAWLINE_LOOPY_NG(8);
		} else {
			BG_DRAWLINE_LOOPY(8);
		}
		ecx += 2;
		ecx &= 0x7f;
	}
}

INLINE void
bg_drawline_loopx16(uint_fast16_t BGTOP, uint32_t h, uint32_t v, uint_fast8_t ng)
{
	int i;

	const uint32_t ebp = (v & 15) << 4;
	const uint32_t edx = BGTOP + ((v & 0x3f0) << 3);
	uint32_t edi = (h & 15) ^ 15;
	uint32_t ecx = (h & 0x3f0) >> 3;

	for (i = TextDotX >> 4; i >= 0; i--) {
		uint8_t bl = BG[ecx + edx];
		uint_fast16_t si = (BG[ecx + edx + 1] << 8) + ebp;
		const uint8_t *esi;
		int d = 1;
		if (bl & 0x80) {
			si ^= 0xf0;
		}
		esi = &BGCHR16[si];
		if (bl & 0x40) {
			esi += 15;
			d = -1;
		}
		if (ng) {
			BG_DRAWLINE_LOOPY_NG(16);
		} else {
			BG_DRAWLINE_LOOPY(16);
		}
		ecx += 2;
		ecx &= 0x7f;
	}
}

static void
BG_DrawLineMcr8(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx8(BGTOP, BGScrollX - BG_HAdjust, v, 0);
}

static void
BG_DrawLineMcr16(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx16(BGTOP, BGScrollX - BG_HAdjust, v, 0);
}

static void
BG_DrawLineMcr8_ng(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx8(BGTOP, BGScrollX - BG_HAdjust, v, 1);
}

static void
BG_DrawLineMcr16_ng(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx16(BGTOP, BGScrollX, v, 1);
}

void
BG_DrawLine(uint32_t v, uint_fast8_t opaq, uint_fast8_t gd)
{
	DWORD i;

	if (opaq) {
		for (i = 16; i < TextDotX + 16; ++i) {
			BG_LineBuf[i] = TextPal[0];
			BG_PriBuf[i] = 0xffff;
		}
	} else {
		for (i = 16; i < TextDotX + 16; ++i) {
			BG_PriBuf[i] = 0xffff;
		}
	}

	v = BG_CalcLine(v);

	Sprite_DrawLineMcr(v, 1);
	if ((BG_Regs[9] & 8) && (BG_CHRSIZE == 8)) { // BG1 on
		void (*func8)(uint_fast16_t, uint32_t, uint32_t) = (gd) ? BG_DrawLineMcr8 : BG_DrawLineMcr8_ng;
		(*func8)(BG_BG1TOP, BG1ScrollX, BG1ScrollY + v);
	}
	Sprite_DrawLineMcr(v, 2);
	if (BG_Regs[9] & 1) { // BG0 on
		if (BG_CHRSIZE == 8) {
			void (*func8)(uint_fast16_t, uint32_t, uint32_t) = (gd) ? BG_DrawLineMcr8 : BG_DrawLineMcr8_ng;
			(*func8)(BG_BG0TOP, BG0ScrollX, BG0ScrollY + v);
		} else {
			void (*func16)(uint_fast16_t, uint32_t, uint32_t) = (gd) ? BG_DrawLineMcr16 : BG_DrawLineMcr16_ng;
			(*func16)(BG_BG0TOP, BG0ScrollX, BG0ScrollY + v);
		}
	}
	Sprite_DrawLineMcr(v, 3);
}
#endif /* !USE_ASM */

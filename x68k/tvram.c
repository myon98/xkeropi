﻿// ---------------------------------------------------------------------------------------
//  TVRAM.C - Text VRAM
//  ToDo : 透明色処理とか色々
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	"tvram.h"
#include	"bg.h"
#include	"crtc.h"
#include	"palette.h"

	BYTE	TVRAM[0x80000];
	BYTE	TextDrawWork[1024 * 1024];
	BYTE	TextDirtyLine[1024];

static	uint32_t	TextDrawPattern[4][256][2];

//	WORD	Text_LineBuf[1024];	// →BGのを使うように変更
	BYTE	Text_TrFlag[1024];


// -----------------------------------------------------------------------
//   全部書き換え～
// -----------------------------------------------------------------------
void TVRAM_SetAllDirty(void)
{
	memset(TextDirtyLine, 1, 1024);
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void TVRAM_Init(void)
{
	int i, j, bit;
	ZeroMemory(TVRAM, 0x80000);
	ZeroMemory(TextDrawWork, 1024*1024);
	TVRAM_SetAllDirty();

	ZeroMemory(TextDrawPattern, sizeof(TextDrawPattern));	// パターンテーブル初期化
	for (i=0; i<256; i++)
	{
		for (j=0, bit=0x80; j<8; j++, bit>>=1)
		{
			if (i & bit)
			{
				((uint8_t *)TextDrawPattern[0][i])[j] = 1;
				((uint8_t *)TextDrawPattern[1][i])[j] = 2;
				((uint8_t *)TextDrawPattern[2][i])[j] = 4;
				((uint8_t *)TextDrawPattern[3][i])[j] = 8;
			}
		}
	}
}


// -----------------------------------------------------------------------
//   撤収
// -----------------------------------------------------------------------
void TVRAM_Cleanup(void)
{
}


// -----------------------------------------------------------------------
//   読むなり
// -----------------------------------------------------------------------
BYTE FASTCALL TVRAM_Read(DWORD adr)
{
	adr &= 0x7ffff;
	adr ^= 1;
	return TVRAM[adr];
}


// -----------------------------------------------------------------------
//   1ばいと書くなり
// -----------------------------------------------------------------------
INLINE void TVRAM_WriteByte(DWORD adr, BYTE data)
{
	if (TVRAM[adr]!=data)
	{
		TextDirtyLine[(((adr&0x1ffff)/128)-TextScrollY)&1023] = 1;
		TVRAM[adr] = data;
	}
}


// -----------------------------------------------------------------------
//   ますく付きで書くなり
// -----------------------------------------------------------------------
INLINE void TVRAM_WriteByteMask(DWORD adr, BYTE data)
{
	data = (TVRAM[adr]&CRTC_Regs[0x2e +((adr^1)&1)])|(data&(~CRTC_Regs[0x2e +((adr^1)&1)]));
	if (TVRAM[adr]!=data)
	{
		TextDirtyLine[(((adr&0x1ffff)/128)-TextScrollY)&1023] = 1;
		TVRAM[adr] = data;
	}
}


// -----------------------------------------------------------------------
//   書くなり
// -----------------------------------------------------------------------
void FASTCALL TVRAM_Write(DWORD adr, BYTE data)
{
	adr &= 0x7ffff;
	adr ^= 1;
	if (CRTC_Regs[0x2a]&1)			// 同時アクセス
	{
		adr &= 0x1ffff;
		if (CRTC_Regs[0x2a]&2)		// Text Mask
		{
			if (CRTC_Regs[0x2b]&0x10) TVRAM_WriteByteMask(adr        , data);
			if (CRTC_Regs[0x2b]&0x20) TVRAM_WriteByteMask(adr+0x20000, data);
			if (CRTC_Regs[0x2b]&0x40) TVRAM_WriteByteMask(adr+0x40000, data);
			if (CRTC_Regs[0x2b]&0x80) TVRAM_WriteByteMask(adr+0x60000, data);
		}
		else
		{
			if (CRTC_Regs[0x2b]&0x10) TVRAM_WriteByte(adr        , data);
			if (CRTC_Regs[0x2b]&0x20) TVRAM_WriteByte(adr+0x20000, data);
			if (CRTC_Regs[0x2b]&0x40) TVRAM_WriteByte(adr+0x40000, data);
			if (CRTC_Regs[0x2b]&0x80) TVRAM_WriteByte(adr+0x60000, data);
		}
	}
	else					// シングルアクセス
	{
		if (CRTC_Regs[0x2a]&2)		// Text Mask
		{
			TVRAM_WriteByteMask(adr, data);
		}
		else
		{
			TVRAM_WriteByte(adr, data);
		}
	}

	{
		uint32_t t0, t1;
		uint_fast8_t pat;
		const uint8_t *src = &TVRAM[adr & 0x1ffff];
		uint32_t *dst;

		pat = src[0x20000 * 0];
		t0 = TextDrawPattern[0][pat][0];
		t1 = TextDrawPattern[0][pat][1];

		pat = src[0x20000 * 1];
		t0 |= TextDrawPattern[1][pat][0];
		t1 |= TextDrawPattern[1][pat][1];

		pat = src[0x20000 * 2];
		t0 |= TextDrawPattern[2][pat][0];
		t1 |= TextDrawPattern[2][pat][1];

		pat = src[0x20000 * 3];
		t0 |= TextDrawPattern[3][pat][0];
		t1 |= TextDrawPattern[3][pat][1];

		dst = (uint32_t *)(&TextDrawWork[((adr & 0x1ffff) ^ 1) << 3]);
		dst[0] = t0;
		dst[1] = t1;
	}
}


// -----------------------------------------------------------------------
//   らすたこぴー時のあっぷでーと
// -----------------------------------------------------------------------
void FASTCALL TVRAM_RCUpdate(void)
{
	const uint32_t adr = (uint32_t)CRTC_Regs[0x2d] << 9;
	uint32_t *dst = (uint32_t *)(&TextDrawWork[adr << 3]);
	unsigned int i;

	for (i = 0; i < 512; i++)
	{
		const uint8_t* src = &TVRAM[(adr + i) ^ 1];
		uint8_t pat;
		uint32_t t0, t1;

		pat = src[0x20000 * 0];
		t0 = TextDrawPattern[0][pat][0];
		t1 = TextDrawPattern[0][pat][1];

		pat = src[0x20000 * 1];
		t0 |= TextDrawPattern[1][pat][0];
		t1 |= TextDrawPattern[1][pat][1];

		pat = src[0x20000 * 2];
		t0 |= TextDrawPattern[2][pat][0];
		t1 |= TextDrawPattern[2][pat][1];

		pat = src[0x20000 * 3];
		t0 |= TextDrawPattern[3][pat][0];
		t1 |= TextDrawPattern[3][pat][1];

		dst[(i * 2) + 0] = t0;
		dst[(i * 2) + 1] = t1;
	}
}


// -----------------------------------------------------------------------
//   1ライン描画
// -----------------------------------------------------------------------
#ifndef USE_ASM
void Text_DrawLine(uint32_t v, uint_fast8_t opaq)
{
	DWORD addr;
	DWORD x, y;
	const uint32_t off = 16;
	uint32_t i;
	uint32_t limit;

	if ((CRTC_Regs[0x29] & 0x1c) == 0x1c) {
		v *= 2;
	}
	y = ((TextScrollY + v) & 0x3ff) << 10;

	x = TextScrollX & 0x3ff;
	addr = x + y;

	limit = 1024 - x;
	if (limit > TextDotX) {
		limit = TextDotX;
	}

	if (opaq) {
		for (i = 0; i < limit; i++) {
			const uint8_t t = TextDrawWork[addr + i];
			Text_TrFlag[off + i] = t ? 1 : 0;
			BG_LineBuf[off + i] = TextPal[t];
		}
		for (; i < TextDotX; i++) {
			BG_LineBuf[off + i] = TextPal[0];
			Text_TrFlag[off + i] = 0;
		}
	} else {
		for (i = 0; i < limit; i++) {
			const uint8_t t = TextDrawWork[addr + i];
			if (t) {
				Text_TrFlag[off + i] |= 1;
				BG_LineBuf[off + i] = TextPal[t];
			}
		}
	}
}
#endif	/* !USE_ASM */

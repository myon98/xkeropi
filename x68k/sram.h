#ifndef _winx68k_sram
#define _winx68k_sram

EXTERNC	BYTE	SRAM[0x4000];

void SRAM_Init(void);
void SRAM_Cleanup(void);
void SRAM_VirusCheck(void);

EXTERNC BYTE FASTCALL SRAM_Read(DWORD adr);
EXTERNC void FASTCALL SRAM_Write(DWORD adr, BYTE data);

#endif


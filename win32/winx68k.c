#include "common.h"
#include "winx68k.h"
#include <shlwapi.h>
#include "resource.h"
#include "fileio.h"
#include "timer.h"
#include "keyboard.h"
#include "windraw.h"
#include "prop.h"
#include "dswin.h"
#include "status.h"
#include "joystick.h"
#include "winui.h"
#include "mkcgrom.h"
#include "cdrom.h"
#include "cmdline.h"
#include "sstp.h"
#include "../x68k/m68000.h"
#include "../x68k/memory.h"
#include "../x68k/mfp.h"
//#include "../x68k/opm.h"
#include "../x68k/bg.h"
#include "../x68k/adpcm.h"
#include "../x68k/mercury.h"
#include "../x68k/crtc.h"
#include "../x68k/fdc.h"
#include "../x68k/fdd.h"
#include "../x68k/dmac.h"
#include "../x68k/irqh.h"
#include "../x68k/ioc.h"
#include "../x68k/rtc.h"
#include "../x68k/sasi.h"
#include "../x68k/scsi.h"
#include "../x68k/bg.h"
#include "../x68k/palette.h"
#include "../x68k/crtc.h"
#include "../x68k/pia.h"
#include "../x68k/scc.h"
#include "../x68k/midi.h"
#include "../x68k/sram.h"
#include "../x68k/gvram.h"
#include "../x68k/tvram.h"
#include "../x68k/windrv.h"

#include "../fmgen/fmg_wrap.h"

#ifdef WIN68DEBUG
#include "../x68k/d68k.h"
#endif

#pragma comment(lib, "shlwapi.lib")

#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

const TCHAR PrgName[]  = TEXT("Keropi");
const TCHAR PrgTitle[] = TEXT("Keropi");

TCHAR winx68k_dir[MAX_PATH];
TCHAR winx68k_ini[MAX_PATH];

HINSTANCE hInst;
HWND      hWndMain;
HMENU     hMenu;

WORD      VLINE_TOTAL = 567;
DWORD     vline = 0;

BYTE DispFrame = 0;
DWORD SoundSampleRate;
int CurFrameRate = 1;
int NoWaitMode = 0;

UINT_PTR hTimerID = 0;
DWORD TimerICount = 0;
BYTE traceflag = 0;

static int ClkUsed = 0;
static int FrameSkipCount = 0;
static int FrameSkipQueue = 0;

// -----------------------------------------------------------------------------------
//  BIOS&FONT読み込み
// -----------------------------------------------------------------------------------
static const LPCTSTR BIOSFILE[4] = {TEXT("iplrom.dat"), TEXT("iplromxv.dat"), TEXT("iplromco.dat"), TEXT("iplrom30.dat")};
static const TCHAR FONTFILE[]    = TEXT("cgrom.dat");
static const TCHAR FONTFILETMP[] = TEXT("cgrom.tmp");

static	BYTE	SCSIIMG[] = {
			0x00, 0xfc, 0x00, 0x14,			// $fc0000 SCSI起動用のエントリアドレス
			0x00, 0xfc, 0x00, 0x16,			// $fc0004 IOCSベクタ設定のエントリアドレス(必ず"Human"の8バイト前)
			0x00, 0x00, 0x00, 0x00,			// $fc0008 ?
			0x48, 0x75, 0x6d, 0x61,			// $fc000c ↓
			0x6e, 0x36, 0x38, 0x6b,			// $fc0010 ID "Human68k"	(必ず起動エントリポイントの直前)
			0x4e, 0x75,				// $fc0014 "rts"		(起動エントリポイント)
			0x23, 0xfc, 0x00, 0xfc, 0x00, 0x2a,	// $fc0016 ↓		(IOCSベクタ設定エントリポイント)
			0x00, 0x00, 0x07, 0xd4,			// $fc001c "move.l #$fc002a, $7d4.l"
			0x74, 0xff,				// $fc0020 "moveq #-1, d2"
			0x4e, 0x75,				// $fc0022 "rts"
//			0x53, 0x43, 0x53, 0x49, 0x49, 0x4e,	// $fc0024 ID "SCSIIN"
// 内蔵SCSIをONにすると、SASIは自動的にOFFになっちゃうらしい…
// よって、IDはマッチしないようにしておく…
			0x44, 0x55, 0x4d, 0x4d, 0x59, 0x20,	// $fc0024 ID "DUMMY "
			0x70, 0xff,				// $fc002a "moveq #-1, d0"	(SCSI IOCSコールエントリポイント)
			0x4e, 0x75,				// $fc002c "rts"
		};



void WinX68k_SCSICheck(void)
{
	int i, scsi = 0;
	DWORD *p;
	for (i=0x30600; i<0x30c00; i+=2) {
		p = (DWORD*)(&IPL[i]);
		if ( *p==0x0000fc00 ) scsi = 1;
	}

	if ( scsi ) {		// SCSIモデルのとき
		ZeroMemory(IPL, 0x2000);			// 本体は8kb
		memset(&IPL[0x2000], 0xff, 0x1e000);		// 残りは0xff
		memcpy(IPL, SCSIIMG, sizeof(SCSIIMG));		// インチキSCSI BIOS
	} else {			// SASIモデルはIPLがそのまま見える
		memcpy(IPL, &IPL[0x20000], 0x20000);
	}
}


short WinX68k_LoadROMs(void)
{
	FILEH fp = NULL;
	int i;
	BYTE tmp;

	for (i=0; (i<4)&&(!fp); i++) {
		fp = File_OpenCurDir(BIOSFILE[i]);
	}

	if (!fp) {
		Error("BIOS ROM イメージが見つかりません.");
		return FALSE;
	}

	File_Read(fp, &IPL[0x20000], 0x20000);
	File_Close(fp);

	WinX68k_SCSICheck();					// SCSI IPLなら、$fc0000〜にSCSI BIOSを置く

	for (i=0; i<0x40000; i+=2) {
		tmp = IPL[i];
		IPL[i] = IPL[i+1];
		IPL[i+1] = tmp;
	}

	fp = File_OpenCurDir(FONTFILE);
	if ( !fp ) {
		fp = File_OpenCurDir(FONTFILETMP);		// cgrom.tmpがある？
		if ( !fp ) {							// なければ作る
			MessageBox(hWndMain,
				TEXT("フォントROMイメージが見つかりません.\nWindowsフォントから新規に作成します."),
				TEXT("けろぴーのメッセージ"), MB_ICONWARNING | MB_OK);
			SSTP_SendMes(SSTPMES_MAKEFONT);
			make_cgromdat(FONT, FALSE, TEXT("ＭＳ ゴシック"), TEXT("ＭＳ 明朝"));
			fp = File_CreateCurDir(FONTFILETMP);
			if ( fp ) {
				File_Write(fp, FONT, 0xc0000);
				File_Close(fp);
				return TRUE;
			}
			return TRUE;
		}
	}
	File_Read(fp, FONT, 0xc0000);
	File_Close(fp);

	return TRUE;
}

// -----------------------------------------------------------------------------------
//  りせっとぉ〜
// -----------------------------------------------------------------------------------

int WinX68k_Reset(void)
{
	OPM_Reset();

#ifdef USE_68KEM
	ZeroMemory(&regs, sizeof(m68k_regs));
	regs.a[7] = regs.isp = (IPL[0x30001]<<24)|(IPL[0x30000]<<16)|(IPL[0x30003]<<8)|IPL[0x30002];
	M68000_SETPC((IPL[0x30005]<<24)|(IPL[0x30004]<<16)|(IPL[0x30007]<<8)|IPL[0x30006]);
	regs.sr_high = 0x27;
	M68KRESET();
#else	// USE_68KEM
#ifdef CYCLONE
	m68000_reset();
	m68000_set_reg(M68K_A7, (IPL[0x30001]<<24)|(IPL[0x30000]<<16)|(IPL[0x30003]<<8)|IPL[0x30002]);
	m68000_set_reg(M68K_PC, (IPL[0x30005]<<24)|(IPL[0x30004]<<16)|(IPL[0x30007]<<8)|IPL[0x30006]);
#else	// CYCLONE
	C68k_Reset(&C68K);
	C68k_Set_AReg(&C68K, 7, (IPL[0x30001]<<24)|(IPL[0x30000]<<16)|(IPL[0x30003]<<8)|IPL[0x30002]);
	C68k_Set_PC(&C68K, (IPL[0x30005]<<24)|(IPL[0x30004]<<16)|(IPL[0x30007]<<8)|IPL[0x30006]);
#endif	// CYCLONE
#endif	// USE_68KEM

	Memory_Init();
	CRTC_Init();
	DMA_Init();
	MFP_Init();
	FDC_Init();
	FDD_Reset();
	SASI_Init();
	SCSI_Init();
	IOC_Init();
	SCC_Init();
	PIA_Init();
	RTC_Init();
	TVRAM_Init();
	GVRAM_Init();
	BG_Init();
	Pal_Init();
	IRQH_Init();
	MIDI_Init();
	WinDrv_Init();

#ifdef USE_68KEM
	m68000_ICount = 0;
	m68000_ICountBk = 0;
#endif	// USE_68KEM
	ICount = 0;

	DSound_Stop();
	SRAM_VirusCheck();
	CDROM_Init();
	DSound_Play();

	return TRUE;
}

// -----------------------------------------------------------------------------------
//  初期化
// -----------------------------------------------------------------------------------

short WinX68k_Init(void)
{
	IPL = (BYTE*)malloc(0x40000);
	MEM = (BYTE*)malloc(0xc00000);			// とりあえず12Mb
	FONT = (BYTE*)malloc(0xc0000);
	if ( MEM ) ZeroMemory(MEM, 0xc00000);
	if ( (MEM)&&(FONT)&&(IPL) ) {
#ifndef USE_68KEM
		m68000_init();
#endif	// !USE_68KEM
		return TRUE;
	} else
		return FALSE;
}

// -----------------------------------------------------------------------------------
//  後始末
// -----------------------------------------------------------------------------------

void WinX68k_Cleanup(void)
{
	if ( IPL  ) free(IPL);
	if ( MEM  ) free(MEM);
	if ( FONT ) free(FONT);
}


#define CLOCK_SLICE 200
// -----------------------------------------------------------------------------------
//  コアのめいんるーぷ
// -----------------------------------------------------------------------------------

void WinX68k_Exec(void)
{
	char *test = NULL;
	int clk_total, clkdiv, usedclk, hsync, clk_next, clk_count, clk_line;
	int KeyIntCnt = 0, MouseIntCnt = 0;
	DWORD t_start = timeGetTime(), t_end;

	if ( FrameRate!=7 ) {
		DispFrame = (DispFrame+1)%FrameRate;
	} else {				// Auto Frame Skip
		if ( FrameSkipQueue ) {
			if ( FrameSkipCount>15 ) {
				FrameSkipCount = 0;
				FrameSkipQueue++;
				DispFrame = 0;
			} else {
				FrameSkipCount++;
				FrameSkipQueue--;
				DispFrame = 1;
			}
		} else {
			FrameSkipCount = 0;
			DispFrame = 0;
		}
	}

	vline = 0;
	clk_count = -ICount;
	clk_total = ((CRTC_Regs[0x29]&0x10)?VSYNC_HIGH:VSYNC_NORM);
	if      ( Config.XVIMode==1 ) { clk_total = (clk_total*16)/10; clkdiv = 16; }
	else if ( Config.XVIMode==2 ) { clk_total = (clk_total*24)/10; clkdiv = 24; }
	else                                                   clkdiv = 10;
	ICount += clk_total;
	clk_next  = (clk_total/VLINE_TOTAL);
	hsync = 1;

	do {
		int m, n = (ICount>CLOCK_SLICE)?CLOCK_SLICE:ICount;
#ifdef USE_68KEM
		m68000_ICount = m68000_ICountBk = 0;			// 割り込み発生前に与えておかないとダメ（CARAT）
#endif	// USE_68KEM

		if ( hsync ) {
			hsync = 0;
			clk_line = 0;
			MFP_Int(0);
			if ( (!(MFP[MFP_AER]&0x40))&&(vline==CRTC_IntLine) ) MFP_Int(1);
			if ( MFP[MFP_AER]&0x10 ) {
				if ( vline==CRTC_VSTART ) MFP_Int(9);
			} else {
				if ( CRTC_VEND>=VLINE_TOTAL ) {
					if ( (long)vline==(CRTC_VEND-VLINE_TOTAL) ) MFP_Int(9);		// エキサイティングアワーとか（TOTAL<VEND）
				} else {
					if ( (long)vline==(VLINE_TOTAL-1) ) MFP_Int(9);			// クレイジークライマーはコレでないとダメ？
				}
			}
		}

#ifdef WIN68DEBUG
		if (traceflag/*&&fdctrace*/)
		{
			FILE *fp;
			static DWORD oldpc;
			int i;
			char buf[200];
			fp=fopen("_trace68.txt", "a");
			for (i=0; i<HSYNC_CLK; i++)
			{
				const DWORD pc = M68000_GETPC;
				m68k_disassemble(buf, pc);
//				if (MEM[0xa84c0]) /**test=1; */tracing=1000;
//				if (pc==0x9d2a) tracing=5000;
//				if ((pc>=0x2000)&&((pc<=0x8e0e0))) tracing=50000;
//				if (pc<0x10000) tracing=1;
//				if ( (pc&1) )
//				fp=fopen("_trace68.txt", "a");
//				if ( (pc==0x7176) /*&& (Memory_ReadW(oldpc)==0xff1a)*/ ) tracing=100;
//				if ( (/*((pc>=0x27000) && (pc<=0x29000))||*/((pc>=0x27000) && (pc<=0x29000))) && (oldpc!=pc))
				if (/*fdctrace&&*/(oldpc!=pc))
				{
//					//tracing--;
					fprintf(fp, "D0:%08X D1:%08X D2:%08X D3:%08X D4:%08X D5:%08X D6:%08X D7:%08X CR:%04X\n", M68000_GETD(0), M68000_GETD(1), M68000_GETD(2), M68000_GETD(3), M68000_GETD(4), M68000_GETD(5), M68000_GETD(6), M68000_GETD(7), M68000_GETCR);
					fprintf(fp, "A0:%08X A1:%08X A2:%08X A3:%08X A4:%08X A5:%08X A6:%08X A7:%08X SR:%04X\n", M68000_GETA(0), M68000_GETA(1), M68000_GETA(2), M68000_GETA(3), M68000_GETA(4), M68000_GETA(5), M68000_GETA(6), M68000_GETA(7), M68000_GETSR);
					fprintf(fp, "<%04X> (%08X ->) %08X : %s\n", Memory_ReadW(pc), oldpc, pc, buf);
				}
				oldpc = pc;
#ifdef USE_68KEM
				m68000_ICount = 1;
				M68KRUN();
#else	// USE_68KEM
#ifdef CYCLONE
				m68000_execute(1);
#else	// CYCLONE
				C68k_Exec(&C68K, 1);
#endif	// CYCLONE
#endif	// USE_68KEM
			}
			fclose(fp);
			usedclk = clk_line = HSYNC_CLK;
			clk_count = clk_next;
		}
		else
#endif
		{
#ifdef USE_68KEM
		m68000_ICount = n;
		M68KRUN();
		m = (n-m68000_ICount-m68000_ICountBk);			// 経過クロック数
#else	// USE_68KEM
#ifdef CYCLONE
		m68000_execute(n);
		m = n;
#else	// CYCLONE
		m = C68k_Exec(&C68K, n);
#endif	// CYCLONE
#endif	// USE_68KEM
		ClkUsed += m*10;
		usedclk = ClkUsed/clkdiv;
		clk_line += usedclk;
		ClkUsed -= usedclk*clkdiv;
		ICount -= m;
		clk_count += m;
#ifdef USE_68KEM
		m68000_ICount = m68000_ICountBk = 0;
#endif	// USE_68KEM
		}

		MFP_Timer(usedclk);
		RTC_Timer(usedclk);
		DMA_Exec(0);
		DMA_Exec(1);
		DMA_Exec(2);

		if ( clk_count>=clk_next ) {
			OPM_RomeoOut(Config.BufferSize*5);
			MIDI_DelayOut((Config.MIDIAutoDelay)?(Config.BufferSize*5):Config.MIDIDelay);
			MFP_TimerA();
			if ( (MFP[MFP_AER]&0x40)&&(vline==CRTC_IntLine) ) MFP_Int(1);
			if ( (!DispFrame)&&(vline>=CRTC_VSTART)&&(vline<CRTC_VEND) ) {
				const uint32_t v = ((vline - CRTC_VSTART) * CRTC_VStep) >> 1;
				if ( CRTC_VStep==1 ) {				// HighReso 256dot（2度読み）
					if ( vline%2 )
						WinDraw_DrawLine(v);
				} else if ( CRTC_VStep==4 ) {			// LowReso 512dot
					WinDraw_DrawLine(v);			// 1走査線で2回描く（インターレース）
					WinDraw_DrawLine(v + 1);
				} else {					// High 512dot / Low 256dot
					WinDraw_DrawLine(v);
				}
			}

			ADPCM_PreUpdate(clk_line);
			OPM_Timer(clk_line);
			MIDI_Timer(clk_line);
			Mcry_PreUpdate(clk_line);

			KeyIntCnt++;
			if ( KeyIntCnt>(VLINE_TOTAL/4) ) {
				KeyIntCnt = 0;
				Keyboard_Int();
			}
			MouseIntCnt++;
			if ( MouseIntCnt>(VLINE_TOTAL/8) ) {
				MouseIntCnt = 0;
				SCC_IntCheck();
			}
			DSound_Send0(clk_line);			// Bufferがでかいとき用

			vline++;
			clk_next  = (clk_total*(vline+1))/VLINE_TOTAL;
			hsync = 1;
		}
	} while ( vline<VLINE_TOTAL );

	if ( CRTC_Mode&2 ) {		// FastClrビットの調整（PITAPAT）
		if ( CRTC_FastClr ) {	// FastClr=1 且つ CRTC_Mode&2 なら 終了
			CRTC_FastClr--;
			if ( !CRTC_FastClr ) CRTC_Mode &= 0xfd;
		} else {				// FastClr開始
			if ( CRTC_Regs[0x29]&0x10 )
				CRTC_FastClr = 1;
			else
				CRTC_FastClr = 2;
			TVRAM_SetAllDirty();
			GVRAM_FastClear();
		}
	}

	Joystick_Update();
	MidiOut_Update();
	DSound_Send();
	FDD_SetFDInt();
	if ( !DispFrame ) WinDraw_Draw();
	TimerICount += clk_total;

	t_end = timeGetTime();
	if ( (int)(t_end-t_start)>((CRTC_Regs[0x29]&0x10)?14:16) ) {
		FrameSkipQueue += ((t_end-t_start)/((CRTC_Regs[0x29]&0x10)?14:16))+1;
		if ( FrameSkipQueue>100 ) FrameSkipQueue = 100;
	}
}


// --------------------------------------------------------------------------
//   お約束のメイン
// --------------------------------------------------------------------------

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPreInst,
				LPTSTR lpszCmdLine, int nCmdShow)
{
	WNDCLASS winx68k;
	MSG msg;
	HIMC hIMC;
	HWND hwnd;
	DWORD StartTime;
	LPTSTR filepart;
	TCHAR buf[MAX_PATH];

	if ((hwnd = FindWindow(PrgName, NULL)) != NULL) {
		ShowWindow(hwnd, SW_RESTORE);
		SetForegroundWindow(hwnd);
		SSTP_Init();
		SSTP_SendMes(SSTPMES_DUALBOOT);
		SSTP_Cleanup();
		return(FALSE);
	}

#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	CoInitialize(NULL);
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

#ifdef _DEBUG
	if (AllocConsole())
	{
		_tfreopen(TEXT("CON"), TEXT("w"), stdout);
	}
#endif	// _DEBUG

	hInst = hInstance;

	GetModuleFileName(NULL, buf, MAX_PATH);
	GetFullPathName(buf, MAX_PATH, winx68k_dir, &filepart);
	*filepart = 0;
	PathCombine(winx68k_ini, winx68k_dir, TEXT("winx68k.ini"));

	if (!hPreInst) {
		winx68k.style = CS_BYTEALIGNCLIENT | CS_HREDRAW | CS_VREDRAW;
		winx68k.lpfnWndProc = WndProc;
		winx68k.cbClsExtra = 0;
		winx68k.cbWndExtra = 0;
		winx68k.hInstance = hInstance;
		winx68k.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
		winx68k.hCursor = LoadCursor(NULL, IDC_ARROW);
		winx68k.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		winx68k.lpszMenuName = MAKEINTRESOURCE(IDR_MENU);
		winx68k.lpszClassName = PrgName;
		if (!RegisterClass(&winx68k)) {
			return(FALSE);
		}
	}

	hWndMain = CreateWindowEx(WS_EX_ACCEPTFILES,
			PrgName, PrgTitle,
			WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION |
			WS_MINIMIZEBOX,
			CW_USEDEFAULT, CW_USEDEFAULT,
			SCREEN_WIDTH, SCREEN_HEIGHT,
			NULL, NULL, hInstance, NULL);

	hMenu = GetMenu(hWndMain);
	hIMC = ImmAssociateContext(hWndMain, 0);

	LoadConfig();

	SSTP_Init();
	MidiOut_Init();

	CheckCmdLine(lpszCmdLine);

	SplashFlag = 20;
	SoundSampleRate = Config.SampleRate;

	StatBar_Show(Config.WindowFDDStat);
	WinDraw_ChangeSize();
	WinDraw_ChangeMode(FALSE);

	//StatBar_Show(FALSE);

	ShowWindow(hWndMain, nCmdShow);
	UpdateWindow(hWndMain);

	WinUI_Init();
	WinDraw_StartupScreen();

	if (!WinDraw_Init()) {
		WinDraw_Cleanup();
		Error("　DirectDrawの初期化に失敗しました. \n\n　画面モードが「16ビット」になっているかどうか, および, \n画面サイズが「800x600」以上に設定されているかどうか\n確認してください.");
		return FALSE;
	}

	if (!WinX68k_Init())
	{
		WinX68k_Cleanup();
		WinDraw_Cleanup();
		Error("メモリが足りません.");
		return FALSE;
	}
	if (!WinX68k_LoadROMs())
	{
		WinX68k_Cleanup();
		WinDraw_Cleanup();
		return FALSE;
	}

	timeBeginPeriod(1);
	StartTime = timeGetTime();

	if ( SoundSampleRate ) {
		ADPCM_Init(SoundSampleRate);
		OPM_Init(4000000/*3579545*/, SoundSampleRate);
		Mcry_Init(SoundSampleRate, winx68k_dir);
	} else {
		ADPCM_Init(100);
		OPM_Init(4000000/*3579545*/, 100);
		Mcry_Init(100, winx68k_dir);
	}

	Joystick_Init();
	SRAM_Init();
	WinX68k_Reset();
	Timer_Init();

	MIDI_SetMimpiMap(Config.ToneMapFile);		// 音色設定ファイル使用反映
	MIDI_EnableMimpiDef(Config.ToneMap);

	if ( !DSound_Init(Config.SampleRate, Config.BufferSize) )
		if ( Config.DSAlert ) Error("DirectSoundの初期化に失敗しました。\nサウンド無しで継続します。");

	ADPCM_SetVolume((BYTE)Config.PCM_VOL);
	OPM_SetVolume((BYTE)Config.OPM_VOL);
	Mcry_SetVolume((BYTE)Config.MCR_VOL);
	DSound_Play();

	SetCmdLineFD();			// コマンドラインでFD挿入を指示している場合、ここで入れる

	hTimerID = SetTimer(hWndMain, 1, 500, 0);
	srand(timeGetTime());

	while (1) {
		if ( PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE) ) {
			if ( !GetMessage(&msg, NULL, 0, 0) ) break;
			DispatchMessage(&msg);
		} else {
//			OPM_RomeoOut(Config.BufferSize*5);
			if ( (NoWaitMode)||(Timer_GetCount()) ) {
				WinX68k_Exec();
				if ( SplashFlag ) {
					SplashFlag--;
					if ( !SplashFlag ) WinDraw_HideSplash();
				}
			} else {
				Sleep(1);
			}
		}
	}

	if ( hTimerID ) KillTimer(hWndMain, hTimerID);
	timeEndPeriod(1);
	ImmAssociateContext(hWndMain, hIMC);

	Memory_WriteB(0xe8e00d, 0x31);														// SRAM書き込み許可
	Memory_WriteD(0xed0040, Memory_ReadD(0xed0040)+1+(timeGetTime()-StartTime)/60000);	// 積算稼働時間(min.)（ムーンクレスタで使ってるので、てきとーに足しておく）
	Memory_WriteD(0xed0044, Memory_ReadD(0xed0044)+1);									// 積算起動回数

	OPM_Cleanup();
	Mcry_Cleanup();

	SRAM_Cleanup();
	FDD_Cleanup();
	CDROM_Cleanup();
	MIDI_Cleanup();
	Joystick_Cleanup();
	DSound_Cleanup();
	WinX68k_Cleanup();
	WinDraw_Cleanup();
	WinDraw_CleanupScreen();
	SSTP_Cleanup();

	SaveConfig();

#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	CoUninitialize();
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	return (int)msg.wParam;
}

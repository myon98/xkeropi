#ifndef winx68k_joy_h
#define winx68k_joy_h

#define	JOY_UP		0x01
#define	JOY_DOWN	0x02
#define	JOY_LEFT	0x04
#define	JOY_RIGHT	0x08
#define	JOY_TRG2	0x20
#define	JOY_TRG1	0x40

#define	JOY_TRG5	0x01
#define	JOY_TRG4	0x02
#define	JOY_TRG3	0x04
#define	JOY_TRG7	0x08
#define	JOY_TRG8	0x20
#define	JOY_TRG6	0x40

#ifndef MAX_BUTTON
#define MAX_BUTTON 32
#endif

extern TCHAR joyname[2][MAX_PATH];
extern TCHAR joybtnname[2][MAX_BUTTON][MAX_PATH];
extern BYTE joybtnnum[2];
extern BYTE JoyKeyState;

void Joystick_Init(void);
void Joystick_Cleanup(void);
void Joystick_Activate(WPARAM wParam);
BYTE FASTCALL Joystick_Read(BYTE num);
void FASTCALL Joystick_Write(BYTE num, BYTE data);
void FASTCALL Joystick_Update(void);
BOOL Joystick_Available(BYTE num);

#endif

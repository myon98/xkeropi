// ---------------------------------------------------------------------------------------
//  CD-ROM drive access routine by WNASPI32.DLL
//    Win95/98用、NT系は未確認〜
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "cdrom.h"
#include "cdaspi.h"
#include "prop.h"

#include "msfiles\scsidefs.h"

#define SECTOR_SIZE  2352

static int	CDOpened	= 0;
static LPBYTE	lpSector	= NULL;
static long	LastSector	= -1;

static int	AdapterID	= 0;
static int	TargetID	= 0;
static int	TargetLUN	= 0;

static TOC	tocbuf;
static SRB_ExecSCSICmd	srb;
static HANDLE		hEventSRB;
static int		LastStatus = SS_COMP;

BYTE CDASPI_CDName[8][4];
int CDASPI_CD_AID[8];
int CDASPI_CD_TID[8];
int CDASPI_CDNum = 0;


HINSTANCE ASPIDLL;
DWORD (*pSendASPI32Command)(LPSRB);
DWORD (*pGetASPI32SupportInfo)(VOID);


//------------------------------------------------------
// コマンド実行終了を待ちます
//------------------------------------------------------
int CDASPI_Wait(void)
{
	if (LastStatus == SS_PENDING)
	{
		WaitForSingleObject(hEventSRB, INFINITE);
		CloseHandle(hEventSRB);
		LastStatus = SS_COMP;
	}
	return TRUE;
}


//------------------------------------------------------
// SCSIコマンド実行
//------------------------------------------------------
int CDASPI_ExecCmd(BYTE *cdb, int cdb_size, BYTE *out_buff, int out_size)
{
	if (!CDOpened) return FALSE;

        if (hEventSRB) CloseHandle(hEventSRB);
	hEventSRB = CreateEvent(NULL, TRUE, FALSE, NULL);

	ZeroMemory(&srb, sizeof(SRB_ExecSCSICmd));
	srb.SRB_Cmd		= SC_EXEC_SCSI_CMD;
	srb.SRB_HaId		= AdapterID;
	srb.SRB_Target		= TargetID;
	srb.SRB_Flags		= SRB_DIR_IN | SRB_EVENT_NOTIFY;
	srb.SRB_SenseLen	= SENSE_LEN;
	srb.SRB_PostProc	= (LPVOID)hEventSRB;
	srb.SRB_BufPointer	= out_buff;
	srb.SRB_BufLen		= out_size;
	srb.SRB_CDBLen		= cdb_size;
	memcpy(srb.CDBByte, cdb, cdb_size);

	LastStatus = pSendASPI32Command(&srb);
	CDASPI_Wait();

	return TRUE;
}


//------------------------------------------------------
// Adapter/Targetで指定されたデバイスのタイプを返します
//------------------------------------------------------
static int GetDeviceType(int adapterid,int targetid)
{
	SRB_GDEVBlock srbGDEVBlock;

	ZeroMemory(&srbGDEVBlock, sizeof(srbGDEVBlock));
	srbGDEVBlock.SRB_Cmd	= SC_GET_DEV_TYPE;
	srbGDEVBlock.SRB_HaId	= adapterid;
	srbGDEVBlock.SRB_Target	= targetid;

	pSendASPI32Command( (LPSRB)&srbGDEVBlock );
	if (srbGDEVBlock.SRB_Status != SS_COMP) return -1;

	return (srbGDEVBlock.SRB_DeviceType);
}


//------------------------------------------------------
// 1ブロック読み込み
//------------------------------------------------------
int CDASPI_Read(long block, BYTE* buf)
{
	BYTE cdb[10];
	if (CDOpened)
	{
		ZeroMemory(cdb, sizeof(cdb));
		cdb[0] = SCSI_READ10;
		cdb[2] = (BYTE)((block>>24)&0xff);
		cdb[3] = (BYTE)((block>>16)&0xff);
		cdb[4] = (BYTE)((block>>8 )&0xff);
		cdb[5] = (BYTE)((block    )&0xff);
		cdb[8] = 1;		// 1セクタだけ

		CDASPI_ExecCmd(cdb, sizeof(cdb), buf, SECTOR_SIZE);
		return TRUE;
	}
	return FALSE;
}


//------------------------------------------------------
// TOC読み込み
//------------------------------------------------------
int CDASPI_ReadTOC(void* buf)
{
	BYTE cdb[10];
	if (CDOpened)
	{
		ZeroMemory(cdb, sizeof(cdb));
		cdb[0] = SCSI_READ_TOC;
		cdb[1] = 2;
		cdb[6] = 1;		// 全トラック。無駄だけど、IOCTRL版に合わせる為
		cdb[7] = (sizeof(TOC)>>8);
		cdb[8] = (sizeof(TOC)&0xff);

		CDASPI_ExecCmd(cdb, sizeof(cdb), (BYTE *)buf, sizeof(TOC));
		return TRUE;
	}
	return FALSE;
}


//------------------------------------------------------
// アクセスモード選択
//------------------------------------------------------
static void CDASPI_ModeSel(int mode)
{
	BYTE cdb[6], param[12];
	if (!CDOpened) return;

        if (hEventSRB) CloseHandle(hEventSRB);
	hEventSRB = CreateEvent(NULL, TRUE, FALSE, NULL);

	memset(cdb,0,sizeof(cdb));
	memset(param,0,sizeof(param));

	cdb[0] = SCSI_MODE_SEL6;
	cdb[1] = 0x10;		// SCSI-2 Page format
	cdb[4] = sizeof(param);

	param[3] = 8;		// ブロックディスクリプタのサイズ
	param[4] = mode;	// 1セクタのサイズ (0:default 1:2048 2:2336 3:2340 4:audio)
	if (mode==0 || mode==1) {
		param[10] = (2048>>8) & 0xFF;
		param[11] = (2048) & 0xFF;
	} else {
		param[10] = (SECTOR_SIZE>>8) & 0xFF;
		param[11] = (SECTOR_SIZE) & 0xFF;
	}

	ZeroMemory(&srb, sizeof(SRB_ExecSCSICmd));
	srb.SRB_Cmd		= SC_EXEC_SCSI_CMD;
	srb.SRB_HaId		= AdapterID;
	srb.SRB_Target		= TargetID;
	srb.SRB_Flags		= SRB_DIR_OUT | SRB_EVENT_NOTIFY;
	srb.SRB_SenseLen	= SENSE_LEN;
	srb.SRB_PostProc	= (LPVOID)hEventSRB;
	srb.SRB_BufPointer	= param;
	srb.SRB_BufLen		= sizeof(param);
	srb.SRB_CDBLen		= sizeof(cdb);
	memcpy(srb.CDBByte, cdb, sizeof(cdb));

	LastStatus = pSendASPI32Command(&srb);
}


//------------------------------------------------------
// Open
//------------------------------------------------------
int CDASPI_Open(void)
{
	DWORD info;
	int stat, hosts, adapter, target;

	adapter = CDASPI_CD_AID[Config.CDROM_ASPI_Drive];
	target  = CDASPI_CD_TID[Config.CDROM_ASPI_Drive];

	info = pGetASPI32SupportInfo();

	stat = (info>>8)&255;
	hosts = info&255;

	if ( (hosts!=0) && (stat==SS_COMP) )				// I/Fがある？
	{
		if ( GetDeviceType(adapter, target) == DTYPE_CDROM )	// 指定のデバイスはCD-ROM？
		{
			CDASPI_ModeSel(3);
			CDASPI_Wait();
			AdapterID = adapter;
			TargetID  = target;
			CDOpened  = 1;
			return TRUE;
		}
	}

	return FALSE;
}


//------------------------------------------------------
// Close
//------------------------------------------------------
void CDASPI_Close(void)
{
	CDASPI_Wait();
	CDOpened = 0;
}


//------------------------------------------------------
// CD-ROMドライブを探します
//------------------------------------------------------
void CDASPI_EnumCD(void)
{
	DWORD info;
	int i, stat, hosts, adapter, target;

	for (i=0; i<8; i++) ZeroMemory(CDASPI_CDName[i], 4);
	CDASPI_CDNum = 0;

	info = pGetASPI32SupportInfo();
	stat = (info>>8)&255;
	hosts = info&255;

	if ( (hosts!=0) && (stat==SS_COMP) )				// I/Fがある？
	{
		for(adapter=0; adapter<hosts; adapter++)
		{
			for(target=0; target<8; target++)
			{
				if ( GetDeviceType(adapter, target) == DTYPE_CDROM )
				{
			                CDASPI_CD_AID[CDASPI_CDNum] = adapter;
			                CDASPI_CD_TID[CDASPI_CDNum] = target;
			                CDASPI_CDName[CDASPI_CDNum][0] = adapter+0x30;
			                CDASPI_CDName[CDASPI_CDNum][1] = ':';
			                CDASPI_CDName[CDASPI_CDNum][2] = target+0x30;
			                CDASPI_CDNum++;
					if (CDASPI_CDNum==8) return;
				}
			}
		}
	}
}

int CDASPI_IsOpen(void)
{
	return CDOpened;
}

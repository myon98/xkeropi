// ---------------------------------------------------------------------------------------
//  WINUI.C - UI関連
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "resource.h"
#include "about.h"
#include "keyboard.h"
#include "windraw.h"
#include "dswin.h"
#include "fileio.h"
#include "prop.h"
#include "status.h"
#include "joystick.h"
#include "mouse.h"
#include "winx68k.h"
#include "version.h"
#include "sstp.h"
#ifdef USE_ROMEO
#include "juliet.h"
#endif	// USE_ROMEO
#include "../x68k/fdd.h"
#include "../x68k/irqh.h"
#include "../x68k/m68000.h"
#include "../x68k/crtc.h"
#include "../x68k/mfp.h"
#include "../x68k/fdc.h"
#include "../x68k/disk_d88.h"
#include "../x68k/dmac.h"
#include "../x68k/ioc.h"
#include "../x68k/rtc.h"
#include "../x68k/sasi.h"
#include "../x68k/bg.h"
#include "../x68k/palette.h"
#include "../x68k/crtc.h"
#include "../x68k/pia.h"
#include "../x68k/scc.h"
#include "../x68k/midi.h"
//#include "../x68k/opm.h"
#include "../x68k/adpcm.h"
#include "../x68k/mercury.h"
#include "../x68k/tvram.h"

#include "../fmgen/fmg_wrap.h"

	DWORD		timertick=0;
	int		UI_MouseFlag = 0;

	BYTE		FrameRate = 2;
	WORD		SelectedFrameRate = IDM_FRAME02;

	TCHAR		filepath[MAX_PATH];
	int		fddblink = 0;
	int		fddblinkcount = 0;

	DWORD		LastClock[4] = {0, 0, 0, 0};

void WinUI_Init(void)
{
	//HMENU hMenu = GetMenu(hWndMain);
	WORD ratetable[9]={0, IDM_FRAME01, IDM_FRAME02, IDM_FRAME03, IDM_FRAME04,
			      IDM_FRAME05, IDM_FRAME06, IDM_FRAMEAUTO, IDM_FRAME08};
	SelectedFrameRate = ratetable[FrameRate];
	CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
	if (Config.JoyKeyReverse)
		CheckMenuItem(hMenu, IDM_JOYKEYREVERSE, MF_CHECKED);
	if (Config.JoyKeyJoy2)
		CheckMenuItem(hMenu, IDM_JOYKEYJOY2, MF_CHECKED);
	if (Config.JoyKey)
	{
		CheckMenuItem(hMenu, IDM_JOYKEY, MF_CHECKED);
		EnableMenuItem(hMenu, IDM_JOYKEYREVERSE, MF_ENABLED);
		EnableMenuItem(hMenu, IDM_JOYKEYJOY2, MF_ENABLED);
	}
	else
	{
		EnableMenuItem(hMenu, IDM_JOYKEYREVERSE, MF_GRAYED);
		EnableMenuItem(hMenu, IDM_JOYKEYJOY2, MF_GRAYED);
	}

	if (Config.XVIMode==1) {
		CheckMenuItem(hMenu, IDM_SPEED_XVI, MF_CHECKED);
	} else if (Config.XVIMode==2) {
		CheckMenuItem(hMenu, IDM_SPEED_REDZONE, MF_CHECKED);
	} else {
		Config.XVIMode = 0;
		CheckMenuItem(hMenu, IDM_SPEED_NORMAL, MF_CHECKED);
	}

	switch(Config.WinStrech)
	{
	case 1:
		CheckMenuItem(hMenu, IDM_FIXEDSTRETCH, MF_CHECKED);
		break;
	case 2:
		CheckMenuItem(hMenu, IDM_AUTOSTRETCH, MF_CHECKED);
		break;
	case 3:
		CheckMenuItem(hMenu, IDM_X68STRETCH, MF_CHECKED);
		break;
	default:
		Config.WinStrech = 0;
		CheckMenuItem(hMenu, IDM_NONSTRETCH, MF_CHECKED);
		break;
	}

#ifdef WIN68DEBUG
	CheckMenuItem(hMenu, IDM_TRACE, (traceflag) ? MF_CHECKED : MF_UNCHECKED);
	CheckMenuItem(hMenu, IDM_DEBUGTEXT, (Debug_Text) ? MF_CHECKED : MF_UNCHECKED);
	CheckMenuItem(hMenu, IDM_DEBUGGRP, (Debug_Grp) ? MF_CHECKED : MF_UNCHECKED);
	CheckMenuItem(hMenu, IDM_DEBUGSP, (Debug_Sp) ? MF_CHECKED : MF_UNCHECKED);
	CheckMenuItem(hMenu, IDM_HDDTRACE, (hddtrace) ? MF_CHECKED : MF_UNCHECKED);
	CheckMenuItem(hMenu, IDM_DMATRACE, (dmatrace) ? MF_CHECKED : MF_UNCHECKED);
	CheckMenuItem(hMenu, IDM_TIMERTRACE, (timertrace) ? MF_CHECKED : MF_UNCHECKED);
#else	// WIN68DEBUG
	DeleteMenu(hMenu, 6, MF_BYPOSITION);
	DrawMenuBar(hWndMain);
#endif	// WIN68DEBUG
}


static int FDType(LPCTSTR fname) {

	int leng;
	LPCTSTR p;

	leng = lstrlen(fname);
	if (leng > 4) {
		p = &fname[leng-4];
		if (lstrcmp(p, TEXT(".D88")) == 0 || lstrcmp(p, TEXT(".d88")) == 0 ||
			lstrcmp(p, TEXT(".88D")) == 0 || lstrcmp(p, TEXT(".88d")) == 0) {
			return(FD_D88);
		}
		if ( lstrcmp(p, TEXT(".DIM")) == 0 || lstrcmp(p, TEXT(".dim")) == 0) {
			return(FD_DIM);
		}
		return(FD_XDF);
	}

	return(FD_Non);
}


void WinUI_ChangeFDD(short drive)
{
	OPENFILENAME ofn;
	TCHAR filename[MAX_PATH];
	BYTE roflag = 0;
	int isopen, i, ret, openflag = 0;
	FILEH fp;
	D88_HEADER d88;

	DSound_Stop();
	WinDraw_ShowMenu(TRUE);
	ShowCursor(TRUE);

	FDD_EjectFD(drive);
	memset(&ofn, 0, sizeof(ofn));
	filename[0] = '\0';
	if (0) {
		_tcscpy(filename, TEXT("(新規ディスク作成時はファイル名を入力)"));
	}
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hWndMain;
	ofn.lpstrFilter = TEXT("X68k Disk Images (*.d88/*.88d/*.hdm/*.dup/*.2hd/*.dim/*.xdf/*.img)\0*.d88;*.88d;*.hdm;*.dup;*.2hd;*.dim;*.xdf;*.img\0")
					  TEXT("All Files (*.*)\0*.*\0");
	ofn.lpstrFile = filename;
	ofn.lpstrInitialDir = filepath;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_CREATEPROMPT | OFN_SHAREAWARE;
	ofn.lpstrDefExt = TEXT("d88");
	ofn.lpstrTitle = TEXT("X68kディスクイメージの選択");
	
	isopen = !!GetOpenFileName(&ofn);

	if (isopen)
	{
		fp = File_Open(filename);
		if (!fp) {
			if (GetLastError()==ERROR_FILE_NOT_FOUND) {
				ret = FDType(filename);
				if (ret==FD_DIM)
				{
					Error("DIM形式でのディスクの作成はできません。");
				}
				else if (ret==FD_XDF)
				{
					fp = File_Create(filename);
					if (!fp)
					{
						Error("ファイルの作成に失敗しました。");
					}
					else
					{
						BYTE *tmp = (BYTE *)malloc(0x1000);
						if (tmp) {
							memset(tmp, 0xe5, 0x1000);
							for (i=0; i<0x134; i++) File_Write(fp, tmp, 0x1000);
							free(tmp);
						} else {
							const BYTE tmp2 = 0xe5;
							for (i=0; i<0x134000; i++) File_Write(fp, &tmp2, 1);
						}
						File_Close(fp);
						openflag = 1;
					}
				}
				else
				{
					fp = File_Create(filename);
					if (!fp)
					{
						Error("ファイルの作成に失敗しました。");
					}
					else
					{
						memset(&d88, 0, sizeof(D88_HEADER));
						d88.fd_size = sizeof(D88_HEADER);
						d88.fd_type = 0x20;		// 2HD
						File_Write(fp, &d88, sizeof(D88_HEADER));
						File_Close(fp);
						openflag = 1;
					}
				}
			}
		}
		else {
			openflag = 1;
			File_Close(fp);
		}

		if (openflag)
		{
			lstrcpyn(filepath, filename, MAX_PATH);
			filepath[ofn.nFileOffset] = 0;
			if ( (ofn.Flags&OFN_READONLY)||(File_Attr(filename)&FILE_ATTRIBUTE_READONLY) ) roflag=1;
			FDD_SetFD(drive, filename, roflag);
		}
	}

	ShowCursor(FALSE);
	WinDraw_ShowMenu(FALSE);
	DSound_Play();
}


void WinUI_DropDisk(HDROP h)
{
	TCHAR file[MAX_PATH];
	POINT pt;
	short drv;

	DragQueryFile(h, 0, file, MAX_PATH);
	DragQueryPoint(h, &pt);
	if ( pt.x<(WindowX/2) ) drv=0; else drv=1;
	FDD_EjectFD(drv);
	FDD_SetFD(drv, file, 0);

	SSTP_SendMes((drv)?SSTPMES_DROPFD1:SSTPMES_DROPFD0);
}


// --------------------------------------------------------------------------
//   ふるすくりーん
// --------------------------------------------------------------------------
void ChangeFullScreen(int sw) 
{
	StatBar_Show(0);
	WinDraw_Cleanup();

	WinDraw_ChangeMode(sw);

	if (!WinDraw_Init()) {
		Error("画面モード切り替えに失敗しました。");
		PostQuitMessage(0);
	}
	else {
		WinDraw_Redraw();
		WinDraw_Draw();
		SetMenu(hWndMain, (sw)?NULL:hMenu);
		//WinDraw_ShowMenu(TRUE);
		//WinDraw_ShowMenu(FALSE);
//		ShowCursor(!sw);
		WinDraw_ChangeSize();
		StatBar_Show((sw)?Config.FullScrFDDStat:Config.WindowFDDStat);
		StatBar_Redraw();
		Mouse_StartCapture(sw||UI_MouseFlag);
	}
}


// --------------------------------------------------------------------------
//   タイトルバー書き換え用タイマー
// --------------------------------------------------------------------------
short WmTimer(HWND hwnd, WPARAM wparam, LPARAM lparam)
{
	int fddflag = 0;

	if (wparam == hTimerID)
	{
		DWORD timernowtick = timeGetTime();
		unsigned int freq = TimerICount/(timernowtick-timertick);
		timertick = timernowtick;
		LastClock[3] = LastClock[2];
		LastClock[2] = LastClock[1];
		LastClock[1] = LastClock[0];
		LastClock[0] = freq;
		TimerICount = 0;
//		if (!fullscreen)
		{
			if (fddblink)
			{
				TCHAR buf[256];
#ifdef WIN68DEBUG
				wsprintf(buf, TEXT("%s - %2d fps / %2d.%03d MHz  PC:%08X"),
					PrgTitle, FrameCount, (freq/1000), (freq%1000), M68000_GETPC);
#else
#ifdef USE_ROMEO
				if ( juliet_YM2151IsEnable() ) {
					wsprintf(buf, TEXT("%s v%s w/ ROMEO - %2d fps / %2d.%03d MHz"),
						PrgTitle, TEXT(APP_VER_STRING), FrameCount, (freq/1000), (freq%1000));
				}
				else
#endif	// USE_ROMEO
				{
					wsprintf(buf, TEXT("%s v%s - %2d fps / %2d.%03d MHz"),
						PrgTitle, TEXT(APP_VER_STRING), FrameCount, (freq/1000), (freq%1000));
				}
#endif
				FrameCount = 0;
				SetWindowText(hwnd, buf);
			}
			fddblink ^= 1;

			StatBar_UpdateTimer();

			if (fddflag) {
				if (fddblink) {
					fddblinkcount++;
					if (fddblinkcount==10) SSTP_SendMes(SSTPMES_WAITFD);
				}
			} else {
				fddblinkcount = 0;
			}
		}
//		else
//			SetWindowText(hwnd, "WinX1");		// あははーっ
	}
	return 0;
}

// --------------------------------------------------------------------------
//   メニューとかの処理
// --------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	char *temp = 0;
	unsigned short wid = LOWORD(wParam);
	PAINTSTRUCT	ps;
	RECT		rc;
	//HMENU	hMenu = GetMenu(hWndMain);
#ifdef WIN68DEBUG
	char	buf[256];
#endif

	switch (msg) {
	case WM_COMMAND:
		switch(LOWORD(wid))
		{
		case IDM_RESET:
			if ((!Config.MIDI_SW) || (Config.MIDI_Reset))
				MIDI_Cleanup();
			WinX68k_Reset();
			SSTP_SendMes(SSTPMES_RESET);
			break;
		case IDM_CONFIG:
			DSound_Stop();
//			DSound_Cleanup();
			WinDraw_ShowMenu(TRUE);
			PropPage_Init();
/*			if (Config.SampleRate)
			{
				ADPCM_Init(Config.SampleRate);
				Mcry_Init(SoundSampleRate);
			}
			else
			{
				ADPCM_Init(100);
				Mcry_Init(SoundSampleRate);
			}
			OPM_SetRate(4000000, Config.SampleRate);
*/
			ADPCM_SetVolume((BYTE)Config.PCM_VOL);
			OPM_SetVolume((BYTE)Config.OPM_VOL);
			Mcry_SetVolume((BYTE)Config.MCR_VOL);
//			DSound_Init(Config.SampleRate, Config.BufferSize);
			DSound_Play();
			//StatBar_Show(FALSE);
			WinDraw_ShowMenu(FALSE);
			MIDI_SetModule();				// MIDI_MODULE変更を反映
			MIDI_SetMimpiMap(Config.ToneMapFile);		// 音色設定ファイル使用反映
			MIDI_EnableMimpiDef(Config.ToneMap);
			break;
		case IDM_TOGGLEFULLSCREEN:
			ChangeFullScreen((!FullScreenFlag));
			break;
		case IDM_NMI:
			IRQH_Int(7, NULL);
			SSTP_SendMes(SSTPMES_NMI);
			break;
		case IDM_FDD0:
			WinUI_ChangeFDD(0);
			break;
		case IDM_FDD1:
			WinUI_ChangeFDD(1);
			break;
		case IDM_ABOUT:
			DSound_Stop();
			WinDraw_ShowMenu(TRUE);
			ShowCursor(TRUE);
			SSTP_SendMes(SSTPMES_ABOUT);
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUT),
					hWnd, (DLGPROC)AboutDialogProc);
			ShowCursor(FALSE);
			WinDraw_ShowMenu(FALSE);
			DSound_Play();
			break;
		case IDM_EXIT:
			SendMessage(hWnd, WM_CLOSE, 0, 0L);
			break;

		case IDM_FRAMEAUTO:
			FrameRate = 7;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAMEAUTO;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME01:
			FrameRate = 1;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME01;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME02:
			FrameRate = 2;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME02;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME03:
			FrameRate = 3;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME03;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME04:
			FrameRate = 4;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME04;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME05:
			FrameRate = 5;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME05;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME06:
			FrameRate = 6;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME06;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;
		case IDM_FRAME08:
			FrameRate = 8;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_UNCHECKED);
			SelectedFrameRate = IDM_FRAME08;
			CheckMenuItem(hMenu, SelectedFrameRate, MF_CHECKED);
			break;

		case IDM_MOUSE:
			UI_MouseFlag ^= 1;
			CheckMenuItem(hMenu, IDM_MOUSE, (UI_MouseFlag)?MF_CHECKED:MF_UNCHECKED);
			Mouse_StartCapture(UI_MouseFlag);
			break;

		case IDM_NOWAIT:
			NoWaitMode ^= 1;
			CheckMenuItem(hMenu, IDM_NOWAIT, (NoWaitMode)?MF_CHECKED:MF_UNCHECKED);
			break;

		case IDM_JOYKEY:
			Config.JoyKey ^= 1;
			CheckMenuItem(hMenu, IDM_JOYKEY, (Config.JoyKey)?MF_CHECKED:MF_UNCHECKED);
			EnableMenuItem(hMenu, IDM_JOYKEYREVERSE, (Config.JoyKey)?MF_ENABLED:MF_GRAYED);
			EnableMenuItem(hMenu, IDM_JOYKEYJOY2, (Config.JoyKey)?MF_ENABLED:MF_GRAYED);
			break;

		case IDM_JOYKEYREVERSE:
			Config.JoyKeyReverse ^= 1;
			CheckMenuItem(hMenu, IDM_JOYKEYREVERSE, (Config.JoyKeyReverse)?MF_CHECKED:MF_UNCHECKED);
			break;

		case IDM_JOYKEYJOY2:
			Config.JoyKeyJoy2 ^= 1;
			CheckMenuItem(hMenu, IDM_JOYKEYJOY2, (Config.JoyKeyJoy2)?MF_CHECKED:MF_UNCHECKED);
			break;

		case IDM_SPEED_XVI:
			Config.XVIMode = 1;
			CheckMenuItem(hMenu, IDM_SPEED_XVI, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_SPEED_REDZONE, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_SPEED_NORMAL, MF_UNCHECKED);
			break;

		case IDM_SPEED_REDZONE:
			Config.XVIMode = 2;
			CheckMenuItem(hMenu, IDM_SPEED_XVI, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_SPEED_REDZONE, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_SPEED_NORMAL, MF_UNCHECKED);
			break;

		case IDM_SPEED_NORMAL:
			Config.XVIMode = 0;
			CheckMenuItem(hMenu, IDM_SPEED_XVI, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_SPEED_REDZONE, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_SPEED_NORMAL, MF_CHECKED);
			break;

		case IDM_AUTOSTRETCH:
			Config.WinStrech = 2;
			CheckMenuItem(hMenu, IDM_AUTOSTRETCH, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_NONSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_FIXEDSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_X68STRETCH, MF_UNCHECKED);
			WinDraw_ChangeSize();
			StatBar_Show((FullScreenFlag)?Config.FullScrFDDStat:Config.WindowFDDStat);
			SSTP_SendMes(SSTPMES_AUTOSTRETCH);
			break;

		case IDM_NONSTRETCH:
			Config.WinStrech = 0;
			CheckMenuItem(hMenu, IDM_AUTOSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_NONSTRETCH, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_FIXEDSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_X68STRETCH, MF_UNCHECKED);
			WinDraw_ChangeSize();
			StatBar_Show((FullScreenFlag)?Config.FullScrFDDStat:Config.WindowFDDStat);
			SSTP_SendMes(SSTPMES_NONSTRETCH);
			break;

		case IDM_FIXEDSTRETCH:
			Config.WinStrech = 1;
			CheckMenuItem(hMenu, IDM_AUTOSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_NONSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_FIXEDSTRETCH, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_X68STRETCH, MF_UNCHECKED);
			WinDraw_ChangeSize();
			StatBar_Show((FullScreenFlag)?Config.FullScrFDDStat:Config.WindowFDDStat);
			SSTP_SendMes(SSTPMES_FIXEDSTRETCH);
			break;

		case IDM_X68STRETCH:
			Config.WinStrech = 3;
			CheckMenuItem(hMenu, IDM_AUTOSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_NONSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_FIXEDSTRETCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_X68STRETCH, MF_CHECKED);
			WinDraw_ChangeSize();
			StatBar_Show((FullScreenFlag)?Config.FullScrFDDStat:Config.WindowFDDStat);
			SSTP_SendMes(SSTPMES_X68STRETCH);
			break;

#ifdef WIN68DEBUG
		case IDM_TRACE:
			traceflag ^= 1;
			CheckMenuItem(hMenu, IDM_TRACE, (traceflag) ? MF_CHECKED : MF_UNCHECKED);
			break;

		case IDM_DEBUGTEXT:
			Debug_Text ^= 1;
			TVRAM_SetAllDirty();
			CheckMenuItem(hMenu, IDM_DEBUGTEXT, (Debug_Text) ? MF_CHECKED : MF_UNCHECKED);
			break;
		case IDM_DEBUGGRP:
			Debug_Grp ^= 1;
			TVRAM_SetAllDirty();
			CheckMenuItem(hMenu, IDM_DEBUGGRP, (Debug_Grp) ? MF_CHECKED : MF_UNCHECKED);
			break;
		case IDM_DEBUGSP:
			Debug_Sp ^= 1;
			TVRAM_SetAllDirty();
			CheckMenuItem(hMenu, IDM_DEBUGSP, (Debug_Sp) ? MF_CHECKED : MF_UNCHECKED);
			break;
		case IDM_VREGSAVE:
			DSound_Stop();
//			sprintf(buf, "VCReg 0:$%02X%02X 1:$%02x%02X 2:$%02X%02X  CRTC$28/29=%02X/%02X  BG$10/11=%02X/%02X", VCReg0[0], VCReg0[1], VCReg1[0], VCReg1[1], VCReg2[0], VCReg2[1], CRTC_Regs[0x28], CRTC_Regs[0x29], BG_Regs[0x10], BG_Regs[0x11]);

sprintf(buf, "VCReg 0:$%02X%02X 1:$%02x%02X 2:$%02X%02X  CRTC00/02/05/06=%02X/%02X/%02X/%02X%02X  BGHT/HD/VD=%02X/%02X/%02X   $%02X/$%02X",
VCReg0[0], VCReg0[1], VCReg1[0], VCReg1[1], VCReg2[0], VCReg2[1],
CRTC_Regs[0x01], CRTC_Regs[0x05], CRTC_Regs[0x0b], CRTC_Regs[0x0c], CRTC_Regs[0x0d],
BG_Regs[0x0b], BG_Regs[0x0d], BG_Regs[0x0f],
CRTC_Regs[0x29], BG_Regs[0x11]);

			Error(buf);
			DSound_Play();
			break;
		case IDM_HDDTRACE:
			hddtrace ^= 1;
			CheckMenuItem(hMenu, IDM_HDDTRACE, (hddtrace) ? MF_CHECKED : MF_UNCHECKED);
			break;
		case IDM_DMATRACE:
			dmatrace ^= 1;
			CheckMenuItem(hMenu, IDM_DMATRACE, (dmatrace) ? MF_CHECKED : MF_UNCHECKED);
			break;
		case IDM_TIMERTRACE:
			timertrace ^= 1;
			CheckMenuItem(hMenu, IDM_TIMERTRACE, (timertrace) ? MF_CHECKED : MF_UNCHECKED);
			break;
#endif
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_ACTIVATE:
		Joystick_Activate(wParam);
		if (!HIWORD(wParam))
		{
			if (!FullScreenFlag)
			{
				WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
				MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);
				WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
				MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);
				StatBar_Show(Config.WindowFDDStat);
				Mouse_ChangePos();
			}

			Draw_DrawFlag = 1;
			WinDraw_Draw();
			if ( (LOWORD(wParam)==WA_INACTIVE) && (UI_MouseFlag||FullScreenFlag) )
			{
				Mouse_StartCapture(FALSE);
			}
			else if ( (LOWORD(wParam)==WA_ACTIVE)||(LOWORD(wParam)==WA_CLICKACTIVE) )
			{
				if (UI_MouseFlag||FullScreenFlag)
					Mouse_StartCapture(TRUE);
			}
		} else {
			if ( (LOWORD(wParam)==WA_INACTIVE) && (UI_MouseFlag||FullScreenFlag) )
			{
				Mouse_StartCapture(FALSE);
			}
		}
//		return DefWindowProc(hWnd, msg, wParam, lParam);
		break;

	case WM_PAINT:
		BeginPaint(hWnd, &ps);
		Draw_DrawFlag = 1;
		WinDraw_Draw();
		EndPaint(hWnd, &ps);
		break;

	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		if (wParam==VK_F11)
		{
			if (UI_MouseFlag||FullScreenFlag) Mouse_StartCapture(FALSE);
			if (FullScreenFlag) {
				SetMenu(hWndMain, hMenu);
				SSTP_SendMes(SSTPMES_FULLSCRHELP);
			}
			//WinDraw_ShowMenu(TRUE);
			return DefWindowProc(hWnd, WM_SYSKEYDOWN, VK_F10, lParam);
		}
		if (wParam==VK_F12) break;
		Keyboard_KeyDown((WORD)wParam, (DWORD)lParam);
		break;

	case WM_KEYUP:
	case WM_SYSKEYUP:
		if (wParam==VK_F11) return DefWindowProc(hWnd, WM_SYSKEYUP, VK_F10, lParam);
		if ((wParam==VK_F12)&&(!FullScreenFlag))
		{
			if (!UI_MouseFlag) SSTP_SendMes(SSTPMES_MOUSEHELP);
			UI_MouseFlag ^= 1;
			Mouse_StartCapture(UI_MouseFlag);
			CheckMenuItem(hMenu, IDM_MOUSE, (UI_MouseFlag)?MF_CHECKED:MF_UNCHECKED);
			break;
		}
		Keyboard_KeyUp((WORD)wParam, (DWORD)lParam);
		break;

	case WM_TIMER:
		WmTimer(hWnd, wParam, lParam);
		break;

	case WM_DRAWITEM:
		if (wParam == 1)
			StatBar_Draw((DRAWITEMSTRUCT*)lParam);
		return TRUE;

	case WM_ENTERMENULOOP:
		DSound_Stop();
		ShowCursor(TRUE);
		break;

	case WM_EXITMENULOOP:
		DSound_Play();
		if (FullScreenFlag)
		{
			SetMenu(hWndMain, NULL);
			StatBar_Redraw();
		}
		ShowCursor(FALSE);
		if ( (UI_MouseFlag||FullScreenFlag) && (GetActiveWindow()==hWndMain) )
		{
			SetActiveWindow(hWndMain);
			Mouse_StartCapture(TRUE);
		}
		break;

	case WM_ENTERSIZEMOVE:
		DSound_Stop();
		break;

	case WM_EXITSIZEMOVE:
		DSound_Play();
		break;

	case WM_MOVE:
		if (!FullScreenFlag) {
			GetWindowRect(hWnd, &rc);
			winx = rc.left;
			winy = rc.top;
		}
		break;

	case WM_MOUSEMOVE:
	case WM_LBUTTONDBLCLK:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
/*{
FILE *fp;
fp=fopen("scc.txt", "a");
fprintf(fp, "MouseEvent\n");
fclose(fp);
}*/
		Mouse_Event(wParam, lParam);
		break;

	case WM_MBUTTONDOWN: 
		if (!FullScreenFlag)
		{ 
			UI_MouseFlag ^= 1; 
			Mouse_StartCapture(UI_MouseFlag);
			CheckMenuItem(hMenu, IDM_MOUSE, (UI_MouseFlag)?MF_CHECKED:MF_UNCHECKED);
		} 
		else
		{
			Mouse_StartCapture(FALSE);
			SetMenu(hWndMain, hMenu);
			return DefWindowProc(hWnd, WM_SYSKEYDOWN, VK_F10, 0x00570001);
		}
	        break; 

	case WM_MBUTTONUP:
		if (FullScreenFlag)
			return DefWindowProc(hWnd, WM_SYSKEYUP, VK_F10, 0xc0570001);
		break;

	case WM_DROPFILES:
		WinUI_DropDisk((HDROP)wParam);
		break;

	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return 0L;
}

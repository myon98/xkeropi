/**
 * @file	cmmidioutvst.h
 * @brief	MIDI OUT VST クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#if defined(SUPPORT_VSTi)

#include "imidiout.h"
#include "../../misc/vsthost/vstbuffer.h"
#include "../../misc/vsthost/vsteditwnd.h"
#include "../../misc/vsthost/vsteffect.h"
#include "../../misc/vsthost/vstmidievent.h"

/**
 * @brief MIDI OUT VST クラス
 */
class MidiOutVst : public IMidiOut
{
public:
	static bool IsEnabled();
	static MidiOutVst* CreateInstance();

	MidiOutVst();
	virtual ~MidiOutVst();
	virtual void Short(uint32_t nMessage);
	virtual void Long(const uint8_t* lpMessage, uint32_t cbMessage);
	virtual void Process16(int16_t* lpBuffer, uint32_t nBufferCount);

private:
	UINT m_nBlockSize;			/*!< ブロック サイズ */
	UINT m_nIndex;				/*!< 読み取りインデックス */
	CVstEffect m_effect;		/*!< エフェクト */
	CVstEditWnd m_wnd;			/*!< ウィンドウ */
	CVstMidiEvent m_event;		/*!< イベント */
	CVstBuffer m_input;			/*!< 入力バッファ */
	CVstBuffer m_output;		/*!< 出力バッファ */

	bool Initialize(LPCTSTR lpPath);
};

#endif	// defined(SUPPORT_VSTi)

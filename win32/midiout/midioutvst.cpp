/**
 * @file	midioutvst.cpp
 * @brief	MIDI OUT VST クラスの動作の定義を行います
 */

#include "common.h"

#if defined(SUPPORT_VSTi)

#include "midioutvst.h"
#include <shlwapi.h>
#include "../prop.h"

#pragma comment(lib, "shlwapi.lib")

/**
 * VSTi モジュール ファイル名を得る
 * @param[out] lpModule VSTi モジュール ファイル名
 * @param[in] cchModule VSTi モジュール ファイル名のバッファの長さ
 */
static void GetPath(LPTSTR lpModule, UINT cchModule)
{
	const TCHAR szVSTiFile[] = TEXT("%ProgramFiles%\\Vstplugins\\Roland\\Sound Canvas VA\\SOUND Canvas VA.dll");
	::ExpandEnvironmentStrings(szVSTiFile, lpModule, cchModule);
}

/**
 * VSTi は有効か?
 * @retval true 有効
 * @retval false 無効
 */
bool MidiOutVst::IsEnabled()
{
	TCHAR szModule[MAX_PATH];
	GetPath(szModule, _countof(szModule));
	return (::PathFileExists(szModule) != FALSE);
}

/**
 * インスタンスを作成
 * @return インスタンス
 */
MidiOutVst* MidiOutVst::CreateInstance()
{
	MidiOutVst* pVst = new MidiOutVst;

	TCHAR szModule[MAX_PATH];
	GetPath(szModule, _countof(szModule));
	if (!pVst->Initialize(szModule))
	{
		delete pVst;
		pVst = NULL;
	}
	return pVst;
}

/**
 * コンストラクタ
 */
MidiOutVst::MidiOutVst()
	: m_nBlockSize(128)
	, m_nIndex(0)
{
}

/**
 * デストラクタ
 */
MidiOutVst::~MidiOutVst()
{
	m_wnd.Destroy();
	m_effect.Unload();
}

/**
 * 初期化
 * @param[in] lpPath パス
 * @retval true 成功
 * @retval false 失敗
 */
bool MidiOutVst::Initialize(LPCTSTR lpPath)
{
	if (Config.SampleRate == 0)
	{
		return false;
	}

	if (!m_effect.Load(lpPath))
	{
		printf("Cloudn't attach VSTi.\n");
		return false;
	}

	// Effect をオープン
	m_effect.open();

	// サンプリング レートを設定
	m_effect.setSampleRate(static_cast<float>(Config.SampleRate));

	// ブロックサイズを設定
	m_effect.setBlockSize(m_nBlockSize);
	m_effect.resume();

	m_effect.beginSetProgram();
	m_effect.setProgram(0);
	m_effect.endSetProgram();

	m_input.Alloc(2, m_nBlockSize);
	m_output.Alloc(2, m_nBlockSize);

	m_wnd.Create(&m_effect, TEXT("Keropi VSTi"), WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX);

	return true;
}

/**
 * ショート メッセージ
 * @param[in] nMessage メッセージ
 */
void MidiOutVst::Short(uint32_t nMessage)
{
	m_event.ShortMessage(m_nIndex, nMessage);
}

/**
 * ロング メッセージ
 * @param[in] lpMessage メッセージ ポインタ
 * @param[in] cbMessage メッセージ サイズ
 */
void MidiOutVst::Long(const uint8_t* lpMessage, uint32_t cbMessage)
{
	m_event.LongMessage(m_nIndex, lpMessage, cbMessage);
}

/**
 * プロセス (16bit)
 * @param[out] lpBuffer バッファ
 * @param[in] nBufferCount サンプル数
 */
void MidiOutVst::Process16(int16_t* lpBuffer, uint32_t nBufferCount)
{
	while (nBufferCount)
	{
		if (m_nIndex >= m_nBlockSize)
		{
			m_nIndex = 0;
			m_effect.processEvents(m_event.GetEvents());
			m_effect.processReplacing(m_input.GetBuffer(), m_output.GetBuffer(), m_nBlockSize);
			m_event.Clear();
		}

		UINT nSize = m_nBlockSize - m_nIndex;
		nSize = min(nSize, nBufferCount);
		nBufferCount -= nSize;
		float** output = m_output.GetBuffer();
		do
		{
			lpBuffer[0] += static_cast<int16_t>(output[0][m_nIndex] * 32767.0f - 0.5f);
			lpBuffer[1] += static_cast<int16_t>(output[1][m_nIndex] * 32767.0f - 0.5f);
			lpBuffer += 2;
			m_nIndex++;
		} while (--nSize);
	}
}

#endif	// defined(SUPPORT_VSTi)

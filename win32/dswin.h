#ifndef winx1_dsound_h
#define winx1_dsound_h

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

LPCTSTR DSound_GetDeviceName(UINT nNum);
int DSound_Init(unsigned long rate, unsigned long length);
void DSound_Cleanup(void);

void DSound_Play(void);
void DSound_Stop(void);
void FASTCALL DSound_Send0(long clock);
void FASTCALL DSound_Send(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //winx1_dsound_h

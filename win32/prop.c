// ---------------------------------------------------------------------------------------
//  PROP.C - 各種設定用プロパティシートと設定値管理
// ---------------------------------------------------------------------------------------

#include "common.h"
#include <commctrl.h>
#include <commdlg.h>
#include "resource.h"
#include "winx68k.h"
#include "dswin.h"
#include "keyboard.h"
#include "keyconf.h"
#include "fileio.h"
#include "prop.h"
#include "cdaspi.h"
#include "cdrom.h"
#include "joystick.h"
#include "midiout.h"
#include "windraw.h"
#include "winui.h"
#ifdef USE_ROMEO
#include "juliet.h"
#endif	// USE_ROMEO

BYTE	LastCode = 0;
int	CurrentHDDNo = 0;

static const int SSTPPorts[] = {11000, 9801, 0};

static const TCHAR KEYCONFFILE[] = TEXT("keyconf.dat");
static const TCHAR ini_title[] = TEXT("WinX68k");
static const TCHAR X68KeyName[128][10] = {
	TEXT("         "), TEXT("ESC"),       TEXT("1"),         TEXT("2"),         TEXT("3"),         TEXT("4"),         TEXT("5"),         TEXT("6"),
	TEXT("7"),         TEXT("8"),         TEXT("9"),         TEXT("0"),         TEXT("-"),         TEXT("^"),         TEXT("\\"),        TEXT("BS"),
	TEXT("TAB"),       TEXT("Q"),         TEXT("W"),         TEXT("E"),         TEXT("R"),         TEXT("T"),         TEXT("Y"),         TEXT("U"),
	TEXT("I"),         TEXT("O"),         TEXT("P"),         TEXT("@"),         TEXT("["),         TEXT("ENTER"),     TEXT("A"),         TEXT("S"),
	TEXT("D"),         TEXT("F"),         TEXT("G"),         TEXT("H"),         TEXT("J"),         TEXT("K"),         TEXT("L"),         TEXT(";"),
	TEXT(":"),         TEXT("]"),         TEXT("Z"),         TEXT("X"),         TEXT("C"),         TEXT("V"),         TEXT("B"),         TEXT("N"),
	TEXT("M"),         TEXT(","),         TEXT("."),         TEXT("/"),         TEXT("_"),         TEXT("         "), TEXT("HOME"),      TEXT("DEL"),
	TEXT("ROLL UP"),   TEXT("ROLL DOWN"), TEXT("UNDO"),      TEXT("←"),        TEXT("↑"),        TEXT("→"),        TEXT("↓"),        TEXT("CLR"),
	TEXT("/(10キー)"), TEXT("*(10キー)"), TEXT("-(10キー)"), TEXT("7(10キー)"), TEXT("8(10キー)"), TEXT("9(10キー)"), TEXT("+(10キー)"), TEXT("4(10キー)"),
	TEXT("5(10キー)"), TEXT("6(10キー)"), TEXT("=(10キー)"), TEXT("1(10キー)"), TEXT("2(10キー)"), TEXT("3(10キー)"), TEXT("ENTER(10)"), TEXT("0(10キー)"),
	TEXT(",(10キー)"), TEXT(".(10キー)"), TEXT("記号入力"),  TEXT("登録"),      TEXT("HELP"),      TEXT("XF1"),       TEXT("XF2"),       TEXT("XF3"),
	TEXT("XF4"),       TEXT("XF5"),       TEXT("かな"),      TEXT("ローマ字"),  TEXT("カナ入力"),  TEXT("CAPS"),      TEXT("INS"),       TEXT("ひらがな"),
	TEXT("全角"),      TEXT("BREAK"),     TEXT("COPY"),      TEXT("F1"),        TEXT("F2"),        TEXT("F3"),        TEXT("F4"),        TEXT("F5"),
	TEXT("F6"),        TEXT("F7"),        TEXT("F8"),        TEXT("F9"),        TEXT("F10"),       TEXT("         "), TEXT("         "), TEXT("         "),
	TEXT("SHIFT"),     TEXT("CTRL"),      TEXT("OPT.1"),     TEXT("OPT.2"),     TEXT("         "), TEXT("         "), TEXT("         "), TEXT("         "),
	TEXT("         "), TEXT("         "), TEXT("         "), TEXT("         "), TEXT("         "), TEXT("         "), TEXT("         "), TEXT("         "),
};

static const TCHAR MIDI_TYPE_NAME[4][8] = {
	TEXT("LA音源"), TEXT("GM音源"), TEXT("GS音源"), TEXT("XG音源")
};

BYTE KeyTableBk[512];

Win68Conf Config;
Win68Conf ConfBk;

#define CFGLEN MAX_PATH
#define BSTATE(b) (b ? BST_CHECKED : BST_UNCHECKED)

static LPCTSTR makeBOOL(BYTE value) {

	if (value) {
		return(TEXT("true"));
	}
	return(TEXT("false"));
}

static BYTE Aacmp(LPCTSTR cmp, LPCTSTR str) {

	TCHAR	p;

	while(*str) {
		p = *cmp++;
		if (!p) {
			break;
		}
		if (p >= 'a' && p <= 'z') {
			p -= 0x20;
		}
		if (p != *str++) {
			return(-1);
		}
	}
	return(0);
}

static BYTE solveBOOL(LPCTSTR str) {

	if ((!Aacmp(str, TEXT("TRUE"))) || (!Aacmp(str, TEXT("ON"))) ||
		(!Aacmp(str, TEXT("+"))) || (!Aacmp(str, TEXT("1"))) ||
		(!Aacmp(str, TEXT("ENABLE")))) {
		return(1);
	}
	return(0);
}


void LoadConfig(void)
{
	int	i, j;
	TCHAR	buf[CFGLEN];
	FILEH fp;

	winx = GetPrivateProfileInt(ini_title, TEXT("WinPosX"), CW_USEDEFAULT, winx68k_ini);
	winy = GetPrivateProfileInt(ini_title, TEXT("WinPosY"), CW_USEDEFAULT, winx68k_ini);
	FrameRate = (BYTE)GetPrivateProfileInt(ini_title, TEXT("FrameRate"), 7, winx68k_ini);
	if (!FrameRate) FrameRate=7;
	GetPrivateProfileString(ini_title, TEXT("StartDir"), TEXT(""), buf, MAX_PATH, winx68k_ini);
	if (buf[0] != 0)
		lstrcpy(filepath, buf);
	else
		filepath[0] = 0;

	Config.OPM_VOL = GetPrivateProfileInt(ini_title, TEXT("OPM_Volume"), 12, winx68k_ini);
	Config.PCM_VOL = GetPrivateProfileInt(ini_title, TEXT("PCM_Volume"), 15, winx68k_ini);
	Config.MCR_VOL = GetPrivateProfileInt(ini_title, TEXT("MCR_Volume"), 13, winx68k_ini);
	Config.SampleRate = GetPrivateProfileInt(ini_title, TEXT("SampleRate"), 22050, winx68k_ini);
	Config.BufferSize = GetPrivateProfileInt(ini_title, TEXT("BufferSize"), 50, winx68k_ini);

	Config.MouseSpeed = GetPrivateProfileInt(ini_title, TEXT("MouseSpeed"), 10, winx68k_ini);

	GetPrivateProfileString(ini_title, TEXT("FDDStatWin"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.WindowFDDStat = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("FDDStatFullScr"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.FullScrFDDStat = solveBOOL(buf);

	GetPrivateProfileString(ini_title, TEXT("DSAlert"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.DSAlert = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("SoundDevice"), DSound_GetDeviceName(0), Config.SoundDevice, _countof(Config.SoundDevice), winx68k_ini);
	GetPrivateProfileString(ini_title, TEXT("SoundLPF"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.Sound_LPF = solveBOOL(buf);
#ifdef USE_ROMEO
	GetPrivateProfileString(ini_title, TEXT("UseRomeo"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.SoundROMEO = solveBOOL(buf);
#endif	// USE_ROMEO
	GetPrivateProfileString(ini_title, TEXT("MIDI_SW"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.MIDI_SW = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("MIDI_Reset"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.MIDI_Reset = solveBOOL(buf);
	Config.MIDI_Type = GetPrivateProfileInt(ini_title, TEXT("MIDI_Type"), 1, winx68k_ini);
	GetPrivateProfileString(ini_title, TEXT("MIDI_Module"), MidiOut_GetModuleName(0), Config.MIDI_Module, _countof(Config.MIDI_Module), winx68k_ini);

	GetPrivateProfileString(ini_title, TEXT("JoySwap"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.JoySwap = solveBOOL(buf);

	GetPrivateProfileString(ini_title, TEXT("JoyKey"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.JoyKey = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("JoyKeyReverse"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.JoyKeyReverse = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("JoyKeyJoy2"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.JoyKeyJoy2 = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("SRAMBootWarning"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.SRAMWarning = solveBOOL(buf);

	GetPrivateProfileString(ini_title, TEXT("WinDrvLFN"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.LongFileName = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("WinDrvFDD"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.WinDrvFD = solveBOOL(buf);

	Config.WinStrech = GetPrivateProfileInt(ini_title, TEXT("WinStretch"), 1, winx68k_ini);

	GetPrivateProfileString(ini_title, TEXT("DSMixing"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.DSMixing = solveBOOL(buf);

	Config.XVIMode = (BYTE)GetPrivateProfileInt(ini_title, TEXT("XVIMode"), 0, winx68k_ini);

	GetPrivateProfileString(ini_title, TEXT("CDROM_ASPI"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.CDROM_ASPI = solveBOOL(buf);
	Config.CDROM_SCSIID = (BYTE)GetPrivateProfileInt(ini_title, TEXT("CDROM_SCSIID"), 6, winx68k_ini);
	Config.CDROM_ASPI_Drive = (BYTE)GetPrivateProfileInt(ini_title, TEXT("CDROM_ASPIDrv"), 0, winx68k_ini);
	Config.CDROM_IOCTRL_Drive = (BYTE)GetPrivateProfileInt(ini_title, TEXT("CDROM_CTRLDrv"), 16, winx68k_ini);
	GetPrivateProfileString(ini_title, TEXT("CDROM_Enable"), TEXT("1"), buf, CFGLEN, winx68k_ini);
	Config.CDROM_Enable = solveBOOL(buf);

	GetPrivateProfileString(ini_title, TEXT("SSTP_Enable"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.SSTP_Enable = solveBOOL(buf);
	Config.SSTP_Port = GetPrivateProfileInt(ini_title, TEXT("SSTP_Port"), 11000, winx68k_ini);

	GetPrivateProfileString(ini_title, TEXT("ToneMapping"), TEXT("0"), buf, CFGLEN, winx68k_ini);
	Config.ToneMap = solveBOOL(buf);
	GetPrivateProfileString(ini_title, TEXT("ToneMapFile"), TEXT(""), buf, MAX_PATH, winx68k_ini);
	if (buf[0] != 0)
		lstrcpy(Config.ToneMapFile, buf);
	else
		Config.ToneMapFile[0] = 0;

	Config.MIDIDelay = GetPrivateProfileInt(ini_title, TEXT("MIDIDelay"), Config.BufferSize*5, winx68k_ini);
	Config.MIDIAutoDelay = GetPrivateProfileInt(ini_title, TEXT("MIDIAutoDelay"), 1, winx68k_ini);

	for (i=0; i<2; i++)
	{
		for (j=0; j<8; j++)
		{
			wsprintf(buf, TEXT("Joy%dButton%d"), i+1, j+1);
			Config.JOY_BTN[i][j] = GetPrivateProfileInt(ini_title, buf, j, winx68k_ini);
		}
	}

	for (i=0; i<16; i++)
	{
		wsprintf(buf, TEXT("HDD%d"), i);
		GetPrivateProfileString(ini_title, buf, TEXT(""), Config.HDImage[i], MAX_PATH, winx68k_ini);
	}

	fp = File_OpenCurDir(KEYCONFFILE);
	if (fp)
	{
		File_Read(fp, KeyTable, 512);
		File_Close(fp);
	}
}


void SaveConfig(void)
{
	int	i, j;
	TCHAR	buf[CFGLEN], buf2[CFGLEN];
	FILEH fp;

	wsprintf(buf, TEXT("%d"), winx);
	WritePrivateProfileString(ini_title, TEXT("WinPosX"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), winy);
	WritePrivateProfileString(ini_title, TEXT("WinPosY"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), FrameRate);
	WritePrivateProfileString(ini_title, TEXT("FrameRate"), buf, winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("StartDir"), filepath, winx68k_ini);

	wsprintf(buf, TEXT("%d"), Config.OPM_VOL);
	WritePrivateProfileString(ini_title, TEXT("OPM_Volume"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.PCM_VOL);
	WritePrivateProfileString(ini_title, TEXT("PCM_Volume"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.MCR_VOL);
	WritePrivateProfileString(ini_title, TEXT("MCR_Volume"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.SampleRate);
	WritePrivateProfileString(ini_title, TEXT("SampleRate"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.BufferSize);
	WritePrivateProfileString(ini_title, TEXT("BufferSize"), buf, winx68k_ini);

	wsprintf(buf, TEXT("%d"), Config.MouseSpeed);
	WritePrivateProfileString(ini_title, TEXT("MouseSpeed"), buf, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("FDDStatWin"), makeBOOL((BYTE)Config.WindowFDDStat), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("FDDStatFullScr"), makeBOOL((BYTE)Config.FullScrFDDStat), winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("SoundDevice"), Config.SoundDevice, winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("SoundLPF"), makeBOOL((BYTE)Config.Sound_LPF), winx68k_ini);
#ifdef USE_ROMEO
	WritePrivateProfileString(ini_title, TEXT("UseRomeo"), makeBOOL((BYTE)Config.SoundROMEO), winx68k_ini);
#endif	// USE_ROMEO
	WritePrivateProfileString(ini_title, TEXT("MIDI_SW"), makeBOOL((BYTE)Config.MIDI_SW), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("MIDI_Reset"), makeBOOL((BYTE)Config.MIDI_Reset), winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.MIDI_Type);
	WritePrivateProfileString(ini_title, TEXT("MIDI_Type"), buf, winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("MIDI_Module"), Config.MIDI_Module, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("JoySwap"), makeBOOL((BYTE)Config.JoySwap), winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("JoyKey"), makeBOOL((BYTE)Config.JoyKey), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("JoyKeyReverse"), makeBOOL((BYTE)Config.JoyKeyReverse), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("JoyKeyJoy2"), makeBOOL((BYTE)Config.JoyKeyJoy2), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("SRAMBootWarning"), makeBOOL((BYTE)Config.SRAMWarning), winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("WinDrvLFN"), makeBOOL((BYTE)Config.LongFileName), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("WinDrvFDD"), makeBOOL((BYTE)Config.WinDrvFD), winx68k_ini);

	wsprintf(buf, TEXT("%d"), Config.WinStrech);
	WritePrivateProfileString(ini_title, TEXT("WinStretch"), buf, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("DSMixing"), makeBOOL((BYTE)Config.DSMixing), winx68k_ini);

	wsprintf(buf, TEXT("%d"), Config.XVIMode);
	WritePrivateProfileString(ini_title, TEXT("XVIMode"), buf, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("CDROM_ASPI"), makeBOOL((BYTE)Config.CDROM_ASPI), winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.CDROM_SCSIID);
	WritePrivateProfileString(ini_title, TEXT("CDROM_SCSIID"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.CDROM_ASPI_Drive);
	WritePrivateProfileString(ini_title, TEXT("CDROM_ASPIDrv"), buf, winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.CDROM_IOCTRL_Drive);
	WritePrivateProfileString(ini_title, TEXT("CDROM_CTRLDrv"), buf, winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("CDROM_Enable"), makeBOOL((BYTE)Config.CDROM_Enable), winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("SSTP_Enable"), makeBOOL((BYTE)Config.SSTP_Enable), winx68k_ini);
	wsprintf(buf, TEXT("%d"), Config.SSTP_Port);
	WritePrivateProfileString(ini_title, TEXT("SSTP_Port"), buf, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("ToneMapping"), makeBOOL((BYTE)Config.ToneMap), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("ToneMapFile"), Config.ToneMapFile, winx68k_ini);

	wsprintf(buf, TEXT("%d"), Config.MIDIDelay);
	WritePrivateProfileString(ini_title, TEXT("MIDIDelay"), buf, winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("MIDIAutoDelay"), makeBOOL((BYTE)Config.MIDIAutoDelay), winx68k_ini);

	for (i=0; i<2; i++)
	{
		for (j=0; j<8; j++)
		{
			wsprintf(buf, TEXT("Joy%dButton%d"), i+1, j+1);
			wsprintf(buf2, TEXT("%d"), Config.JOY_BTN[i][j]);
			WritePrivateProfileString(ini_title, buf, buf2, winx68k_ini);
		}
	}

	for (i=0; i<16; i++)
	{
		wsprintf(buf, TEXT("HDD%d"), i);
		WritePrivateProfileString(ini_title, buf, Config.HDImage[i], winx68k_ini);
	}

	fp = File_OpenCurDir(KEYCONFFILE);
	if (!fp)
		fp = File_CreateCurDir(KEYCONFFILE);
	if (fp)
	{
		File_Write(fp, KeyTable, 512);
		File_Close(fp);
	}
}


static LRESULT CALLBACK PropDialogProc(HWND hDlg, UINT Msg, WPARAM wParam, LPARAM lParam);
void PropPage_Init(void);

#define NUM_PROPSHEETS  8

static DWORD dwDlgId[] = {
	IDD_PROP_SOUND,
	IDD_PROP_MIDI,
	IDD_PROP_KEYBOARD,
	IDD_PROP_JOYSTICK1,
	IDD_PROP_JOYSTICK2,
	IDD_PROP_MOUSE,
	IDD_PROP_SCSI,
	IDD_PROP_OTHERS,
};

void PropPage_Init(void)
{
	PROPSHEETPAGE	pspage[NUM_PROPSHEETS];
	PROPSHEETHEADER	pshead;
	int i;

	pshead.hwndParent     = hWndMain;
	pshead.dwSize         = sizeof(PROPSHEETHEADER);
	pshead.dwFlags        = PSH_PROPSHEETPAGE | PSH_USEICONID ;
	pshead.hInstance      = hInst;
	pshead.pszCaption     = TEXT("けろぴーの行動設定");
	pshead.nPages         = NUM_PROPSHEETS;
	pshead.nStartPage     = 0;
	pshead.pszIcon        = MAKEINTRESOURCE(IDI_MAIN_ICON);
	pshead.ppsp           = pspage;

	for (i = 0; i < NUM_PROPSHEETS; i++)
	{
		pspage[i].dwSize      = sizeof(PROPSHEETPAGE);
		pspage[i].dwFlags     = 0;
		pspage[i].hInstance   = hInst;
		pspage[i].pszTemplate = MAKEINTRESOURCE(dwDlgId[i]);
		pspage[i].pfnCallback = NULL;
		pspage[i].lParam      = 0;
		pspage[i].pfnDlgProc  = (DLGPROC)PropDialogProc;
	}

	if (PropertySheet(&pshead) == -1)
		Error("プロパティシートの作成に失敗しました。");
}

static void Setup_SoundPage(HWND hDlg)
{
	UINT i = 0;
	while (TRUE /*CONSTCOND*/)
	{
		LPCTSTR lpName = DSound_GetDeviceName(i++);
		if (!lpName)
		{
			break;
		}
		SendDlgItemMessage(hDlg, IDC_SOUNDDEVICE, CB_ADDSTRING, 0, (LPARAM)lpName);
	}
	SendDlgItemMessage(hDlg, IDC_SOUNDDEVICE, CB_SELECTSTRING, 0, (LPARAM)Config.SoundDevice);

	CheckDlgButton(hDlg, IDC_RATE_NON, BSTATE((Config.SampleRate==0)));
	CheckDlgButton(hDlg, IDC_RATE_48,  BSTATE((Config.SampleRate==48000)));
	CheckDlgButton(hDlg, IDC_RATE_44,  BSTATE((Config.SampleRate==44100)));
	CheckDlgButton(hDlg, IDC_RATE_22,  BSTATE((Config.SampleRate==22050)));
	CheckDlgButton(hDlg, IDC_RATE_11,  BSTATE((Config.SampleRate==11025)));

	SendDlgItemMessage(hDlg, IDC_OPMVOL, TBM_SETRANGE, TRUE, MAKELONG(0, 16));
	SendDlgItemMessage(hDlg, IDC_OPMVOL, TBM_SETPOS, TRUE, Config.OPM_VOL);
	SetDlgItemInt(hDlg, IDC_OPMVOL_TEXT, Config.OPM_VOL, FALSE);
	SendDlgItemMessage(hDlg, IDC_PCMVOL, TBM_SETRANGE, TRUE, MAKELONG(0, 16));
	SendDlgItemMessage(hDlg, IDC_PCMVOL, TBM_SETPOS, TRUE, Config.PCM_VOL);
	SetDlgItemInt(hDlg, IDC_PCMVOL_TEXT, Config.PCM_VOL, FALSE);
	SendDlgItemMessage(hDlg, IDC_MCRVOL, TBM_SETRANGE, TRUE, MAKELONG(0, 16));
	SendDlgItemMessage(hDlg, IDC_MCRVOL, TBM_SETPOS, TRUE, Config.MCR_VOL);
	SetDlgItemInt(hDlg, IDC_MCRVOL_TEXT, Config.MCR_VOL, FALSE);
	SendDlgItemMessage(hDlg, IDC_BUFSIZE, TBM_SETRANGE, TRUE, MAKELONG(1, 200));
	SendDlgItemMessage(hDlg, IDC_BUFSIZE, TBM_SETPOS, TRUE, Config.BufferSize);
	SetDlgItemInt(hDlg, IDC_BUFSIZE_TEXT, Config.BufferSize, FALSE);
	EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE), (Config.SampleRate!=0));
	EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE_TEXT), (Config.SampleRate!=0));
}

static void Update_MIDIPage(HWND hDlg)
{
	const BOOL bEnabled = (Config.MIDI_SW) ? TRUE : FALSE;
	EnableWindow(GetDlgItem(hDlg, IDC_MIDIMODULE), bEnabled);
	EnableWindow(GetDlgItem(hDlg, IDC_MIDITYPE), bEnabled);
	EnableWindow(GetDlgItem(hDlg, IDC_MIDITEXT), bEnabled);
	EnableWindow(GetDlgItem(hDlg, IDC_MIDIRESET), bEnabled);
	EnableWindow(GetDlgItem(hDlg, IDC_MIMPIMAP), bEnabled);
	EnableWindow(GetDlgItem(hDlg, IDC_MIMPIFILE), bEnabled);
	EnableWindow(GetDlgItem(hDlg, IDC_MIMPIFILESEL), bEnabled);
}

static void Update_MIDIDelay(HWND hDlg)
{
	TCHAR buf[32];
	wsprintf(buf, TEXT("%dms"), Config.MIDIDelay);
	SetDlgItemText(hDlg, IDC_MIDIDELAYTEXT, buf);
}

static void Setup_MIDIPage(HWND hDlg)
{
	UINT i;

	CheckDlgButton(hDlg, IDC_MIDI, BSTATE(Config.MIDI_SW));
	CheckDlgButton(hDlg, IDC_MIDIRESET, BSTATE(Config.MIDI_Reset));

	i = 0;
	while (TRUE /*CONSTCOND*/)
	{
		LPCTSTR lpName = MidiOut_GetModuleName(i++);
		if (lpName == NULL)
		{
			break;
		}
		SendDlgItemMessage(hDlg, IDC_MIDIMODULE, CB_ADDSTRING, 0, (LPARAM)lpName);
	}
	SendDlgItemMessage(hDlg, IDC_MIDIMODULE, CB_SELECTSTRING, 0, (LPARAM)Config.MIDI_Module);

	for (i = 0; i < 4; i++)
		SendDlgItemMessage(hDlg, IDC_MIDITYPE, CB_ADDSTRING, 0, (LPARAM)MIDI_TYPE_NAME[i]);
	SendDlgItemMessage(hDlg, IDC_MIDITYPE, CB_SELECTSTRING, 0, (LPARAM)MIDI_TYPE_NAME[Config.MIDI_Type]);

	if ( Config.MIDIAutoDelay ) {
		Config.MIDIDelay = Config.BufferSize*5;
	}
	EnableWindow(GetDlgItem(hDlg, IDC_MIDIDELAY), !Config.MIDIAutoDelay);
	CheckDlgButton(hDlg, IDC_MIDIDELAYAUTO, BSTATE(Config.MIDIAutoDelay));
	SendDlgItemMessage(hDlg, IDC_MIDIDELAY, TBM_SETRANGE, TRUE, MAKELONG(0, 1000));
	SendDlgItemMessage(hDlg, IDC_MIDIDELAY, TBM_SETPOS, TRUE, Config.MIDIDelay);

	Update_MIDIPage(hDlg);
	Update_MIDIDelay(hDlg);
}

void Setup_JoystickPage(HWND hDlg)
{
	int i;
	char *nobutton="なし";

	if (Joystick_Available(0))
	{
		SetDlgItemText(hDlg, IDC_JOY1_NAME, joyname[0]);

		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN3), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN4), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN5), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN6), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN7), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN8), TRUE);

		SendDlgItemMessage(hDlg, IDC_JOY1_BTN1, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN2, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN3, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN4, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN5, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN6, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN7, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY1_BTN8, CB_ADDSTRING, 0, (LPARAM)nobutton);
		for (i=0; i<joybtnnum[0]; i++)
		{
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN1, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN2, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN3, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN4, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN5, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN6, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN7, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN8, CB_ADDSTRING, 0, (LPARAM)joybtnname[0][i]);
		}
		if (Config.JOY_BTN[0][0]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN1, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][0]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN1, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][1]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN2, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][1]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN2, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][2]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN3, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][2]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN3, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][3]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN4, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][3]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN4, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][4]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN5, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][4]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN5, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][5]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN6, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][5]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN6, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][6]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN7, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][6]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN7, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[0][7]<joybtnnum[0])
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN8, CB_SELECTSTRING, 0, (LPARAM)joybtnname[0][Config.JOY_BTN[0][7]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY1_BTN8, CB_SELECTSTRING, 0, (LPARAM)nobutton);
	}
	else
	{
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY1_BTN8), FALSE);
	}

	if (Joystick_Available(1))
	{
		SetDlgItemText(hDlg, IDC_JOY2_NAME, joyname[1]);

		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN3), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN4), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN5), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN6), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN7), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN8), TRUE);

		SendDlgItemMessage(hDlg, IDC_JOY2_BTN1, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN2, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN3, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN4, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN5, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN6, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN7, CB_ADDSTRING, 0, (LPARAM)nobutton);
		SendDlgItemMessage(hDlg, IDC_JOY2_BTN8, CB_ADDSTRING, 0, (LPARAM)nobutton);
		for (i=0; i<joybtnnum[1]; i++)
		{
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN1, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN2, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN3, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN4, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN5, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN6, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN7, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN8, CB_ADDSTRING, 0, (LPARAM)joybtnname[1][i]);
		}
		if (Config.JOY_BTN[1][0]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN1, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][0]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN1, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][1]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN2, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][1]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN2, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][2]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN3, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][2]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN3, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][3]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN4, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][3]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN4, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][4]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN5, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][4]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN5, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][5]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN6, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][51]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN6, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][6]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN7, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][6]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN7, CB_SELECTSTRING, 0, (LPARAM)nobutton);
		if (Config.JOY_BTN[1][7]<joybtnnum[1])
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN8, CB_SELECTSTRING, 0, (LPARAM)joybtnname[1][Config.JOY_BTN[1][7]]);
		else
			SendDlgItemMessage(hDlg, IDC_JOY2_BTN8, CB_SELECTSTRING, 0, (LPARAM)nobutton);
	}
	else
	{
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_JOY2_BTN8), FALSE);
	}
}


// キーアサイン変更
void Prop_KeyCodeSet(HWND hDlg, BYTE key)
{
//	int i;
	WORD code;
	TCHAR buf[200], keyname[20];

	wsprintf(KeyConfMessage, TEXT("X68kの「%s」キーに割り当てたいキーを押してください。\n\n（F12キーでキャンセル）"), X68KeyName[key]);
	SetDlgItemText(hDlg, IDC_KEYCONF_TEXT, KeyConfMessage);
//	SetDlgItemText(hDlg, IDC_KEYCONF_DLG, KeyConfMessage);

	hWndKeyConf = (HWND)DialogBox(hInst, MAKEINTRESOURCE(IDD_KEYCONF), hDlg, (DLGPROC)KeyConfProc);
	if (!KeyConf_CodeW)
	{
		wsprintf(buf, TEXT("キャンセルしました。"));
		SetDlgItemText(hDlg, IDC_KEYCONF_TEXT, buf);
		return;			// 変更なし
	}

	code = (WORD)((KeyConf_CodeW&0xff)|((KeyConf_CodeL>>16)&0x0100));
/*
	for (i=0; i<512; i++)
	{
		if ( (KeyTable[i]==key) && (i!=code) )
		{
		}
	}
*/
	KeyTable[code] = key;

	GetKeyNameText(KeyConf_CodeL, keyname, 20);
	wsprintf(buf, TEXT("X68kの「%s」キーとして、Windowsの「%s」キーを割り当てました。"), X68KeyName[key], keyname);
	SetDlgItemText(hDlg, IDC_KEYCONF_TEXT, buf);
}


FILEH MakeHDDFile = NULL;
int MakeHDDSize = 0;
int MakeHDDCurSize = 0;


static LRESULT CALLBACK MakeHDDProc(HWND hDlg,UINT Msg,WPARAM wParam,LPARAM lParam)
{
	int i, j;
	char* hddbuf, hddbuf2;
	TCHAR buf[200];
	switch (Msg)
	{
	case WM_INITDIALOG :
		SendDlgItemMessage(hDlg, IDC_PROGBAR, PBM_SETRANGE, 0, MAKELPARAM(0,MakeHDDSize));
		MakeHDDCurSize = 0;

		if (MakeHDDFile)
			SetTimer(hDlg,0,1,NULL);
		else
			EndDialog(hDlg,0);
			
		return 1;

	case WM_TIMER :
		for (j=0; j<10; j++)
		{
			if (MakeHDDCurSize != MakeHDDSize)
			{
				wsprintf(buf, TEXT("Making HDD Image ...\n(%d/%d)"), MakeHDDCurSize, MakeHDDSize);
			        SetDlgItemText( hDlg, IDC_PROGTEXT, buf );
				hddbuf = (char *)malloc(0x1000);
				if (hddbuf)
				{
					ZeroMemory(hddbuf, 0x1000);
					File_Write(MakeHDDFile, hddbuf, 0x1000);
					free(hddbuf);
				}
				else
				{
					hddbuf2 = 0;
					for (i=0; i<0x1000; i++) File_Write(MakeHDDFile, &hddbuf2, 1);
				}
				MakeHDDCurSize++;
				SendDlgItemMessage(hDlg, IDC_PROGBAR, PBM_SETPOS, MakeHDDCurSize, 0);
			}
			else
			{
				KillTimer(hDlg,0);
				EndDialog(hDlg,0);
				break;
			}
		}
		break;
  
	case WM_COMMAND :
		switch(LOWORD(wParam))
		{
		case IDCANCEL:
		//	EndDialog(hDlg,0);
			break;
		}
		return 1;
	}
	return 0;
}


static LRESULT CALLBACK PropDialogProc(HWND hDlg, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	TCHAR buf[255];
	TCHAR keyname[20];
	int temp;
	int i, j;
	WORD wp;
	unsigned int id, code;
	DWORD keycode;
	OPENFILENAME ofn;
	int isopen, openflag = 0;
	TCHAR filename[MAX_PATH];
	FILEH fp;

/*{
FILE *fp;
fp=fopen("_mes.txt", "a");
fprintf(fp, "Message - Mes:%08X  WP:%04X  LP:%04X\n", Msg, wParam, lParam);
fclose(fp);
}*/
	switch (Msg)
	{
	case WM_INITDIALOG:
		{
			HWND hWnd = PropSheet_GetTabControl(GetParent(hDlg));
			DWORD tabStyle = (GetWindowLong(hWnd,GWL_STYLE) & ~TCS_MULTILINE);
			SetWindowLong(hWnd,GWL_STYLE,tabStyle | TCS_SINGLELINE);

			memcpy(&ConfBk, &Config, sizeof(Win68Conf));
			memcpy(KeyTableBk, KeyTable, 512);

			Setup_SoundPage(hDlg);

			Setup_MIDIPage(hDlg);

			SendDlgItemMessage(hDlg, IDC_MOUSESPEED, TBM_SETRANGE, TRUE, MAKELONG(1, 20));
			SendDlgItemMessage(hDlg, IDC_MOUSESPEED, TBM_SETPOS, TRUE, Config.MouseSpeed);

			CheckDlgButton(hDlg, IDC_STATUS, BSTATE(Config.WindowFDDStat));
			CheckDlgButton(hDlg, IDC_FULLSCREENFDD, BSTATE(Config.FullScrFDDStat));

			CheckDlgButton(hDlg, IDC_SOUNDLPF, BSTATE(Config.Sound_LPF));
#ifdef USE_ROMEO
			CheckDlgButton(hDlg, IDC_SOUNDROMEO, BSTATE(Config.SoundROMEO));
#endif	// USE_ROMEO

			CheckDlgButton(hDlg, IDC_SRAMWARNING, BSTATE(Config.SRAMWarning));

			CheckDlgButton(hDlg, IDC_DSMIXING, BSTATE(Config.DSMixing));

			CheckDlgButton(hDlg, IDC_LONGFILENAME, BSTATE(Config.LongFileName));
			CheckDlgButton(hDlg, IDC_WINDRVFD, BSTATE(Config.WinDrvFD));

			CheckDlgButton(hDlg, IDC_JOYSWAP, BSTATE(Config.JoySwap));

			CheckDlgButton(hDlg, IDC_SSTP, BSTATE(Config.SSTP_Enable));
			for (i=0; SSTPPorts[i]; i++) {
				wsprintf(buf, TEXT("%d"), SSTPPorts[i]);
				SendDlgItemMessage(hDlg, IDC_SSTPPORT, CB_ADDSTRING, 0, (LPARAM)buf);
			}
			wsprintf(buf, TEXT("%d"), Config.SSTP_Port);
			SendDlgItemMessage(hDlg, IDC_SSTPPORT, CB_SELECTSTRING, 0, (LPARAM)buf);
			EnableWindow(GetDlgItem(hDlg, IDC_SSTPPORT), Config.SSTP_Enable);
			EnableWindow(GetDlgItem(hDlg, IDC_SSTPPORT_TEXT), Config.SSTP_Enable);

			for (i=0; i<7; i++)
			{
				buf[0] = i+0x30;
				buf[1] = 0;
				SendDlgItemMessage(hDlg, IDC_CDROM_SCSIID, CB_ADDSTRING, 0, (LPARAM)buf);
			}
			buf[0] = Config.CDROM_SCSIID+0x30;
			SendDlgItemMessage(hDlg, IDC_CDROM_SCSIID, CB_SELECTSTRING, 0, (LPARAM)buf);
			CheckDlgButton(hDlg, IDC_CDROM_ASPI, BSTATE(Config.CDROM_ASPI));
			CheckDlgButton(hDlg, IDC_CDROM_IOCTRL, BSTATE(!Config.CDROM_ASPI));
			for (i=0; i<CDASPI_CDNum; i++)
				SendDlgItemMessage(hDlg, IDC_CDROM_ASPIID, CB_ADDSTRING, 0, (LPARAM)CDASPI_CDName[i]);
			SendDlgItemMessage(hDlg, IDC_CDROM_ASPIID, CB_SELECTSTRING, 0, (LPARAM)CDASPI_CDName[Config.CDROM_ASPI_Drive]);
			for (i=0; i<26; i++)
			{
				wsprintf(buf, TEXT("%c:"), i+0x41);
				SendDlgItemMessage(hDlg, IDC_CDROM_CTRLID, CB_ADDSTRING, 0, (LPARAM)buf);
			}
			wsprintf(buf, TEXT("%c:"), Config.CDROM_IOCTRL_Drive+0x41);
			SendDlgItemMessage(hDlg, IDC_CDROM_CTRLID, CB_SELECTSTRING, 0, (LPARAM)buf);

			EnableWindow(GetDlgItem(hDlg, IDC_SCSIID_TEXT), Config.CDROM_Enable);
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_SCSIID), Config.CDROM_Enable);
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_IOCTRL), Config.CDROM_Enable);
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPI), Config.CDROM_Enable);
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID), (Config.CDROM_Enable)&&(!Config.CDROM_ASPI));
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID_TEXT), (Config.CDROM_Enable)&&(!Config.CDROM_ASPI));
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID), (Config.CDROM_Enable)&&(Config.CDROM_ASPI));
			EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID_TEXT), (Config.CDROM_Enable)&&(Config.CDROM_ASPI));
			CheckDlgButton(hDlg, IDC_CDROM_ENABLE, BSTATE(Config.CDROM_Enable));

			for (i=0; i<16; i++)
			{
				wsprintf(buf, TEXT("%d"), i);
				SendDlgItemMessage(hDlg, IDC_HDDNO, CB_ADDSTRING, 0, (LPARAM)buf);
			}
			SendDlgItemMessage(hDlg, IDC_HDDNO, CB_SELECTSTRING, 0, (LPARAM)"0");
			SetDlgItemText(hDlg, IDC_HDDFILE, Config.HDImage[0]);
			CurrentHDDNo = 0;

			CheckDlgButton(hDlg, IDC_MIMPIMAP, BSTATE(Config.ToneMap));
			SetDlgItemText(hDlg, IDC_MIMPIFILE, Config.ToneMapFile);

#ifdef USE_ROMEO
			EnableWindow(GetDlgItem(hDlg, IDC_SOUNDROMEO), juliet_YM2151IsEnable());
			if ( Config.SoundROMEO ) {
				EnableWindow(GetDlgItem(hDlg, IDC_OPMVOL), 0);
				EnableWindow(GetDlgItem(hDlg, IDC_OPMVOL_TEXT), 0);
			}
#else	// USE_ROMEO
			EnableWindow(GetDlgItem(hDlg, IDC_SOUNDROMEO), FALSE);
			ShowWindow(GetDlgItem(hDlg, IDC_SOUNDROMEO), SW_HIDE);
#endif	// USE_ROMEO

			Setup_JoystickPage(hDlg);
		}

		ShowWindow(hDlg,SW_SHOW);
		return 1;

	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED)
		{
			wp = LOWORD(wParam);
			if ((wp>IDC_KEY_TOP)&&(wp<IDC_KEY_END))
			{
				Prop_KeyCodeSet(hDlg, (BYTE)(wp-IDC_KEY_TOP));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			}
			else switch (wp)
			{
			case IDC_KEY_RESET:
				memcpy(KeyTable, KeyTableMaster, sizeof(KeyTable));
				wsprintf(buf, TEXT("設定をクリアして初期設定に戻しました。"));
				SetDlgItemText(hDlg, IDC_KEYCONF_TEXT, buf);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_RATE_NON:
				Config.SampleRate=0; 
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE_TEXT), FALSE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_RATE_48:
				Config.SampleRate=48000;
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE_TEXT), TRUE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_RATE_44:
				Config.SampleRate=44100;
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE_TEXT), TRUE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_RATE_22:
				Config.SampleRate=22050;
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE_TEXT), TRUE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_RATE_11:
				Config.SampleRate=11025;
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_BUFSIZE_TEXT), TRUE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_STATUS:
				Config.WindowFDDStat=(!(Config.WindowFDDStat));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_FULLSCREENFDD:
				Config.FullScrFDDStat=(!(Config.FullScrFDDStat));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

#if 1
			case IDC_SOUNDLPF:
				Config.Sound_LPF=(!(Config.Sound_LPF));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
#endif

			case IDC_SOUNDROMEO:
#ifdef USE_ROMEO
				Config.SoundROMEO=(!(Config.SoundROMEO));
				EnableWindow(GetDlgItem(hDlg, IDC_OPMVOL), !Config.SoundROMEO);
				EnableWindow(GetDlgItem(hDlg, IDC_OPMVOL_TEXT), !Config.SoundROMEO);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
#endif	// USE_ROMEO
				return 1;

			case IDC_SRAMWARNING:
				Config.SRAMWarning=(!(Config.SRAMWarning));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_LONGFILENAME:
				Config.LongFileName=(!(Config.LongFileName));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_WINDRVFD:
				Config.WinDrvFD=(!(Config.WinDrvFD));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_SSTP:
				Config.SSTP_Enable=(!(Config.SSTP_Enable));
				EnableWindow(GetDlgItem(hDlg, IDC_SSTPPORT), Config.SSTP_Enable);
				EnableWindow(GetDlgItem(hDlg, IDC_SSTPPORT_TEXT), Config.SSTP_Enable);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_CDROM_ASPI:
				Config.CDROM_ASPI = 1;
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID_TEXT), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID_TEXT), TRUE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_CDROM_IOCTRL:
				Config.CDROM_ASPI = 0;
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID_TEXT), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID_TEXT), FALSE);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_CDROM_ENABLE:
				Config.CDROM_Enable = (!Config.CDROM_Enable);
				EnableWindow(GetDlgItem(hDlg, IDC_SCSIID_TEXT), Config.CDROM_Enable);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_SCSIID), Config.CDROM_Enable);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_IOCTRL), Config.CDROM_Enable);
				EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPI), Config.CDROM_Enable);
				if (CDROM_ASPIChecked)
				{
					EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID), (Config.CDROM_Enable)&&(!Config.CDROM_ASPI));
					EnableWindow(GetDlgItem(hDlg, IDC_CDROM_CTRLID_TEXT), (Config.CDROM_Enable)&&(!Config.CDROM_ASPI));
					EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID), (Config.CDROM_Enable)&&(Config.CDROM_ASPI));
					EnableWindow(GetDlgItem(hDlg, IDC_CDROM_ASPIID_TEXT), (Config.CDROM_Enable)&&(Config.CDROM_ASPI));
				}
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_MIDIDELAYAUTO:
				Config.MIDIAutoDelay = !Config.MIDIAutoDelay;
				if ( Config.MIDIAutoDelay ) {
					Config.MIDIDelay = Config.BufferSize*5;
				}
				EnableWindow(GetDlgItem(hDlg, IDC_MIDIDELAY), !Config.MIDIAutoDelay);
				CheckDlgButton(hDlg, IDC_MIDIDELAYAUTO, BSTATE(Config.MIDIAutoDelay));
				SendDlgItemMessage(hDlg, IDC_MIDIDELAY, TBM_SETPOS, TRUE, Config.MIDIDelay);
				Update_MIDIDelay(hDlg);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_MIDI:
				Config.MIDI_SW=(!(Config.MIDI_SW));
				Update_MIDIPage(hDlg);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_MIDIRESET:
				Config.MIDI_Reset=(!(Config.MIDI_Reset));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_MIMPIMAP:
				Config.ToneMap=(!(Config.ToneMap));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_JOYSWAP:
				Config.JoySwap=(!(Config.JoySwap));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_DSMIXING:
				Config.DSMixing=(!(Config.DSMixing));
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_MIMPIFILESEL:
				memset(&ofn, 0, sizeof(ofn));
				filename[0] = 0;
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = hDlg;
				ofn.lpstrFilter = TEXT("MIMPI Tone Mapping File (*.def)\0*.def\0")
							  TEXT("All Files (*.*)\0*.*\0");
				ofn.lpstrFile = filename;
				ofn.lpstrInitialDir = winx68k_dir;
				ofn.nMaxFile = MAX_PATH;
				ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_SHAREAWARE;
				ofn.lpstrDefExt = TEXT("hdf");
				ofn.lpstrTitle = TEXT("音色設定ファイルの選択");
	
				isopen = !!GetOpenFileName(&ofn);

				if (isopen)
				{
					lstrcpyn(Config.ToneMapFile, filename, MAX_PATH);
					SetDlgItemText(hDlg, IDC_MIMPIFILE, Config.ToneMapFile);
			                PropSheet_Changed(GetParent(hDlg), hDlg);
				}
				return 1;

			case IDC_HDDEJECT:
				memset(Config.HDImage[CurrentHDDNo], 0, sizeof(Config.HDImage[CurrentHDDNo]));
				SetDlgItemText(hDlg, IDC_HDDFILE, Config.HDImage[CurrentHDDNo]);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_HDDNEW:
				memset(&ofn, 0, sizeof(ofn));
				filename[0] = 0;
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = hDlg;
				ofn.lpstrFilter = TEXT("X68k HardDisk Image (*.hdf)\0*.hdf\0")
							  TEXT("All Files (*.*)\0*.*\0");
				ofn.lpstrFile = filename;
				ofn.lpstrInitialDir = winx68k_dir;
				ofn.nMaxFile = MAX_PATH;
				ofn.Flags = /*OFN_CREATEPROMPT | */OFN_HIDEREADONLY | OFN_SHAREAWARE;
				ofn.lpstrDefExt = TEXT("hdf");
				ofn.lpstrTitle = TEXT("新規HDイメージ作成");
	
				isopen = !!GetOpenFileName(&ofn);
				openflag = 0;

				if (isopen)
				{
					fp = File_Open(filename);
					if (!fp) {
						if (GetLastError()==ERROR_FILE_NOT_FOUND) {
							fp = File_Create(filename);
							if (fp)
							{
								MakeHDDFile = fp;
								MakeHDDSize = 0x2793; // 40Mb/0x1000
								DialogBox(hInst, MAKEINTRESOURCE(IDD_PROGBAR),
									hDlg, (DLGPROC)MakeHDDProc);
								openflag = 1;
								File_Close(fp);
							}
							else
							{
								Error("HDDイメージが作成できませんでした。");
							}
						}

					} else {
						Error("同名のファイルが既に存在します。");
						File_Close(fp);
					}
					if (openflag)
					{
						lstrcpyn(Config.HDImage[CurrentHDDNo], filename, MAX_PATH);
						SetDlgItemText(hDlg, IDC_HDDFILE, Config.HDImage[CurrentHDDNo]);
				                PropSheet_Changed(GetParent(hDlg), hDlg);
					}
				}
				return 1;

			case IDC_HDDSELECT:
				memset(&ofn, 0, sizeof(ofn));
				filename[0] = 0;
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = hDlg;
				ofn.lpstrFilter = TEXT("X68k HardDisk Image (*.hdf)\0*.hdf\0")
							  TEXT("All Files (*.*)\0*.*\0");
				ofn.lpstrFile = filename;
				ofn.lpstrInitialDir = winx68k_dir;
				ofn.nMaxFile = MAX_PATH;
				ofn.Flags = /*OFN_CREATEPROMPT | */OFN_HIDEREADONLY | OFN_SHAREAWARE;
				ofn.lpstrDefExt = TEXT("hdf");
				ofn.lpstrTitle = TEXT("X68k HDイメージの選択");
	
				isopen = !!GetOpenFileName(&ofn);
				openflag = 0;

				if (isopen)
				{
					fp = File_Open(filename);
					if (fp) {
						openflag = 1;
						File_Close(fp);
					}
					if (openflag)
					{
						lstrcpyn(Config.HDImage[CurrentHDDNo], filename, MAX_PATH);
						SetDlgItemText(hDlg, IDC_HDDFILE, Config.HDImage[CurrentHDDNo]);
				                PropSheet_Changed(GetParent(hDlg), hDlg);
					}
				}
				return 1;
			}
		} else {
			switch (LOWORD(wParam))
			{
			case IDC_SOUNDDEVICE:
				GetDlgItemText(hDlg, IDC_SOUNDDEVICE, Config.SoundDevice, _countof(Config.SoundDevice));
				PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_JOY1_BTN1:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN1, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][0] = temp-1;
				else Config.JOY_BTN[0][0] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN2:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN2, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][1] = temp-1;
				else Config.JOY_BTN[0][1] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN3:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN3, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][2] = temp-1;
				else Config.JOY_BTN[0][2] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN4:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN4, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][3] = temp-1;
				else Config.JOY_BTN[0][3] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN5:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN5, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][4] = temp-1;
				else Config.JOY_BTN[0][4] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN6:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN6, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][5] = temp-1;
				else Config.JOY_BTN[0][5] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN7:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN7, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][6] = temp-1;
				else Config.JOY_BTN[0][6] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY1_BTN8:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN8, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[0][7] = temp-1;
				else Config.JOY_BTN[0][7] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_JOY2_BTN1:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY2_BTN1, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][0] = temp-1;
				else Config.JOY_BTN[1][0] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN2:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY2_BTN2, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][1] = temp-1;
				else Config.JOY_BTN[1][1] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN3:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY2_BTN3, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][2] = temp-1;
				else Config.JOY_BTN[1][2] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN4:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY2_BTN4, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][3] = temp-1;
				else Config.JOY_BTN[1][3] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN5:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN5, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][4] = temp-1;
				else Config.JOY_BTN[1][4] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN6:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN6, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][5] = temp-1;
				else Config.JOY_BTN[1][5] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN7:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN7, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][6] = temp-1;
				else Config.JOY_BTN[1][6] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_JOY2_BTN8:
				temp = (int)SendDlgItemMessage(hDlg, IDC_JOY1_BTN8, CB_GETCURSEL, 0, 0);
				if (temp) Config.JOY_BTN[1][7] = temp-1;
				else Config.JOY_BTN[1][7] = MAX_BUTTON;
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_HDDNO:
				CurrentHDDNo = (int)SendDlgItemMessage(hDlg, IDC_HDDNO, CB_GETCURSEL, 0, 0);
				SetDlgItemText(hDlg, IDC_HDDFILE, Config.HDImage[CurrentHDDNo]);
				return 1;

			case IDC_HDDFILE:
				GetDlgItemText(hDlg, IDC_HDDFILE, Config.HDImage[CurrentHDDNo], MAX_PATH);
				return 1;

			case IDC_MIDIMODULE:
				GetDlgItemText(hDlg, IDC_MIDIMODULE, Config.MIDI_Module, _countof(Config.MIDI_Module));
				PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_MIDITYPE:
				Config.MIDI_Type = (int)SendDlgItemMessage(hDlg, IDC_MIDITYPE, CB_GETCURSEL, 0, 0);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_CDROM_SCSIID:
				Config.CDROM_SCSIID = (int)SendDlgItemMessage(hDlg, IDC_CDROM_SCSIID, CB_GETCURSEL, 0, 0);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_CDROM_ASPIID:
				Config.CDROM_ASPI_Drive = (int)SendDlgItemMessage(hDlg, IDC_CDROM_ASPIID, CB_GETCURSEL, 0, 0);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;

			case IDC_CDROM_CTRLID:
				Config.CDROM_IOCTRL_Drive = (int)SendDlgItemMessage(hDlg, IDC_CDROM_CTRLID, CB_GETCURSEL, 0, 0);
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			case IDC_SSTPPORT:
				temp = (int)SendDlgItemMessage(hDlg, IDC_SSTPPORT, CB_GETCURSEL, 0, 0);
				Config.SSTP_Port = SSTPPorts[temp];
		                PropSheet_Changed(GetParent(hDlg), hDlg);
				return 1;
			}
		}
		return 0;

	case WM_HSCROLL:
		switch (GetDlgCtrlID((HWND)lParam))
		{
		case IDC_BUFSIZE:
			Config.BufferSize = (DWORD)SendDlgItemMessage(hDlg, IDC_BUFSIZE, TBM_GETPOS, 0, 0);
			wsprintf(buf, TEXT("%d"), Config.BufferSize);
			SetDlgItemText(hDlg, IDC_BUFSIZE_TEXT, buf);
	                PropSheet_Changed(GetParent(hDlg), hDlg);
			return 1;

		case IDC_OPMVOL:
			Config.OPM_VOL = (int)SendDlgItemMessage(hDlg, IDC_OPMVOL, TBM_GETPOS, 0, 0);
			wsprintf(buf, TEXT("%d"), Config.OPM_VOL);
			SetDlgItemText(hDlg, IDC_OPMVOL_TEXT, buf);
	                PropSheet_Changed(GetParent(hDlg), hDlg);
			return 1;

		case IDC_PCMVOL:
			Config.PCM_VOL = (int)SendDlgItemMessage(hDlg, IDC_PCMVOL, TBM_GETPOS, 0, 0);
			wsprintf(buf, TEXT("%d"), Config.PCM_VOL);
			SetDlgItemText(hDlg, IDC_PCMVOL_TEXT, buf);
	                PropSheet_Changed(GetParent(hDlg), hDlg);
			return 1;

		case IDC_MCRVOL:
			Config.MCR_VOL = (int)SendDlgItemMessage(hDlg, IDC_MCRVOL, TBM_GETPOS, 0, 0);
			wsprintf(buf, TEXT("%d"), Config.MCR_VOL);
			SetDlgItemText(hDlg, IDC_MCRVOL_TEXT, buf);
	                PropSheet_Changed(GetParent(hDlg), hDlg);
			return 1;

		case IDC_MOUSESPEED:
			Config.MouseSpeed = (int)SendDlgItemMessage(hDlg, IDC_MOUSESPEED, TBM_GETPOS, 0, 0);
	                PropSheet_Changed(GetParent(hDlg), hDlg);
			return 1;

		case IDC_MIDIDELAY:
			Config.MIDIDelay = (int)SendDlgItemMessage(hDlg, IDC_MIDIDELAY, TBM_GETPOS, 0, 0);
			Update_MIDIDelay(hDlg);
	                PropSheet_Changed(GetParent(hDlg), hDlg);
			return 1;

		}
		return 0;

	case WM_SETCURSOR:
		id =  GetDlgCtrlID((HWND)wParam);

		if ( (id>IDC_KEY_TOP)&&(id<IDC_KEY_END) )
		{
			code = id-IDC_KEY_TOP;
			if (LastCode == code) break;
			LastCode = code;
			wsprintf(buf, TEXT("「%s」に割り当てられているキー：\n\n"), X68KeyName[code]);
			for (i=0, j=0; i<512; i++)
			{
				if ( (KeyTable[i]==code) )
				{
					j=1;
					keycode=(MapVirtualKey(i&255, 0)&255);
					keycode <<= 16;
					if (i&0x100) keycode |= 0x01000000;
					GetKeyNameText(keycode, keyname, 20);
					_tcscat(buf, TEXT("「"));
					if (!keyname[0])
						_tcscat(buf, TEXT("(名称不明)"));
					else
						_tcscat(buf, keyname);
					_tcscat(buf, TEXT("」"));
				}
			}
			if (!j) _tcscat(buf, TEXT("ありません。"));
			SetDlgItemText(hDlg, IDC_KEYCONF_TEXT, buf);
		}
		break;


	case WM_NOTIFY:
		id =   (UINT)(((NMHDR*) lParam)->idFrom);
		code = (((NMHDR*) lParam)->code);

		switch (code)
		{
		case PSN_SETACTIVE:
			break;

		case PSN_APPLY:
			memcpy(&ConfBk, &Config, sizeof(Win68Conf));
			memcpy(KeyTableBk, KeyTable, 512);
			PropSheet_UnChanged(GetParent(hDlg), hDlg);
			return PSNRET_NOERROR;
		
		case PSN_QUERYCANCEL:
			memcpy(&Config, &ConfBk, sizeof(Win68Conf));
			memcpy(KeyTable, KeyTableBk, 512);
			return FALSE;
		}
		return TRUE;

	}
	return 0;
}

// ---------------------------------------------------------------------------------------
//  ABOUT.C - ABOUTダイアログ
// ---------------------------------------------------------------------------------------
#include	"common.h"
#include	"resource.h"
#include	"version.h"

/**
 * ビットマップの透明色を変更する
 * @param[in] hdlg ダイアログ ハンドル
 * @param[in] id コントロール ID
 */
static void SetDlgBitmapTrans(HWND hdlg, UINT id)
{
	HBITMAP hBitmap = (HBITMAP)SendDlgItemMessage(hdlg, id, STM_GETIMAGE, IMAGE_BITMAP, 0L);
	if (hBitmap != NULL) {
		BITMAP bm;
		if (GetObject(hBitmap, sizeof(bm), &bm) == sizeof(bm)) {
			HDC hdc;
			int x, y;
			COLORREF bkColor;
			bkColor = GetSysColor(COLOR_3DFACE);
			hdc = CreateCompatibleDC(NULL);
			hBitmap = (HBITMAP)SelectObject(hdc, hBitmap);
			for (y = 0; y < bm.bmHeight; y++) {
				for (x = 0; x < bm.bmWidth; x++) {
					if (GetPixel(hdc, x, y) == 0x00c0c0c0) {
						SetPixel(hdc, x, y, bkColor);
					}
				}
			}
			SelectObject(hdc, hBitmap);
			DeleteDC(hdc);
		}
	}
}

LRESULT CALLBACK AboutDialogProc(HWND hdlg, UINT msg, WPARAM wp, LPARAM lp)
{
	TCHAR buf[4096];
	
	switch (msg)
	{
	case WM_INITDIALOG:
#ifndef WIN68DEBUG
		wsprintf(buf, TEXT("Keropi (WinX68k)\nSHARP X680x0 series emulator\n")
					  TEXT("Version ") TEXT(APP_VER_STRING) TEXT(" w/ SSTP1.0\n")
					  TEXT("Copyright (C) 2000-02 Kenjo\n")
					  TEXT("\nUsing \"FM Sound Generator\" (C) cisc"));
#else
		wsprintf(buf, TEXT("Keropi (Test Version)\nSHARP X680x0 series emulator\n")
					  TEXT("Version ") TEXT(APP_VER_STRING) TEXT(" w/ SSTP1.0\n")
					  TEXT("Copyright (C) 2000-02 Kenjo\n")
					  TEXT("\nUsing \"FM Sound Generator\" (C) cisc"));
#endif
		SetDlgItemText(hdlg, IDC_ABOUT_TEXT, buf);

		SetDlgBitmapTrans(hdlg, IDC_ABOUT_OPM);
		SetDlgBitmapTrans(hdlg, IDC_ABOUT_ADPCM);
		SetDlgBitmapTrans(hdlg, IDC_ABOUT_NEKO);

		SetFocus(GetDlgItem(hdlg, IDOK));

		return 0;

	case WM_COMMAND:
		if (IDOK == LOWORD(wp))
		{
			EndDialog(hdlg, TRUE);
			break;
		}
		return TRUE;

	case WM_CLOSE:
		EndDialog(hdlg, FALSE);
		return TRUE;

	default:
		return FALSE;
	}
	return FALSE;
}

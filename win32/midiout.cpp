/**
 * @file	midiout.cpp
 * @brief	MIDI OUT クラスの動作の定義を行います
 */

#include "common.h"
#include "midiout.h"
#include <vector>
#if defined(SUPPORT_VSTi)
#include "midiout/midioutvst.h"
#endif	// defined(SUPPORT_VSTi)
#include "midiout/midioutwin32.h"
#include "prop.h"
#include "winx68k.h"
#include "../misc/tstring.h"

#if defined(SUPPORT_VSTi)
static const TCHAR szVSTi[] = TEXT("Sound Canvas VA");
#endif	// defined(SUPPORT_VSTi)
static std::vector<std::tstring> s_modules;

void MidiOut_Init(void)
{
#if defined(SUPPORT_VSTi)
	CVstEditWnd::Initialize(hInst);
#endif	// defined(SUPPORT_VSTi)
}

LPCTSTR MidiOut_GetModuleName(unsigned int num)
{
	if (num == 0)
	{
		return szMidiMapper;
	}

	num--;
	if (num == 0)
	{
		s_modules.clear();

#if defined(SUPPORT_VSTi)
		if (MidiOutVst::IsEnabled())
		{
			s_modules.push_back(szVSTi);
		}
#endif	// defined(SUPPORT_VSTi)

		const UINT nNum = ::midiOutGetNumDevs();
		for (UINT i = 0; i < nNum; i++)
		{
			MIDIOUTCAPS moc;
			if (midiOutGetDevCaps(i, &moc, sizeof(moc)) != MMSYSERR_NOERROR)
			{
				continue;
			}
			s_modules.push_back(moc.szPname);
		}
	}
	if (num < s_modules.size())
	{
		return s_modules[num].c_str();
	}
	return NULL;
}

MIDIOUTHDL MidiOut_Create(void)
{
#if defined(SUPPORT_VSTi)
	if (lstrcmp(Config.MIDI_Module, szVSTi) == 0)
	{
		return MidiOutVst::CreateInstance();
	}
#endif	// defined(SUPPORT_VSTi)
	return MidiOutWin32::CreateInstance(Config.MIDI_Module);
}

void MidiOut_Destroy(MIDIOUTHDL hdl)
{
	delete hdl;
}

void MidiOut_SendShort(MIDIOUTHDL hdl, uint32_t msg)
{
	if (hdl)
	{
		hdl->Short(msg);
	}
}

void MidiOut_SendLong(MIDIOUTHDL hdl, const uint8_t *excv, uint32_t length)
{
	if (hdl)
	{
		hdl->Long(excv, length);
	}
}

void MidiOut_Process16(MIDIOUTHDL hdl, int16_t* buffer, uint32_t length)
{
	if (hdl)
	{
		hdl->Process16(buffer, length);
	}
}

void MidiOut_Update(void)
{
#if defined(SUPPORT_VSTi)
	CVstEditWnd::OnIdle();
#endif	// defined(SUPPORT_VSTi)
}

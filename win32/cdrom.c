// ---------------------------------------------------------------------------------------
//  SCSI CD-ROM Drive Emulation
//    ……とゆ〜か、SCSI IOCSのフックルーチンです ^^;
//  ToDo:
//    ……バグだらけの修正（汗。あと、オーディオ再生系の実装も。
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "winx68k.h"
#include "prop.h"
#include "cdrom.h"
#include "cdctrl.h"
#include "cdaspi.h"
#include "../x68k/m68000.h"
#include "../x68k/memory.h"

static	TOC	toc, tocbk;

static	int	CDROM_Opened = 0;
static	BYTE	CDROM_Buf[4096];
static	BYTE	SenseStatus = 6;
static	int	Phase = 0xff;
static	int	SelID = 0;

static	DWORD	MCIDEVID = 0;

BYTE	CDROM_SCSIID = 6;
BYTE	CDROM_EnableFlag = 0;
BYTE	CDROM_ASPIChecked = 0;

#define SCSIIOCS_ID		(BYTE)(M68000_GETD(4)&0xff)
#define SCSIIOCS_BUF		M68000_GETA(1)
#define SCSIIOCS_BUFSIZE	(int)(M68000_GETD(3)&0xff)
#define SCSIIOCS_CMDSIZE	(int)M68000_GETD(3)
#define SCSIIOCS_RET(v)		M68000_SETD(0, v)

//#define CDROM_DEBUG


//------------------------------------------------------
// MCIによるCD再生/停止
//   IOCTRL用（cdctrl.cに移動すべき？）
//------------------------------------------------------
void CDROM_MCIPlayCD(DWORD st, DWORD ed)
{
	MCI_OPEN_PARMS mciOpen;
	MCI_SET_PARMS  mciSet;
	MCI_PLAY_PARMS mciPlay;
	TCHAR szElementName[4];
	TCHAR szAliasName[32];
	DWORD dwFlags;
	DWORD dwAliasCount = GetCurrentTime();
	DWORD dwRet;
 
	if ( !MCIDEVID ) {
		ZeroMemory(&mciOpen, sizeof(mciOpen));
		mciOpen.lpstrDeviceType = (LPTSTR)MCI_DEVTYPE_CD_AUDIO;
		wsprintf( szElementName, TEXT("%c:"), 0x41+Config.CDROM_IOCTRL_Drive);
		wsprintf( szAliasName, TEXT("CD%lu:"), dwAliasCount );
		mciOpen.lpstrElementName = szElementName;
		mciOpen.lpstrAlias = szAliasName;
		dwFlags = MCI_OPEN_ELEMENT | MCI_OPEN_SHAREABLE | MCI_OPEN_ALIAS | MCI_OPEN_TYPE | MCI_OPEN_TYPE_ID | MCI_WAIT;
		dwRet = mciSendCommand(0, MCI_OPEN, dwFlags, (DWORD_PTR)&mciOpen);
		if ( dwRet == MMSYSERR_NOERROR ) {
			MCIDEVID = mciOpen.wDeviceID;
		}
		if ( MCIDEVID ) {
			dwFlags = MCI_SET_AUDIO | MCI_SET_ON | MCI_SET_TIME_FORMAT | MCI_WAIT;
			mciSet.dwCallback = 0;
			mciSet.dwTimeFormat = MCI_FORMAT_MSF;
			mciSet.dwAudio = MCI_SET_AUDIO_ALL;
			dwRet = mciSendCommand(MCIDEVID, MCI_SET, dwFlags, (DWORD_PTR)&mciSet);
		}
	}
	dwFlags = MCI_FROM | MCI_TO | MCI_NOTIFY;
	mciPlay.dwCallback = 0;
	mciPlay.dwFrom = st;
	mciPlay.dwTo = ed;
	mciSendCommand(MCIDEVID, MCI_PLAY, dwFlags, (DWORD_PTR)&mciPlay);
}


void CDROM_MCIStopCD(void)
{
	MCI_GENERIC_PARMS  mciParam;
	mciParam.dwCallback = 0;
	mciSendCommand(MCIDEVID, MCI_STOP, MCI_NOTIFY, (DWORD_PTR)&mciParam);
}


//------------------------------------------------------
// TOC比較（CD-ROM入れ替えチェック）
//   前回から変化があればTRUEを返す
//------------------------------------------------------
int CDROM_CompareTOC(void)
{
	ZeroMemory(&toc, sizeof(toc));
	if (Config.CDROM_ASPI)			// ASPI設定の時
	{
		if (!CDASPI_CDNum) return TRUE;	// ASPIのCDドライブが見つからない
		if (CDASPI_Open())
		{
			CDASPI_ReadTOC(&toc);
			CDASPI_Close();
			if ( memcmp(&toc, &tocbk, sizeof(toc)) )	// TOCが変わってる？
			{
				memcpy(&tocbk, &toc, sizeof(toc));	// 新しいTOCをコピー
				return TRUE;
			}
			return FALSE;
		}
		return TRUE;
	}
	else					// IOCTRL設定の時
	{
		if (CDCTRL_Open())
		{
			CDCTRL_ReadTOC(&toc);
			CDCTRL_Close();
			if ( memcmp(&toc, &tocbk, sizeof(toc)) )	// TOCが変わってる？
			{
				memcpy(&tocbk, &toc, sizeof(toc));	// 新しいTOCをコピー
				return TRUE;
			}
			return FALSE;
		}
		return TRUE;
	}
}


//------------------------------------------------------
// TOCりーど
//------------------------------------------------------
int CDROM_ReadTOC(void* buf)
{
	if (Config.CDROM_ASPI)			// ASPI設定の時
	{
		if (!CDASPI_CDNum) return FALSE;	// ASPIのCDドライブが見つからない
		if (CDASPI_Open())
		{
			CDASPI_ReadTOC(buf);
			CDASPI_Close();
			return TRUE;
		}
		else
			return FALSE;
	}
	else					// IOCTRL設定の時
	{
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "------------------ TOC Read by I/O Control\n");
fclose(fp);
}
#endif
		if (CDCTRL_Open())
		{
			CDCTRL_ReadTOC(buf);
			CDCTRL_Close();
			return TRUE;
		}
		else
			return FALSE;
	}
}


//------------------------------------------------------
// とらっくりーど
//------------------------------------------------------
int CDROM_ReadTrack(long pos, int trknum, int size, DWORD adr)
{
	int trk, i;

	if (Config.CDROM_ASPI)			// ASPI設定の時
	{
		if (!CDASPI_CDNum) return FALSE;	// ASPIのCDドライブが見つからない
		if (CDASPI_Open())
		{
			for (trk=0; trk<trknum; trk++)
			{
				CDASPI_Read(pos+trk, CDROM_Buf);
				for (i=0; i<size; i++)
				{
					Memory_WriteB(adr+i, CDROM_Buf[i]);
				}
				adr += size;
			}
			CDASPI_Close();
			return TRUE;
		}
		else
			return FALSE;
	}
	else					// IOCTRL設定の時
	{
		if (CDCTRL_Open())
		{
			for (trk=0; trk<trknum; trk++)
			{
				CDCTRL_Read(pos+trk, CDROM_Buf);
				for (i=0; i<size; i++)
				{
					Memory_WriteB(adr+i, CDROM_Buf[i]);
				}
				adr += size;
			}
			CDCTRL_Close();
			return TRUE;
		}
		else
			return FALSE;
	}
}


//------------------------------------------------------
// SCSI Command エミュレーション
//    SCSI IOCS $03 コマンド内容を調べて、適切な応答をしましょう…
//------------------------------------------------------
static void CDROM_SCSICmd(void)
{
	int i, trk, pos;
	BYTE cdb[32];
	DWORD st, ed;

	ZeroMemory(cdb, sizeof(cdb));
/*{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "Cmd:$%02X\n", Memory_ReadB(SCSIIOCS_BUF));
fclose(fp);
}*/
	switch(Memory_ReadB(SCSIIOCS_BUF))
	{
	case 0x43:		// TOC取得コマンド
		ZeroMemory(CDROM_Buf, sizeof(CDROM_Buf));		// Buffer Clear
		if (CDROM_ReadTOC(&toc))				// TOC取得
		{
			memcpy(CDROM_Buf, &toc, 4);			// Size/Start/Lastをコピー
			trk = Memory_ReadB(SCSIIOCS_BUF+6);
			for (i=0; i<100; i++)
			{
				if (toc.track[i].trackno==trk)		// 対象トラックのステータスをコピー
				{					// Track=0xAA はトラックの最後
					if (Memory_ReadB(SCSIIOCS_BUF+1)&2)			// MSF
					{
						memcpy(&CDROM_Buf[4], &toc.track[i], 8);
					}
					else							// Logical Address
					{
						memcpy(&CDROM_Buf[4], &toc.track[i], 4);
						pos = ( ( (toc.track[i].addr[1])*60 + (toc.track[i].addr[2]) -2 )*75 + (toc.track[i].addr[3]) );	// MSF -> Block No
						CDROM_Buf[ 8] = (BYTE)((pos>>24)&0xff);
						CDROM_Buf[ 9] = (BYTE)((pos>>16)&0xff);
						CDROM_Buf[10] = (BYTE)((pos>>8 )&0xff);
						CDROM_Buf[11] = (BYTE)((pos    )&0xff);
						// ほんとはこの後に残りトラックの情報も書いておかなきゃいけないんだけど…
						// 取敢えず指定の先頭トラック情報だけでOKかな？
					}
					break;
				}
			}
			if (i==100) SCSIIOCS_RET(-1);
		}
		else
			SCSIIOCS_RET(-1);
		break;

	// ASPIのみ対応。何故なら垂れ流しでOKだから :-p
	case 0x42:		// Read SubChannel
	case 0x45:		// オーディオ再生（10）
	case 0x47:		// オーディオ再生（MSF）
	case 0x48:		// オーディオ再生（TrackRelative10）
	case 0x49:		// オーディオ再生（Index）
	case 0x4b:		// オーディオのポーズ
	case 0xA5:		// オーディオ再生（12）
	case 0xA9:		// オーディオ再生（TrackRelative12）
		if (Config.CDROM_ASPI)			// ASPI設定の時
		{
			if (!CDASPI_CDNum) {
				SCSIIOCS_RET(-1);
				break;
			}
			if (CDASPI_Open())
			{
				for (i=0; i<SCSIIOCS_CMDSIZE; i++) {
					cdb[i] = Memory_ReadB(SCSIIOCS_BUF+i);
				}
				CDASPI_ExecCmd(cdb, SCSIIOCS_CMDSIZE, CDROM_Buf, sizeof(CDROM_Buf));
				CDASPI_Close();
			}
		} else {
			switch (Memory_ReadB(SCSIIOCS_BUF))
			{
				case 0x42:		// Read SubChannel
					break;
				case 0x45:		// オーディオ再生（10）
					break;
				case 0x47:		// オーディオ再生（MSF）
					st = MCI_MAKE_MSF( Memory_ReadB(SCSIIOCS_BUF+3),
							   Memory_ReadB(SCSIIOCS_BUF+4),
							   Memory_ReadB(SCSIIOCS_BUF+5) );
					ed = MCI_MAKE_MSF( Memory_ReadB(SCSIIOCS_BUF+6),
							   Memory_ReadB(SCSIIOCS_BUF+7),
							   Memory_ReadB(SCSIIOCS_BUF+8) );
					CDROM_MCIPlayCD(st, ed);
					break;
				case 0x48:		// オーディオ再生（TrackRelative10）
					break;
				case 0x49:		// オーディオ再生（Index）
					break;
				case 0x4b:		// オーディオのポーズ
					CDROM_MCIStopCD();
					break;
				case 0xA5:		// オーディオ再生（12）
					break;
				case 0xA9:		// オーディオ再生（TrackRelative12）
					break;
			}
		}
		break;

	default:
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "     ************* COMMAND Unknown\n");
fclose(fp);
}
#endif
		break;
	}
}



//------------------------------------------------------
// SCSI IOCS エミュレーション
//    IOCS番号に合わせてレジスタ／バッファ内容を書き換えて、帰ります
//------------------------------------------------------

static byte InquiryStr[] = "KEROPI  VIRTUAL CD-ROM  1.1000-20010319";	// Inquiry で返す Vendor(8byte) / ProductName(16byte) / Version(>4byte)

void CDROM_Command(BYTE cmd)
{
	BYTE tmp[256];
	int i;
	long blocks;

#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "SCSI IOCS $%02X  ID:$%02X  @ $%08X\n", cmd, SelID, M68000_GETPC);
fclose(fp);
}
#endif

	if ( !CDROM_EnableFlag )		// CDROM OFF
	{
		SCSIIOCS_RET(-1);
		return;
	}

#if 0
	if ( SCSIIOCS_ID==CDROM_SCSIID )	// 予めCDROM以外のID時はエラーにしておく
		SCSIIOCS_RET(0);
	else
		SCSIIOCS_RET(-1);
#endif

	switch(cmd)
	{
	case 0x00:		// _S_RESET
		SCSIIOCS_RET(0);		// ほんとはD0は不定
		Phase = 0xff;
		break;

	case 0x01:		// _S_SELECT
		SelID = SCSIIOCS_ID;
		if ( SelID==CDROM_SCSIID ) {
			SCSIIOCS_RET(0);
			Phase = 0xd7;		// ATN/BSY
		}
		else
			SCSIIOCS_RET(-1);
		break;

	case 0x03:		// _S_CMDOUT
				// ここでの実行結果は CDROM_Buf[] に入り、次のS_DATAINで読み込まれる
		if ( SelID==CDROM_SCSIID ) {
			SCSIIOCS_RET(0);
			Phase = 0xf0;		// MSG/IO/CD/BSY
		}
		else
			SCSIIOCS_RET(-1);
		CDROM_SCSICmd();
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "ID %d - COMMAND OUT (%dbytes) : ", SelID, SCSIIOCS_CMDSIZE);
for (i=0; i<SCSIIOCS_CMDSIZE; i++)
	fprintf(fp, "%02X ", Memory_ReadB(SCSIIOCS_BUF+i));
fprintf(fp, "\n");
fclose(fp);
}
#endif
		break;

	case 0x04:		// _S_DATAIN
	case 0x0b:		// _S_DATAINI （転送がDMAかMPUかの違いなので、同じ処理で良い）
		if ( SelID==CDROM_SCSIID ) {
			SCSIIOCS_RET(0);
			Phase = 0xff;
		}
		else
			SCSIIOCS_RET(-1);
		for (i=0; i<SCSIIOCS_CMDSIZE; i++)
			Memory_WriteB(SCSIIOCS_BUF+i, CDROM_Buf[i]);
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "DATA IN (%dbytes) : ", SCSIIOCS_CMDSIZE);
for (i=0; i<SCSIIOCS_CMDSIZE; i++)
	fprintf(fp, "%02X ", Memory_ReadB(SCSIIOCS_BUF+i));
fprintf(fp, "\n");
fclose(fp);
}
#endif
		break;

	case 0x06:		// _S_STSIN
		if ( SelID==CDROM_SCSIID )
			SCSIIOCS_RET(0);
		else
			SCSIIOCS_RET(-1);
		if (CDROM_CompareTOC())
		{
			Memory_WriteB(SCSIIOCS_BUF, 2);		// Status = 2 (Check Condition)
			SenseStatus = 6;			// Unit Attention
		}
		else
			Memory_WriteB(SCSIIOCS_BUF, 0);		// Status = 0 (Good)
		break;

	case 0x07:		// _S_MSGIN
		if ( SelID==CDROM_SCSIID )
			SCSIIOCS_RET(0);
		else
			SCSIIOCS_RET(-1);
//		Memory_WriteB(SCSIIOCS_BUF, 0xc0);	// Identify Mes (DisConn=OK, LUN=0)
		Memory_WriteB(SCSIIOCS_BUF, 0x00);	// Command Complete
		break;

	case 0x09:		// _S_PHASE
		SCSIIOCS_RET(Phase);
		break;

	case 0x0a:		// _S_LEVEL
		SCSIIOCS_RET(3);		// 取敢えずIOCS Ver.は6BS1のってことにしとこう…
		break;

	case 0x1e:		// TWOSCSI（TNB製作所）チェック
		SCSIIOCS_RET(-1);		// Ret=-2 で TWOSCSI
		break;

	case 0x20:		// _S_INQUIRY
		ZeroMemory(tmp, 256);
		if (SCSIIOCS_ID == CDROM_SCSIID)
		{
			tmp[0] = 0x05;		// DevType = Read Only Direct Dev.
			tmp[1] = 0x80;		// DevTypeQual = 0, Removable
			tmp[2] = 0x02;		// ISO Ver = 0, ECMA Ver = 0, ANSI = SCSI2
			tmp[3] = 0x02;		// Reserved
			tmp[4] = 0x1f;		// Additional Data Length
			tmp[5] = 0x00;		//
			tmp[6] = 0x00;		//
			tmp[7] = 0x18;		//
			memcpy(&tmp[8], InquiryStr, sizeof(InquiryStr));
		}
		else
		{
			tmp[0] = 0x7f;		// DevType = No Unit
			tmp[1] = 0x00;		// DevTypeQual = 0
		}
		for (i=0; i<SCSIIOCS_BUFSIZE; i++)
			Memory_WriteB(SCSIIOCS_BUF+i, tmp[i]);
		SCSIIOCS_RET(0);
		break;

	case 0x22:		// _S_WRITE
		break;

	case 0x23:		// _S_FORMAT
		break;

	case 0x24:		// _S_TESTUNIT
		if ( SCSIIOCS_ID==CDROM_SCSIID )
			SCSIIOCS_RET(0);
		else
			SCSIIOCS_RET(-1);
		break;

	case 0x25:		// _S_READCAP
		if ( SCSIIOCS_ID!=CDROM_SCSIID ) {
			SCSIIOCS_RET(-1);
			break;
		}
		SCSIIOCS_RET(0);
		if (CDROM_ReadTOC(&toc))				// TOC取得
		{
			for (i=0; i<100; i++)
			{
				if (toc.track[i].trackno==0xaa)		// TOCから最終トラック情報を取得
				{
					blocks = ( ( (toc.track[i].addr[1])*60 + (toc.track[i].addr[2]) -2 )*75 + (toc.track[i].addr[3]) );	// MSF -> Block No
					Memory_WriteD(SCSIIOCS_BUF  , (DWORD)(blocks-1));	// CDの総容量（Audioトラック含むブロック数）
					Memory_WriteD(SCSIIOCS_BUF+4, 0x800);			// ブロックサイズ（大抵2048）
					break;
				}
			}
			if (i==100) SCSIIOCS_RET(-1);
		}
		else
			SCSIIOCS_RET(-1);
		break;

	case 0x21:		// _S_READ
	case 0x26:		// _S_READEXT
		if ( SCSIIOCS_ID!=CDROM_SCSIID ) {
			SCSIIOCS_RET(-1);
			break;
		}
		if (!CDROM_ReadTrack(M68000_GETD(2), SCSIIOCS_CMDSIZE, 256*(1<<(M68000_GETD(5)&0xff)), SCSIIOCS_BUF))
		{
			SCSIIOCS_RET(-1);
			break;
		}
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "拡張READ  Pos:$%08X  Num:$%08X  Size:$%08X  @ $%08X\n", M68000_GETD(2), M68000_GETD(3), M68000_GETD(5), M68000_GETPC);
fclose(fp);
}
#endif
		SCSIIOCS_RET(0);
		break;

	case 0x27:		// _S_WRITEEXT
		break;

	case 0x28:		// _S_VERIFYEXT
		break;

	case 0x29:		// _S_MODESENSE
		if ( SCSIIOCS_ID!=CDROM_SCSIID ) {
			SCSIIOCS_RET(-1);
			break;
		}
		ZeroMemory(tmp, 256);
		tmp[0] = 0x0c;		// Length
		tmp[1] = 0x00;		// Media Type
		tmp[2] = 0x80;		// Write Protect(bit7)
		tmp[3] = 0x08;		// Block Desc. Length (8byte order)
		tmp[4] = 0x00;		// Density Code
		tmp[5] = 0x00;		// Blocks (High)
		tmp[6] = 0x00;		// Blocks (Mid)
		tmp[7] = 0x00;		// Blocks (Low)
		tmp[8] = 0x00;		// Reseved
		tmp[9] = 0x00;		// Block Length (High)
		tmp[10] = 0x08;		// Block Length (Mid)
		tmp[11] = 0x00;		// Block Length (Low)
		for (i=0; i<SCSIIOCS_BUFSIZE; i++)
			Memory_WriteB(SCSIIOCS_BUF+i, tmp[i]);
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "*** ModeSense  Size:$%08X  @ $%08X\n", SCSIIOCS_BUFSIZE, M68000_GETPC);
fclose(fp);
}
#endif
		SCSIIOCS_RET(0);
		break;

	case 0x2a:		// _S_MODESELECT
		if ( SCSIIOCS_ID==CDROM_SCSIID )
			SCSIIOCS_RET(0);
		else
			SCSIIOCS_RET(-1);
		break;

	case 0x2b:		// _S_REZEROUNIT (ユニット初期状態)
		if ( SCSIIOCS_ID==CDROM_SCSIID )
			SCSIIOCS_RET(0);
		else
			SCSIIOCS_RET(-1);
		break;

	case 0x2c:		// _S_REQUEST
		tmp[0] = 0x70;		// 拡張センスデータ
		tmp[1] = 0x00;		// Segment No.
		tmp[2] = SenseStatus;	// Sense Key
		tmp[3] = 0x00;		// Information Bytes
		tmp[4] = 0x00;		//
		tmp[5] = 0x00;		//
		tmp[6] = 0x00;		//
		tmp[7] = 0x00;		//
		for (i=0; i<SCSIIOCS_BUFSIZE; i++)
			Memory_WriteB(SCSIIOCS_BUF+i, tmp[i]);
		SCSIIOCS_RET(0);
		break;

	case 0x2d:		// _S_SEEK
		break;

	case 0x2e:		// _S_READI
		break;

	case 0x2f:		// _S_STARTSTOP
		break;

	default:
		SCSIIOCS_RET(-1);
		break;
	}
#ifdef CDROM_DEBUG
{
FILE *fp;
fp=fopen("_scsi.txt", "a");
fprintf(fp, "   - Ret:$%02X\n", M68000_GETD(0));
fclose(fp);
}
#endif
}


//------------------------------------------------------
// I/O部（$e9f800〜$e9ffff）
//------------------------------------------------------

			// 判別用ID。いらないけどね（笑）。
			// $e9f800〜の5バイトを読むと文字列「CDROM」が返ります
BYTE FASTCALL CDROM_Read(DWORD adr)
{
	static BYTE ID[] = "CDROM";
	if ((adr&0x7ff)<5)
		return ID[adr&0x7ff];
	else
		return 0;
}

			// SCSI IOCSが起こると、dataにSCSI IOCS番号が入った状態で
			// ここが呼ばれます → 参照：scsi.c / windrv.c
void FASTCALL CDROM_Write(DWORD adr, BYTE data)
{
	if (adr>=0xe9f800) CDROM_Command(data);
}

void CDROM_Init()
{
	if (Config.CDROM_Enable)
	{
		ASPIDLL = LoadLibrary(TEXT("WNASPI32.DLL"));
		if ( ASPIDLL ) {
			*(FARPROC *)&pSendASPI32Command = GetProcAddress(ASPIDLL, "SendASPI32Command");
			*(FARPROC *)&pGetASPI32SupportInfo = GetProcAddress(ASPIDLL, "GetASPI32SupportInfo");
			CDASPI_EnumCD();
			CDROM_ASPIChecked = 1;
		}
	}

	memset(&toc, 0, sizeof(toc));
	memset(&tocbk, 0xff, sizeof(tocbk));
	SenseStatus = 6;			// Unit Attention

	CDROM_SCSIID = Config.CDROM_SCSIID;
	CDROM_EnableFlag = Config.CDROM_Enable;

	MCIDEVID = 0;
}


void CDROM_Cleanup()
{
	MCI_GENERIC_PARMS  mciParam;
	BYTE cmd[10] = {0x4b, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	mciParam.dwCallback = 0;
	if (Config.CDROM_ASPI)			// ASPI設定の時
	{
		if (CDASPI_CDNum) {
			if (CDASPI_Open()) {
				CDASPI_ExecCmd(cmd, 10, CDROM_Buf, sizeof(CDROM_Buf));
				CDASPI_Close();
			}
		}
		if ( ASPIDLL ) {
			FreeLibrary(ASPIDLL);
		}
	}
	if (MCIDEVID) {
		CDROM_MCIStopCD();
		mciSendCommand(MCIDEVID, MCI_CLOSE, MCI_NOTIFY, (DWORD_PTR)&mciParam);
		MCIDEVID = 0;
	}
}

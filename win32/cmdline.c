// -----------------------------------------------------------------------
//   こまんどらいん処理なの。てきとーなの（ぉぃ。
// -----------------------------------------------------------------------
//
// えっとね、ここ見てる人はあんまりいないだろうけど、ここで説明しておくの（ぉ。
//
// ディスク挿入起動は、「Aファイル名」「A:ファイル名」「A=ファイル名」が通ります。
// ドライブ名は A/B もしくは 0/1 が通ります。また、ドライブ名の前に「-」か「/」が
// あっても可。
//
// 「NOCD」を記述する事で、ASPIによるCDチェックを飛ばしての起動が可能。この状態で
// 一回終了すれば、CD無し設定がINIに書き込まれるので、以降はこのオプションはいら
// ないはず。これも「-」「/」が前についててもいいです。


#include "common.h"
#include "winui.h"
#include "prop.h"
#include "../x68k/fdd.h"

#include <string.h>

static TCHAR fd0[MAX_PATH];
static TCHAR fd1[MAX_PATH];
TCHAR macrofile[MAX_PATH];

void SetCmdLineFD(void) {
	if ( fd0[0] ) FDD_SetFD(0, fd0, 0);
	if ( fd1[0] ) FDD_SetFD(1, fd1, 0);
}


void CheckCmdLineSub(LPTSTR s)
{
	if ( (*s=='-')||(*s=='/') ) s++;
	if ( (*s=='a')||(*s=='A')||(*s=='0') ) {
		s++;
		if ( (*s=='=')||(*s==':') ) s++;
		lstrcpy(fd0, s);
	} else if ( (*s=='b')||(*s=='B')||(*s=='1') ) {
		s++;
		if ( (*s=='=')||(*s==':') ) s++;
		lstrcpy(fd1, s);
	} else if ( (*s=='m')||(*s=='M') ) {
		s++;
		if ( (*s=='=')||(*s==':') ) s++;
		lstrcpy(macrofile, s);
	} else {
		_tcsupr(s);
		if ( !_tcscmp(s, TEXT("NOCD")) ) {
			Config.CDROM_Enable = 0;
		}
	}
}


void CheckCmdLine(LPCTSTR cmd)
{
	int len;
	TCHAR buf[256];
	LPTSTR p;
	LPCTSTR s = cmd;

	ZeroMemory(macrofile, sizeof(macrofile));
	len = (int)_tcslen(s);
	while( (*s)&&((s-cmd)<len) ) {
		while( *s==0x20 ) s++;
		p = buf;
		while( (*s!=0x20)&&(*s) ) {
			*p++ = *s++;
		}
		*p = 0;
		if ( p!=buf ) CheckCmdLineSub(buf);
	}
}

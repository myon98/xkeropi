/**
 * @file	sddsound3.h
 * @brief	DSound3 オーディオ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <dsound.h>
#include <vector>
#include "sounddevice.h"
#include "../misc/win32/thread.h"

/**
 * @brief デバイス
 */
struct DSound3Device
{
	GUID guid;							//!< GUID
	TCHAR szDevice[MAX_PATH];			//!< デバイス
};

/**
 * @brief Direct Sound3 クラス
 */
class SoundDeviceDSound3 : public ISoundDevice, protected CThread
{
public:
	static void Initialize();
	static std::vector<LPCTSTR> EnumerateDevices();

	SoundDeviceDSound3();
	virtual ~SoundDeviceDSound3();
	virtual bool Open(LPCTSTR lpDevice = NULL, HWND hWnd = NULL);
	virtual void Close();
	virtual UINT CreateStream(UINT nSamplingRate, UINT nChannels, UINT nBufferSize = 0);
	virtual void DestroyStream();
	virtual void ResetStream();
	virtual bool PlayStream();
	virtual void StopStream();

protected:
	virtual void Task();

private:
	static std::vector<DSound3Device> sm_devices;	//!< デバイス リスト

	LPDIRECTSOUND m_lpDSound;					//!< Direct Sound インタフェイス
	LPDIRECTSOUNDBUFFER m_lpDSStream;			//!< ストリーム バッファ
	UINT m_nChannels;							//!< チャネル数
	UINT m_nBufferSize;							//!< バッファ サイズ
	UINT m_dwHalfBufferSize;					//!< バッファ バイト
	HANDLE m_hEvents[2];						//!< イベント

	static BOOL CALLBACK EnumCallback(LPGUID lpGuid, LPCTSTR lpcstrDescription, LPCTSTR lpcstrModule, LPVOID lpContext);
	void FillStream(DWORD dwPosition);
};

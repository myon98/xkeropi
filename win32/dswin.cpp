// ---------------------------------------------------------------------------
// 　OPMDRV2... DSOUND3.CPP
// ---------------------------------------------------------------------------

#include "common.h"
#include "dswin.h"
#include "prop.h"
#include "sounddevice/sounddevicedsound3.h"
#include "winx68k.h"
#include "../x68k/adpcm.h"
#include "../x68k/midi.h"
#include "../x68k/mercury.h"
#include "../fmgen/fmg_wrap.h"
#include "../misc/tstring.h"
#include "../misc/win32/guard.h"
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
#include "sounddevice/sounddeviceasio.h"
#include "sounddevice/sounddevicewasapi.h"
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

static const TCHAR szSeparator[] = TEXT(" - ");

/**
 * @brief サウンド クラス
 */
class DSound : public ISoundData
{
private:
	ISoundDevice* m_pDevice;
	UINT m_nRate;
	UINT DSound_PreCounter;
	int16_t* m_pBuffer;
	UINT m_nBufferIndex;
	UINT m_nBufferSize;
	CGuard m_cs;
	std::vector<std::tstring> m_devices;

public:
	/**
	 * コンストラクタ
	 */
	DSound()
		: m_pDevice(NULL)
		, m_nRate(0)
		, DSound_PreCounter(0)
		, m_pBuffer(NULL)
		, m_nBufferIndex(0)
		, m_nBufferSize(0)
	{
	}

	/**
	 * デストラクタ
	 */
	virtual ~DSound()
	{
	}

	/**
	 * デバイス名を得る
	 */
	LPCTSTR GetDeviceName(UINT nNum)
	{
		if (nNum == 0)
		{
			static const TCHAR szDefault[] = TEXT("Default");
			return szDefault;
		}

		nNum--;
		if (m_devices.empty())
		{
			Add(TEXT("DSound"), SoundDeviceDSound3::EnumerateDevices());
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
			Add(TEXT("ASIO"), SoundDeviceAsio::EnumerateDevices());
			Add(TEXT("WASAPI"), SoundDeviceWasapi::EnumerateDevices());
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)
		}

		if (nNum < m_devices.size())
		{
			return m_devices[nNum].c_str();
		}
		return NULL;
	}

	/**
	 * リストに追加
	 */
	void Add(LPCTSTR lpType, const std::vector<LPCTSTR>& devices)
	{
		for (std::vector<LPCTSTR>::const_iterator it = devices.begin(); it != devices.end(); ++it)
		{
			m_devices.push_back(std::tstring(lpType) + std::tstring(szSeparator) + std::tstring(*it));
		}
	}

	static bool Starts(LPCTSTR lpDevice, LPCTSTR lpType, LPCTSTR* lpName)
	{
		const std::tstring lpPrefix = std::tstring(lpType) + std::tstring(szSeparator);
		const size_t nSize = lpPrefix.size();
		if (memcmp(lpDevice, &lpPrefix.at(0), nSize * sizeof(TCHAR)) == 0)
		{
			*lpName = lpDevice + nSize;
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * 初期化
	 * @param[in] nRate サンプリング レート
	 * @param[in] nBufferSize バッファ サイズ
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Init(UINT nRate, UINT nBufferSize)
	{
		// 初期化済みか？
		if (m_pDevice)
		{
			return false;
		}

		// 無音指定か？
		if (nRate == 0)
		{
			return true;
		}

		ISoundDevice* pDevice = NULL;
		LPCTSTR lpDevice = NULL;

		if (Starts(Config.SoundDevice, TEXT("DSound"), &lpDevice))
		{
			pDevice = new SoundDeviceDSound3;
		}
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
		else if (Starts(Config.SoundDevice, TEXT("ASIO"), &lpDevice))
		{
			pDevice = new SoundDeviceAsio;
		}
		else if (Starts(Config.SoundDevice, TEXT("WASAPI"), &lpDevice))
		{
			pDevice = new SoundDeviceWasapi;
		}
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

		if (pDevice)
		{
			if (!pDevice->Open(lpDevice, hWndMain))
			{
				delete pDevice;
				pDevice = NULL;
			}
		}

		if (!pDevice)
		{
			pDevice = new SoundDeviceDSound3;
			if (!pDevice->Open(NULL, hWndMain))
			{
				delete pDevice;
				pDevice = NULL;
			}
		}

		if (!pDevice)
		{
			return false;
		}

		nBufferSize = (nRate * nBufferSize) / 200;
		nBufferSize = pDevice->CreateStream(nRate, 2, nBufferSize);
		if (nBufferSize == 0)
		{
			delete pDevice;
			return false;
		}

		// 1フレーム分は補償する
		const UINT nFrameSize = (nRate * 55) / 1000;
		nBufferSize = max(nBufferSize, nFrameSize);

		m_pDevice = pDevice;
		m_nRate = nRate;
		DSound_PreCounter = 0;
		m_pBuffer = new int16_t[nBufferSize * 4];
		m_nBufferIndex = 0;
		m_nBufferSize = nBufferSize;

		pDevice->SetStreamData(this);
		pDevice->PlayStream();

		return true;
	}

	/**
	 * 解放
	 */
	void Cleanup()
	{
		delete m_pDevice;
		m_pDevice = NULL;
		m_nRate = 0;
		DSound_PreCounter = 0;
		delete[] m_pBuffer;
		m_pBuffer = NULL;
		m_nBufferIndex = 0;
		m_nBufferSize = 0;
	}

	/**
	 * 再生
	 */
	void Play()
	{
		if (m_pDevice)
		{
			m_pDevice->PlayStream();
		}
	}

	/**
	 * 停止
	 */
	void Stop()
	{
		if (m_pDevice)
		{
			m_pDevice->StopStream();
		}
	}

	/**
	 * サンプル更新
	 * @param[in] length 長さ
	 */
	void Update(UINT nLength)
	{
		nLength = min(nLength, (m_nBufferSize * 2) - m_nBufferIndex);
		if (nLength > 0)
		{
			int16_t* p = &m_pBuffer[m_nBufferIndex * 2];
			ADPCM_Update(p, nLength);
			OPM_Update(p, nLength);
			Mcry_Update(p, nLength);
			MidiOut_Process16(hMIDIOut, p, nLength);
			m_nBufferIndex += nLength;
		}
	}

	/**
	 * サウンド更新
	 * @param[in] clock クロック
	 */
	void Send0(int clock)
	{
		if (m_pDevice)
		{
			m_cs.lock();
			DSound_PreCounter += (m_nRate * clock);
			UINT nLength = 0;
			while (DSound_PreCounter >= 10000000)
			{
				nLength++;
				DSound_PreCounter -= 10000000;
			}
			Update(nLength);
			m_cs.unlock();
		}
	}

	/**
	 * サウンド更新
	 */
	void Send()
	{
		// 本来は sdlaudio_callback内で足りないサンプルを補償したいが、
		// スレッドが異なるので、メインスレッドで埋めておく
		if (m_pDevice)
		{
			m_cs.lock();
			if (m_nBufferIndex < m_nBufferSize)
			{
				Update(m_nBufferSize - m_nBufferIndex);
			}
			m_cs.unlock();
		}
	}

	/**
	 * ストリーム データを得る
	 * @param[out] lpBuffer バッファ
	 * @param[in] nBufferCount バッファ カウント
	 * @return サンプル数
	 */
	virtual UINT Get16(int16_t* lpBuffer, UINT nBufferCount)
	{
		m_cs.lock();
		nBufferCount = min(nBufferCount, m_nBufferIndex);
		if (nBufferCount > 0)
		{
			CopyMemory(lpBuffer, m_pBuffer, nBufferCount * 4);
			const UINT nRemain = m_nBufferIndex - nBufferCount;
			if (nRemain > 0)
			{
				memmove(m_pBuffer, m_pBuffer + nBufferCount * 2, nRemain * 4);
			}
			m_nBufferIndex = nRemain;
		}
		m_cs.unlock();
		return nBufferCount;
	}
};

static DSound s_ds;

LPCTSTR DSound_GetDeviceName(UINT nNum)
{
	return s_ds.GetDeviceName(nNum);
}

int DSound_Init(DWORD rate, DWORD buflen) {

	SoundDeviceDSound3::Initialize();
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	SoundDeviceAsio::Initialize();
	SoundDeviceWasapi::Initialize();
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	return s_ds.Init(rate, buflen) ? TRUE : FALSE;
}

void DSound_Cleanup(void) {

	s_ds.Cleanup();

#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	SoundDeviceWasapi::Deinitialize();
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)
}

void DSound_Play(void) {

	s_ds.Play();
}

void DSound_Stop(void) {

	s_ds.Stop();
}

void FASTCALL DSound_Send0(long clock) {

	s_ds.Send0(clock);
}

void FASTCALL DSound_Send(void) {

	s_ds.Send();
}

## xkeropi
エックスけろぴー (WinX68k for X)

## 本レポジトリについて
- xkeropi をちょこちょこ修正したものです
- x64 ビルドや C++ 化、np2 からの流用を計画しています

## 参照ソースコード
- 本レポジトリは、下記のソースコードを利用させていただいています
### Winx68k
- けんじょさんによる Windows版 (オリジナル)
- [Web Page](http://retropc.net/usalin/)

### xkeropi
- nonakap さんによる X11 ポート
- [GitHub レポジトリ](https://github.com/nonakap/xkeropi)

### px68k
- ひっそりぃさんによる Android/iOS/PSP/RPI/MacOSX ポート
- [GitHub レポジトリ](https://github.com/hissorii/px68k)

### px68k-libretro
- r-type (not6) さんによる libretro ポート
- [GitHub レポジトリ](https://github.com/r-type/px68k-libretro)

### fmgen
- cisc さんによる OPM エンジン
- [Web Page](http://retropc.net/cisc/sound/)

----
Twitter: [@yuinejp](https://twitter.com/yuinejp)

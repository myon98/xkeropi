/*!
 * @file	thread.h
 * @brief	スレッド基底クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <process.h>

/**
 * @brief スレッド基底クラス
 */
class CThread
{
public:
	/**
	 * コンストラクタ
	 */
	CThread()
		: m_hThread(INVALID_HANDLE_VALUE)
		, m_dwThreadId(0)
	{
	}

	/**
	 * デストラクタ
	 */
	virtual ~CThread()
	{
		Join();
	}

	/**
	 * スレッド開始
	 * @retval true 成功
	 */
	bool Start()
	{
		if (m_hThread != INVALID_HANDLE_VALUE)
		{
			return false;
		}

		DWORD dwThreadId = 0;
		HANDLE hThread = ::CreateThread(NULL, 0, &ThreadProc, this, 0, &dwThreadId);
		if (hThread == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		m_hThread = hThread;
		m_dwThreadId = dwThreadId;
		return true;
	}

	/**
	 * スレッド終了
	 */
	void Join()
	{
		if (m_hThread != INVALID_HANDLE_VALUE)
		{
			::WaitForSingleObject(m_hThread, INFINITE);
			::CloseHandle(m_hThread);
			m_hThread = INVALID_HANDLE_VALUE;
		}
	}

protected:
	virtual void Task() = 0;	/*!< スレッド */

private:
	HANDLE m_hThread;			//!< スレッド ハンドル
	DWORD m_dwThreadId;			//!< スレッド ID

	/*!
	 * スレッド処理
	 * @param[in] pParam this ポインタ
	 * @retval 0 常に0
	 */
	static DWORD __stdcall ThreadProc(LPVOID pParam)
	{
		(static_cast<CThread*>(pParam))->Task();
		return 0;
	}
};
